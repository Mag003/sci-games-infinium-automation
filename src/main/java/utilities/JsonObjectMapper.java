package utilities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.*;

public class JsonObjectMapper {

    private static final Logger logger = LoggerFactory.getLogger(JsonObjectMapper.class);
    private static final ObjectMapper objectMapper = new ObjectMapper();

    private JsonObjectMapper() {
        throw new IllegalStateException("Utility class");
    }

    public static <T> T mapToObject(Response response, Class<T> className) {
        try {
            return mapToObject(response.getBody().asString(), className);
        } catch (NullPointerException | IllegalArgumentException e) {
            String message = "An exception during converting response: " + e.getMessage() +
                    "\nClass " + className +
                    "\nResponse was:" + response.getBody();
            logger.error(message);
            throw new IllegalArgumentException(message);
        }
    }

    public static <T> T mapToObject(String json, Class<T> className) {
        try {
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            objectMapper.disable(DeserializationFeature.FAIL_ON_NULL_CREATOR_PROPERTIES);
            return objectMapper.readValue(json, className);
        } catch (JsonProcessingException e) {
            String message = "An exception during converting JSON: " + e.getMessage() +
                    "\nClass " + className +
                    "\nJSON was:" + json;
            logger.error(message);
            throw new IllegalArgumentException(message);
        }
    }

    public static String mapToJson(Object object) {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException | NullPointerException e) {
            String errorMessage = "An exception while trying to represent object as String: " + e.getMessage() + "\n Object Class: " + object.getClass();
            logger.error(errorMessage);
            return errorMessage;
        }
    }

    public static <T> List<String> validateForNull(T instance, List<String> jsonProperties) {
        Field[] fields = instance.getClass().getDeclaredFields();
        Map<String, Object> properties = new HashMap<>();
        Arrays.stream(fields).forEach(field -> {
            Class<JsonProperty> annotation = JsonProperty.class;
            if (field.isAnnotationPresent(annotation)) {
                String jsonProperty = field.getAnnotation(annotation).value();
                field.setAccessible(true);
                try {
                    properties.put(jsonProperty, field.get(instance));
                } catch (IllegalAccessException e) {
                    logger.warn(e.getMessage());
                }
            }
        });

        List<String> nullFields = new ArrayList<>();
        jsonProperties.forEach(property -> {
            if (properties.containsKey(property)) {
                if (properties.get(property) == null) {
                    nullFields.add(property);
                }
            } else {
                nullFields.add(property);
            }
        });
        return nullFields;
    }
}
