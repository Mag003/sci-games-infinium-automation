package utilities;

import com.google.common.base.Strings;
import extensions.configuration.FrameworkModule;
import extensions.configuration.IConfiguration;
import net.thucydides.core.guice.Injectors;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;

import java.time.Duration;
import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import java.util.concurrent.TimeoutException;
import java.util.function.BooleanSupplier;

public class ConditionalWait {
    private final IConfiguration configuration;

    public ConditionalWait() {
        this.configuration = Injectors.getInjector(new FrameworkModule()).getInstance(IConfiguration.class);
    }

    public boolean waitFor(BooleanSupplier condition, String message) {
        return waitFor(condition, configuration.timeouts().getCondition(), configuration.timeouts().getPolling(), message, handledExceptions());
    }

    public void waitForTrue(BooleanSupplier condition, String message) throws TimeoutException {
        waitForTrue(condition, configuration.timeouts().getCondition(), configuration.timeouts().getPolling(), message, handledExceptions());
    }

    private Collection<Class<? extends Throwable>> handledExceptions() {
        return Arrays.asList(NoSuchElementException.class, StaleElementReferenceException.class);
    }

    public boolean waitFor(BooleanSupplier condition, Duration timeout, Duration pollingInterval, String message) {
        return waitFor(condition, timeout, pollingInterval, message, handledExceptions());
    }

    public boolean waitFor(BooleanSupplier condition, Duration timeout, Duration pollingInterval, String message, Collection<Class<? extends Throwable>> exceptionsToIgnore) {
        try {
            waitForTrue(condition, timeout, pollingInterval, message, exceptionsToIgnore);
            return true;
        } catch (TimeoutException e) {
            return false;
        }
    }

    public void waitForTrue(BooleanSupplier condition, Duration timeout, Duration pollingInterval, String message, Collection<Class<? extends Throwable>> exceptionsToIgnore) throws TimeoutException {
        BooleanSupplier supplier = Optional.ofNullable(condition).orElseThrow(() -> new IllegalArgumentException("Condition cannot be null"));
        long timeoutInSeconds = resolveConditionTimeoutInSeconds(timeout);
        long pollingIntervalInMilliseconds = resolvePollingInterval(pollingInterval).toMillis();
        String exMessage = resolveMessage(message);
        double startTime = getCurrentTime();
        while (true) {
            if (isConditionSatisfied(supplier, exceptionsToIgnore)) {
                return;
            }

            double currentTime = getCurrentTime();
            if ((currentTime - startTime) > timeoutInSeconds) {
                String exceptionMessage = String.format("Timed out after %1$s seconds during wait for condition '%2$s'", timeoutInSeconds, exMessage);
                throw new TimeoutException(exceptionMessage);
            }

            try {
                Thread.sleep(pollingIntervalInMilliseconds);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }

    private double getCurrentTime() {
        return System.nanoTime() / Math.pow(10, 9);
    }

    private boolean isConditionSatisfied(BooleanSupplier condition, Collection<Class<? extends Throwable>> exceptionsToIgnore) {
        try {
            return condition.getAsBoolean();
        } catch (Exception e) {
            if (exceptionsToIgnore == null || !exceptionsToIgnore.contains(e.getClass())) {
                throw e;
            }

            return false;
        }
    }

    private long resolveConditionTimeoutInSeconds(Duration timeout) {
        return Optional.ofNullable(timeout).orElse(configuration.timeouts().getCondition()).getSeconds();
    }

    private Duration resolvePollingInterval(Duration pollingInterval) {
        return Optional.ofNullable(pollingInterval).orElse(configuration.timeouts().getPolling());
    }

    private String resolveMessage(String message) {
        return Strings.isNullOrEmpty(message) ? "" : message;
    }
}
