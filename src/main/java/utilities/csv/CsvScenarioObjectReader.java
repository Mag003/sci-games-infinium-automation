package utilities.csv;

import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class CsvScenarioObjectReader extends CsvObjectReaderFunctions<Map<String, Object>> {
    public CsvScenarioObjectReader(String csvFilePath) {
        super(csvFilePath);
    }

    public List<Map<String, Object>> readAll() {
        if (Paths.get(csvFilePath).toFile().isFile()) {
            return read(super.getValues(csvFilePath));
        } else {
            List<Map<String, Object>> scenarios = new ArrayList<>();
            Arrays.stream(Objects.requireNonNull(Paths.get(csvFilePath).toFile().listFiles()))
                    .filter(file -> file.getName().contains("csv"))
                    .forEach(file -> scenarios.addAll(read(super.getValues(file.getPath()))));
            return scenarios;
        }
    }

    @Override
    public List<Map<String, Object>> read(List<List<Object>> values) {
        List<Map<String, Object>> scenarios = new ArrayList<>();
        List<String> keys = values.get(0).stream().map(Object::toString).collect(Collectors.toList());
        List<String> types = values.get(1).stream().map(Object::toString).collect(Collectors.toList());
        for (int i = 2; i < values.size(); i++) {
            List<Object> value = values.get(i);
            if (value.size() == keys.size()) {
                Map<String, Object> stringMap = new HashMap<>();
                for (int k = 0; k < keys.size(); k++) {
                    Object valueWithType;
                    if (types.get(k).equalsIgnoreCase("int")) {
                        valueWithType = Integer.parseInt(value.get(k).toString());
                    } else {
                        valueWithType = value.get(k);
                    }
                    stringMap.put(keys.get(k), valueWithType);
                }
                scenarios.add(stringMap);
            }
        }
        return scenarios;
    }
}
