package utilities.csv;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

public abstract class CsvReaderFunctions<T> {
    protected final String csvFilePath;
    protected final Logger logger = LoggerFactory.getLogger(CsvReaderFunctions.class);

    public CsvReaderFunctions(String csvFilePath) {
        this.csvFilePath = csvFilePath;
    }

    /**
     * This method needs to be implemented to read the rows of the csv file to a custom object model
     *
     * @param values is the list of the csv file rows
     * @return the list of models
     */
    public abstract List<T> read(List<List<String>> values);

    public List<T> getObjects() {
        return read(getValues(csvFilePath));
    }

    public List<T> getObjects(char separator) {
        return read(getValues(csvFilePath, separator));
    }

    /**
     * This method is used to read a list of model objects from the csv file which contain a key in a row
     *
     * @param key is the value to look for in the csv file
     * @return the list of models which contain the key
     */
    public List<T> getObjectsByKey(String key) {
        List<List<String>> values = getValues(csvFilePath);
        List<List<String>> result = new ArrayList<>();
        for (List<String> valuesList : values) {
            if (valuesList.contains(key)) {
                result.add(valuesList);
            }
        }
        return read(result);
    }

    /**
     * This method is used to read a list of model objects from the csv file which contain a key in a row in the specific column
     *
     * @param key is the value to look for in the csv file
     * @param col is the number of column for search key
     * @return the list of models which contain the key in a specific column
     */
    public List<T> getObjectsByKey(String key, Integer col) {
        List<List<String>> values = getValues(csvFilePath);
        List<List<String>> result = new ArrayList<>();
        for (List<String> valuesList : values) {
            if (valuesList.size() > col && valuesList.get(0).equals(key)) {
                result.add(valuesList);
            }
        }
        return read(result);
    }

    /**
     * This method is used to read a list of model objects from the csv file which contain a key in a row in content of the cell
     *
     * @param key is the value to look for in the csv file
     * @return the list of models which contain the key in a content of cell
     */
    public List<T> getObjectsByValueContainsInColumn(String key) {
        List<List<String>> values = getValues(csvFilePath);
        List<List<String>> result = new ArrayList<>();
        for (List<String> valuesList : values) {
            for (String colValue : valuesList) {
                if (colValue.contains(key)) {
                    result.add(valuesList);
                    break;
                }
            }
        }
        return read(result);
    }

    /**
     * This method is used to read a unique object from the csv file
     *
     * @param key is the value to look for in the csv file
     * @return the object of a model
     */
    public T getObjectByKey(String key) {
        List<T> objects = getObjectsByKey(key);
        if (objects.size() != 1) {
            logger.error(String.format("Unique object by key '%s' was not found", key));
            throw new NoSuchElementException();
        }
        return objects.get(0);
    }

    private List<List<String>> getValues(String filePath, char separator) {
        String resourcesFilePath = filePath.replace("\\", "/");
        List<List<String>> values = new ArrayList<>();
        CSVParser parser = new CSVParserBuilder().withSeparator(separator).build();
        try (CSVReader reader = new CSVReaderBuilder(new InputStreamReader(new FileInputStream(resourcesFilePath), StandardCharsets.UTF_8)).withCSVParser(parser).build()) {
            String[] nextLine;
            while ((nextLine = reader.readNext()) != null) {
                List<String> line = Arrays.asList(nextLine);
                values.add(line);
            }
        } catch (java.io.IOException e) {
            logger.error(String.format("An error occurred during parsing of the CSV file '%s': %s", resourcesFilePath, e.getMessage()));
        }
        return values;
    }

    private List<List<String>> getValues(String filePath) {
        return getValues(filePath, ',');
    }
}