package utilities.sheets;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UncheckedIOException;


public abstract class WorkbookAbstractForm implements Form, IWorkSheet {
    protected XSSFWorkbook form;

    public WorkbookAbstractForm(File file) {
        this.form = readFromFile(file);
    }

    protected XSSFWorkbook readFromFile(File file) {
        try (FileInputStream fis = new FileInputStream(file)) {
            return new XSSFWorkbook(fis);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
