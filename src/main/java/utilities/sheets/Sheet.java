package utilities.sheets;

import java.util.List;

public interface Sheet {
    int size();

    void add(List<String> row);

    List<List<String>> getStringList();
}

