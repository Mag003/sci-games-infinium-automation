package utilities.sheets;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractSheet implements Sheet {
    private final List<List<String>> rows = new ArrayList<>();

    public final int size() {
        return rows.size();
    }

    public void add(List<String> row) {
        rows.add(row);
    }

    public final List<List<String>> getStringList() {
        return rows;
    }
}
