package utilities.sheets;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

public interface IWorkSheet {
    default void readSheet(XSSFSheet from, Sheet sheet, int rowOffset) {
        Iterator<Row> sheetIterator;
        try {
            sheetIterator = from.iterator();
        } catch (NullPointerException e) {
            throw new RuntimeException(e);
        }
        for (int i = 0; i < rowOffset; i++) {
            sheetIterator.next();
        }
        while (sheetIterator.hasNext()) {
            Row row = sheetIterator.next();
            List<String> rowList = new ArrayList<>();
            int rowSize = row.getLastCellNum();
            for (int i = 0; i < rowSize; i++) {
                try {
                    Cell cell = row.getCell(i);
                    String value = "";
                    switch (cell.getCellType()) {
                        case NUMERIC:
                            value = BigDecimal.valueOf(cell.getNumericCellValue()).stripTrailingZeros().toPlainString();
                            break;
                        case STRING:
                            value = cell.getStringCellValue();
                            break;
                        case BOOLEAN:
                            value = String.valueOf(cell.getBooleanCellValue());
                            break;
                    }
                    rowList.add(value);
                } catch (NullPointerException e) {
                  // ignore
                }
            }
            if (rowList.stream().anyMatch(Objects::nonNull)) {
                sheet.add(rowList);
            }
        }
    }
}
