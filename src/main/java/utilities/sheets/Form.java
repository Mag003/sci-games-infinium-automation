package utilities.sheets;

public interface Form {
    Sheet getSheet(String name);
}
