package scigamesinfinium.generators;

import scigamesinfinium.generators.templates.IGenerator;

import java.util.ArrayList;
import java.util.List;

public class Generator {
    public static void main(String[] args) {
        List<IGenerator> generatorTemplates = new ArrayList<>();
        //here we have to specify generators
        // if we are using separate models
        generatorTemplates.forEach(IGenerator::generate);
    }
}
