package scigamesinfinium.generators.templates;

public interface IGenerator {
    void generate();
}
