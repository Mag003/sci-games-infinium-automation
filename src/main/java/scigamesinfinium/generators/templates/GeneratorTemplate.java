package scigamesinfinium.generators.templates;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;
import java.util.stream.Stream;

public abstract class GeneratorTemplate<T> implements IGenerator {

    private static final Logger LOGGER = LoggerFactory.getLogger(GeneratorTemplate.class);
    private final String featuresDir;

    public GeneratorTemplate(String featuresDir) {
        this.featuresDir = featuresDir;
    }

    public abstract List<T> getData();

    public abstract Template createVelocityTemplate(VelocityEngine velocityEngine, T item);

    public abstract VelocityContext createVelocityContext(T item);

    public abstract File getFeatureFile(T item);

    protected String getFeaturesDir() {
        return featuresDir;
    }

    @Override
    public void generate() {
        Properties props = new Properties();
        props.setProperty("resource.loader", "class");
        props.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");

        String mavenDir = System.getProperty("project.basedir");
        File baseDir = new File(mavenDir == null ? System.getProperty("user.dir") : mavenDir);

        String resourcesDir = "/src/test/resources";

        File projectDir = new File(baseDir.getAbsolutePath() + resourcesDir);
        String rootDirectory = projectDir.exists() ? projectDir.getAbsolutePath() : baseDir.getAbsolutePath();

        props.setProperty("file.resource.loader.path", new File(rootDirectory).getAbsolutePath());
        props.setProperty("file.resource.loader.cache", "false");
        props.setProperty("file.resource.loader.modificationCheckInterval", "0");
        VelocityEngine velocityEngine = new VelocityEngine(props);
        velocityEngine.init();

        deleteDirectory(featuresDir);

        getData().forEach(item -> {
            Template template = createVelocityTemplate(velocityEngine, item);
            VelocityContext context = createVelocityContext(item);
            try {
                File featureFile = getFeatureFile(item);
                featureFile.getParentFile().mkdirs();
                FileWriter writer = new FileWriter(featureFile);
                template.merge(context, writer);
                writer.flush();
            } catch (IOException e) {
                LOGGER.error(e.getMessage());
            }
        });
    }


    /**
     * deletes directory and sub-directories
     *
     * @param directory path to directory
     */
    private void deleteDirectory(String directory) {
        if (new File(directory).exists()) {
            try {
                Stream<Path> files = Files.walk(Paths.get(directory));
                files.sorted(Comparator.reverseOrder())
                        .map(Path::toFile)
                        .forEach(File::delete);
                files.close();
            } catch (IOException ex) {
                LOGGER.error(ex.getMessage());
            }
        }
    }
}
