package scigamesinfinium.generators;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scigamesinfinium.DiProvider;
import scigamesinfinium.generators.templates.GeneratorTemplate;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static org.apache.commons.lang3.StringUtils.EMPTY;

public class FeatureGenerator extends GeneratorTemplate<Map<String, Object>> {
    private static final String TEMPLATE = "template";
    private static final String ID = "id";
    private static final String UNDEFINED = "UNDEFINED";
    private static final String DIR = "dir";
    private static final Path RESOURCES_PATH = Paths.get("src", "main", "resources");
    private static final Logger LOGGER = LoggerFactory.getLogger(FeatureGenerator.class);

    public static void main(String[] args) {
        new FeatureGenerator().generate();
    }

    public FeatureGenerator() {
        super("src/test/resources/features/templates");
    }

    public FeatureGenerator(String path) {
        super(path);
    }

    @Override
    public List<Map<String, Object>> getData() {
        return DiProvider.environment().data().getScenarios();
    }

    @Override
    public Template createVelocityTemplate(VelocityEngine velocityEngine, Map<String, Object> scenario) {
        List<Path> templates = getFeaturesPaths();
        return velocityEngine.getTemplate(templates
                .stream()
                .filter(path -> scenario.get(TEMPLATE) != null)
                .filter(path -> path.toString().replace("\\", "/").contains(scenario.get(TEMPLATE).toString()))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Not found templates for scenario " + scenario))
                .toString().replace(RESOURCES_PATH.toString(), EMPTY));
    }

    @Override
    public VelocityContext createVelocityContext(Map<String, Object> scenario) {
        VelocityContext context = new VelocityContext();
        scenario.forEach(context::put);
        return context;
    }

    private List<Path> getFeaturesPaths() {
        List<Path> featureFiles = new ArrayList<>();
        try (Stream<Path> paths = Files.walk(Paths.get(RESOURCES_PATH.toString(), "templates"))) {
            paths.filter(file -> file.toFile().isFile()).forEach(featureFiles::add);
        } catch (IOException e) {
            throw new IllegalArgumentException("Not found templates");
        }
        return featureFiles;
    }

    @Override
    public File getFeatureFile(Map<String, Object> scenario) {
        return new File(String.format("%1$s/%2$s/%3$s_C%4$s.feature", getFeaturesDir(),
                getKey(scenario, DIR), getKey(scenario, TEMPLATE).replaceAll("(\\w+/)", EMPTY).replace(".vm", EMPTY), getKey(scenario, ID)));
    }


    private String getKey(Map<String, Object> bet, String key) {
        return bet.get(key) != null ? bet.get(key).toString() : UNDEFINED;
    }
}
