package scigamesinfinium;

import extensions.configuration.FrameworkModule;
import extensions.configuration.IConfiguration;
import net.thucydides.core.guice.Injectors;
import scigamesinfinium.environment.SciEnvironment;
import scigamesinfinium.environment.SciModule;

public class DiProvider {
    private static final FrameworkModule frameworkModule = new FrameworkModule();
    private static final SciModule sciModule = new SciModule();
    private static final ThreadLocal<IConfiguration> configurationHolder = new ThreadLocal<>();

    public static FrameworkModule getFrameworkModule() {
        return frameworkModule;
    }

    public static SciModule getSciModule() {
        return sciModule;
    }

    public static IConfiguration configuration() {
        if (configurationHolder.get() == null) {
            configurationHolder.set(get(IConfiguration.class));
        }
        return configurationHolder.get();
    }

    public static SciEnvironment environment() {
        return get(SciEnvironment.class);
    }

    private static <T> T get(Class<T> tClass) {
        return Injectors.getInjector(getFrameworkModule()).getInstance(tClass);
    }
}
