package scigamesinfinium.environment;

import com.google.inject.Inject;
import extensions.configuration.FrameworkModule;
import utilities.ISettingsFile;
import utilities.JsonSettingsFile;

import java.io.File;

public class SciEnvironment {
    private final ISettingsFile environmentSettingsFile;
    private final Data data;

    @Inject
    public SciEnvironment(ISettingsFile settingsFile) {
        String envName = String.valueOf(settingsFile.getValue("/environment/name"));
        File envConfig = new File("src/test/resources/environment/" + envName + "/config.json");
        this.environmentSettingsFile = new JsonSettingsFile(envConfig);
        this.data = new Data(environmentSettingsFile);
    }

    public String getUrl() {
        return getValue("/url");
    }

    public String getApiUrl() {
        return getValue("/apiUrl");
    }

    public String getSwaggerUrl() {
        return getValue("/swaggerUrl");
    }

    public Data data() {
        return data;
    }

    public static boolean isIosOS() {
        return String.valueOf(FrameworkModule.getFrameworkSettings().getValue("/screens/type")).equals("iOS");
    }

    public static boolean isDesktopWeb() {
        return String.valueOf(FrameworkModule.getFrameworkSettings().getValue("/screens/type")).equals("desktop_web");
    }

    public static boolean isMobileWeb() {
        return String.valueOf(FrameworkModule.getFrameworkSettings().getValue("/screens/type")).equals("mobile_web");
    }

    private String getValue(String jsonPath) {
        return String.valueOf(environmentSettingsFile.getValue(jsonPath));
    }
}
