package scigamesinfinium.environment;

import utilities.ISettingsFile;
import utilities.csv.CsvScenarioObjectReader;

import java.util.List;
import java.util.Map;

public class Data {
    private final CsvScenarioObjectReader csvScenarioReader;

    public Data(ISettingsFile settingsFile) {
        this.csvScenarioReader = new CsvScenarioObjectReader(settingsFile.getCanonicalPath().replace("config.json", ""));
    }

    public List<Map<String, Object>> getScenarios() {
        return csvScenarioReader.readAll();
    }
}
