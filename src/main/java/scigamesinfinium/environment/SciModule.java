package scigamesinfinium.environment;

import com.google.inject.AbstractModule;
import extensions.configuration.FrameworkModule;

public class SciModule extends AbstractModule {
    @Override
    protected void configure() {
        this.bind(SciEnvironment.class).toInstance(new SciEnvironment(FrameworkModule.getFrameworkSettings()));
    }
}
