package extensions.retrier;

import com.google.inject.Inject;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;

import java.util.Arrays;
import java.util.Collection;
import java.util.function.Supplier;

public class ActionRetrier {
    private final IRetryConfiguration retryConfiguration;

    @Inject
    public ActionRetrier(IRetryConfiguration retryConfiguration) {
        this.retryConfiguration = retryConfiguration;
    }

    public void doWithRetry(Runnable runnable) {
        doWithRetry(runnable, getDefaultExceptionsToIgnore());
    }

    public void doWithRetry(Runnable runnable, Collection<Class<? extends Throwable>> handledExceptions) {
        Supplier<?> supplier = () -> {
            runnable.run();
            return true;
        };
        doWithRetry(supplier, handledExceptions);
    }

    public <T> T doWithRetry(Supplier<T> function) {
        return doWithRetry(function, getDefaultExceptionsToIgnore());
    }

    public <T> T doWithRetry(Supplier<T> function, Collection<Class<? extends Throwable>> handledExceptions) {
        int retryAttemptsLeft = retryConfiguration.getNumber();
        T result = null;
        while (retryAttemptsLeft >= 0) {
            try {
                result = function.get();
                break;
            } catch (Exception exception) {
                if (isExceptionHandled(handledExceptions, exception) && retryAttemptsLeft != 0) {
                    try {
                        Thread.sleep(retryConfiguration.getPollingInterval().toMillis());
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                    retryAttemptsLeft--;
                } else {
                    throw exception;
                }
            }
        }
        return result;
    }

    protected boolean isExceptionHandled(Collection<Class<? extends Throwable>> handledExceptions, Exception exception) {
        return handledExceptions.contains(exception.getClass());
    }

    private Collection<Class<? extends Throwable>> getDefaultExceptionsToIgnore() {
        return Arrays.asList(InvalidElementStateException.class, NoSuchElementException.class, StaleElementReferenceException.class, StaleElementReferenceException.class);
    }
}
