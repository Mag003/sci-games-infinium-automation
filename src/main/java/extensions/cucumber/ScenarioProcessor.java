package extensions.cucumber;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ScenarioProcessor {
    private ScenarioProcessor() {
    }

    public static Optional<String> getTestRailId(Collection<String> tags) {
        String testRailTagPrefix = "@TR-";
        List<String> trTags = tags.stream()
                .filter(tag -> tag.startsWith(testRailTagPrefix))
                .map(tag -> tag.replace(testRailTagPrefix, ""))
                .collect(Collectors.toList());
        if (trTags.size() == 1) {
            return Optional.of(trTags.get(0));
        } else {
            return Optional.empty();
        }
    }

    public static List<String> getTestRailIds(Collection<String> tags) {
        String testRailTagPrefix = "@TR-";
        return tags.stream()
                .filter(tag -> tag.startsWith(testRailTagPrefix))
                .map(tag -> tag.replace(testRailTagPrefix, ""))
                .collect(Collectors.toList());

    }
}
