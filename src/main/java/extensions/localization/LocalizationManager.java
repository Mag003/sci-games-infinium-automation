package extensions.localization;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utilities.ISettingsFile;
import utilities.JsonSettingsFile;

import java.util.Locale;

public class LocalizationManager implements ILocalizationManager {
    private static final String LANG_RESOURCE_TEMPLATE = "localization/%1$s.json";
    private final ISettingsFile localizationFile;
    private final Logger logger = LoggerFactory.getLogger(LocalizationManager.class);
    private final String locResourceName;
    private final Locale locale;

    public LocalizationManager(String language) {
        String[] langAndCountry = language.split("-");
        locale = new Locale(langAndCountry[0], langAndCountry[1]);
        locResourceName = String.format(LANG_RESOURCE_TEMPLATE, language);
        this.localizationFile = getLocalizationFileIfExist(locResourceName);
    }

    private static ISettingsFile getLocalizationFileIfExist(String fileName) {
        return LocalizationManager.class.getClassLoader().getResource(fileName) == null
                ? null
                : new JsonSettingsFile(fileName);
    }

    @Override
    public String getLocalizedMessage(String messageKey, Object... args) {
        String jsonKeyPath = "/".concat(messageKey);
        if (localizationFile != null && localizationFile.isValuePresent(jsonKeyPath)) {
            return String.format(localizationFile.getValue(jsonKeyPath).toString(), args);
        }
        logger.warn(String.format("Cannot find localized message by key '%1$s' in resource file %2$s",
                jsonKeyPath, locResourceName));
        return messageKey;
    }

    @Override
    public Locale getLocale() {
        return locale;
    }
}
