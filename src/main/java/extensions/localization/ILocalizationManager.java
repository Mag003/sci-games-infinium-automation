package extensions.localization;

import java.util.Locale;

public interface ILocalizationManager {
    String getLocalizedMessage(String messageKey, Object... args);

    Locale getLocale();
}
