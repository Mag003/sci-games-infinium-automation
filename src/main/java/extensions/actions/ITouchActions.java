package extensions.actions;

/**
 * Describes general Touch Actions.
 */
public interface ITouchActions {
    void swipe(SwipeAction swipeAction);

    void swipeWithLongPress(SwipeAction swipeAction);
}
