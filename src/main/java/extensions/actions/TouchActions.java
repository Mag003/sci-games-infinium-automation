package extensions.actions;

import extensions.configuration.FrameworkModule;
import extensions.configuration.IConfiguration;
import extensions.retrier.ActionRetrier;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.guice.Injectors;
import org.openqa.selenium.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Collection;
import java.util.function.BooleanSupplier;
import java.util.function.UnaryOperator;

import static io.appium.java_client.touch.WaitOptions.waitOptions;

public class TouchActions implements ITouchActions {
    private final IConfiguration configuration;
    private final ActionRetrier actionRetrier;
    private final WebDriver webDriver;
    private final Logger logger = LoggerFactory.getLogger(TouchActions.class);
    private final Point scrollDownStartPoint;
    private final Point scrollDownEndPoint;
    private final Point swipeLeftStartPoint;
    private final Point swipeLeftEndPoint;

    public TouchActions(WebDriver webDriver) {
        this.configuration = Injectors.getInjector(new FrameworkModule()).getInstance(IConfiguration.class);
        this.actionRetrier = new ActionRetrier(configuration.retry());
        this.webDriver = webDriver;
        this.scrollDownStartPoint = recalculatePointCoordinates(
                getBottomRightCornerPoint(),
                (1 - configuration.touchActions().getSwipeHorizontalOffset()),
                (1 - configuration.touchActions().getSwipeVerticalOffset()));
        this.scrollDownEndPoint = recalculatePointCoordinates(
                getBottomRightCornerPoint(),
                configuration.touchActions().getSwipeHorizontalOffset(),
                configuration.touchActions().getSwipeVerticalOffset());
        this.swipeLeftStartPoint = recalculatePointCoordinates(
                getBottomRightCornerPoint(),
                (1 - configuration.touchActions().getSwipeVerticalOffset()),
                (1 - configuration.touchActions().getSwipeHorizontalOffset()));
        this.swipeLeftEndPoint = recalculatePointCoordinates(
                getBottomRightCornerPoint(),
                configuration.touchActions().getSwipeVerticalOffset(),
                configuration.touchActions().getSwipeHorizontalOffset());
    }

    public void scrollUntil(BooleanSupplier condition, SwipeDirection direction) {
        scrollAndPerform(condition, () -> {
        }, direction);
    }

    public void scrollUntil(BooleanSupplier condition, SwipeDirection direction, int retries) {
        scrollAndPerform(condition, () -> {
        }, direction, retries);
    }

    public void scrollUntil(BooleanSupplier condition, SwipeAction action) {
        scrollAndPerform(condition, () -> {
        }, action);
    }

    public void scrollAndPerform(BooleanSupplier condition, Runnable action, SwipeDirection direction) {
        scrollAndPerform(condition, action, swipeActionWithDirection(direction));
    }

    public void scrollAndPerform(BooleanSupplier condition, Runnable action, SwipeDirection direction, int retries) {
        scrollAndPerform(condition, action, swipeActionWithDirection(direction), retries);
    }

    private SwipeAction swipeActionWithDirection(SwipeDirection direction) {
        SwipeAction swipeAction;
        switch (direction) {
            case DOWN:
                swipeAction = new SwipeAction(scrollDownStartPoint, scrollDownEndPoint);
                break;
            case UP:
                swipeAction = new SwipeAction(scrollDownEndPoint, scrollDownStartPoint);
                break;
            case LEFT:
                swipeAction = new SwipeAction(swipeLeftStartPoint, swipeLeftEndPoint);
                break;
            case RIGHT:
                swipeAction = new SwipeAction(swipeLeftEndPoint, swipeLeftStartPoint);
                break;
            default:
                throw new IllegalArgumentException(
                        String.format("'%s' direction does not exist", direction.toString()));
        }
        return swipeAction;
    }

    public void scrollAndPerform(BooleanSupplier condition, Runnable action, SwipeAction swipeAction) {
        int numberOfRetries = configuration.touchActions().getSwipeRetries();
        while (numberOfRetries-- > 0 && !evaluateCondition(condition)) {
            action.run();
            swipe(swipeAction);
        }
    }

    public void scrollAndPerform(BooleanSupplier condition, Runnable action, SwipeAction swipeAction, int retries) {
        int numberOfRetries = retries;
        while (numberOfRetries-- > 0 && !evaluateCondition(condition)) {
            action.run();
            swipe(swipeAction);
        }
    }


    private boolean evaluateCondition(BooleanSupplier condition) {
        boolean evaluationResult = false;
        try {
            evaluationResult = condition.getAsBoolean();
        } catch (NoSuchElementException | StaleElementReferenceException e) {
            LoggerFactory.getLogger(this.getClass()).warn(String.format("Condition was not completed cause of %1$s", e.getMessage()));
        }
        return evaluationResult;
    }

    public SwipeAction swipeToElement(WebElementFacade startElement, WebElementFacade stopElement, SwipeDirection direction) {
        return swipeToElement(startElement, stopElement, direction, false);
    }

    public SwipeAction swipeToElement(WebElementFacade startElement, WebElementFacade stopElement, SwipeDirection direction, boolean longPress) {
        SwipeAction swipeAction = getSwipeAction(startElement, stopElement, direction);
        if (longPress) {
            swipeWithLongPress(swipeAction);
        } else {
            swipe(swipeAction);
        }
        return swipeAction;
    }

    @Override
    public void swipe(SwipeAction swipeAction) {
        logSwipe(false, swipeAction.getStartPoint(), swipeAction.getEndPoint());
        performTouchAction(touchAction -> touchAction
                        .press(PointOption.point(swipeAction.getStartPoint()))
                        .waitAction(waitOptions(configuration.touchActions().getSwipeDuration())),
                swipeAction.getEndPoint());
    }

    @Override
    public void swipeWithLongPress(SwipeAction swipeAction) {
        logSwipe(true, swipeAction.getStartPoint(), swipeAction.getEndPoint());
        performTouchAction(touchAction -> touchAction.longPress(PointOption.point(swipeAction.getStartPoint())), swipeAction.getEndPoint());
    }

    private SwipeAction getSwipeAction(WebElementFacade firstInOrder, WebElementFacade lastInOrder, SwipeDirection direction) {
        int shift;
        Point start;
        Point end;
        shift = getShift(firstInOrder, direction);
        switch (direction) {
            case DOWN:
                start = new Point(getX(firstInOrder), getY(firstInOrder) - shift);
                end = new Point(getX(lastInOrder), getY(lastInOrder) + shift);
                break;
            case UP:
                start = new Point(getX(firstInOrder), getY(firstInOrder) + shift);
                end = new Point(getX(lastInOrder), getY(lastInOrder) - shift);
                break;
            case LEFT:
                start = new Point(getX(firstInOrder) + shift, getY(firstInOrder));
                end = new Point(getX(lastInOrder) - shift, getY(lastInOrder));
                break;
            case RIGHT:
                start = new Point(getX(firstInOrder) - shift, getY(firstInOrder));
                end = new Point(getX(lastInOrder) + shift, getY(lastInOrder));
                break;
            default:
                throw new IllegalArgumentException("Direction " + direction + " is not supported by automation framework");
        }
        return applyCorrection(new SwipeAction(start, end));
    }

    private SwipeAction applyCorrection(SwipeAction swipeAction) {
        Point start = applyCorrectionToPoint(swipeAction.getStartPoint());
        Point end = applyCorrectionToPoint(swipeAction.getEndPoint());
        return new SwipeAction(start, end);
    }

    private Point applyCorrectionToPoint(Point point) {
        Dimension dimension = webDriver.manage().window().getSize();
        int defaultShift = configuration.touchActions().getDefaultBorderShiftPixels();
        int y = (dimension.getHeight() - point.getY()) < defaultShift ? dimension.getHeight() - defaultShift : point.getY();
        int x = (dimension.getWidth() - point.getX()) < defaultShift ? dimension.getWidth() - defaultShift : point.getX();

        y = Math.max(y, defaultShift);
        x = Math.max(x, defaultShift);

        return new Point(x, y);
    }

    private int getShift(WebElementFacade element, SwipeDirection direction) {
        double deviation = configuration.touchActions().getElementShiftPercents();
        int measure = DirectionType.VERTICAL.equals(direction.getType()) ?
                element.getSize().getHeight() :
                element.getSize().getWidth();
        return (int) (measure * deviation);
    }

    private int getX(WebElementFacade element) {
        int x = getLocationSafely(element).getX();
        return x == 0 ? (x + configuration.touchActions().getDefaultBorderShiftPixels()) : x;
    }

    private int getY(WebElementFacade element) {
        int y = getLocationSafely(element).getY();
        return y == 0 ? (y - configuration.touchActions().getDefaultBorderShiftPixels()) : y;
    }

    private Point getLocationSafely(WebElementFacade element) {
        return actionRetrier.doWithRetry(element::getLocation, getHandledExceptions());
    }

    private void logSwipe(boolean longPress, Point startPoint, Point endPoint) {
        String message = String.format("Swiping %1$sfrom coordinates (x:%2$s; y:%3$s) to (x:%4$s; y:%5$s)",
                (longPress ? "using long press " : ""),
                startPoint.getX(),
                startPoint.getY(),
                endPoint.getX(),
                endPoint.getY());
        logger.info(message);
    }

    /**
     * Returns Point in the bottom right corner of the screen.
     */
    private Point getBottomRightCornerPoint() {
        Dimension screenSize = webDriver.manage().window().getSize();
        return new Point(screenSize.width, screenSize.height);
    }

    /**
     * Returns the point with recalculated coordinates.
     *
     * @param point            point to recalculate coordinates
     * @param horizontalOffset coefficient to recalculate the point with horizontal offset.
     * @param verticalOffset   coefficient to recalculate the point with vertical offset.
     * @return point with recalculated coordinates with horizontal and vertical offset.
     */
    private Point recalculatePointCoordinates(Point point, double horizontalOffset, double verticalOffset) {
        return new Point(
                (int) (point.getX() * horizontalOffset),
                (int) (point.getY() * verticalOffset));
    }

    private void performTouchAction(UnaryOperator<TouchAction<?>> function, Point endPoint) {
        TouchAction<?> touchAction = new TouchAction<>((AppiumDriver) webDriver);
        actionRetrier.doWithRetry(
                () -> function.apply(touchAction).moveTo(PointOption.point(endPoint)),
                getHandledExceptions())
                .release()
                .perform();
    }

    private Collection<Class<? extends Throwable>> getHandledExceptions() {
        return Arrays.asList(WebDriverException.class, InvalidElementStateException.class, NoSuchElementException.class, StaleElementReferenceException.class, org.openqa.selenium.StaleElementReferenceException.class);
    }
}
