package extensions.actions;

public enum SwipeDirection {
    UP(DirectionType.VERTICAL),
    DOWN(DirectionType.VERTICAL),
    LEFT(DirectionType.HORIZONTAL),
    RIGHT(DirectionType.HORIZONTAL);

    private final DirectionType type;

    SwipeDirection(DirectionType type) {
        this.type = type;
    }

    public DirectionType getType() {
        return type;
    }
}
