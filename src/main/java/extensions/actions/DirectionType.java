package extensions.actions;

public enum DirectionType {
    HORIZONTAL, VERTICAL
}
