package extensions.actions;

import org.openqa.selenium.Point;

public class SwipeAction {
    private final Point startPoint;
    private final Point endPoint;

    public SwipeAction(Point startPoint, Point endPoint) {
        this.startPoint = startPoint;
        this.endPoint = endPoint;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (object instanceof SwipeAction) {
            SwipeAction swipeAction = (SwipeAction) object;
            return (this.startPoint.equals(swipeAction.startPoint)) &&
                    (this.endPoint.equals(swipeAction.endPoint));
        } else {
            return false;
        }
    }

    public Point getStartPoint() {
        return startPoint;
    }

    public Point getEndPoint() {
        return endPoint;
    }
}
