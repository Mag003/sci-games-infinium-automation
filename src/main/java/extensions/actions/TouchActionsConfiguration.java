package extensions.actions;

import com.google.inject.Inject;
import utilities.ISettingsFile;

import java.time.Duration;

public class TouchActionsConfiguration implements ITouchActionsConfiguration {
    private final Duration swipeDuration;
    private final int swipeRetries;
    private final double swipeVerticalOffset;
    private final double swipeHorizontalOffset;
    private final int maxSequenceSize;
    private final int defaultBorderShiftPixels;
    private final double elementShiftPercents;

    @Inject
    public TouchActionsConfiguration(ISettingsFile settingsFile) {
        this.swipeDuration = Duration.ofMillis(Long.parseLong(settingsFile.getValue("/touchActions/swipe/duration").toString()));
        this.swipeRetries = (int) settingsFile.getValue("/touchActions/swipe/retries");
        this.swipeVerticalOffset = (double) settingsFile.getValue("/touchActions/swipe/verticalOffset");
        this.swipeHorizontalOffset = (double) settingsFile.getValue("/touchActions/swipe/horizontalOffset");
        this.maxSequenceSize = (int) settingsFile.getValue("/touchActions/swipe/maxSequenceSize");
        this.defaultBorderShiftPixels = (int) settingsFile.getValue("/touchActions/swipe/defaultBorderShiftPixels");
        this.elementShiftPercents = (double) settingsFile.getValue("/touchActions/swipe/elementShiftPercents");
    }

    @Override
    public int getSwipeRetries() {
        return this.swipeRetries;
    }

    @Override
    public Duration getSwipeDuration() {
        return this.swipeDuration;
    }

    @Override
    public double getSwipeVerticalOffset() {
        return this.swipeVerticalOffset;
    }

    @Override
    public double getSwipeHorizontalOffset() {
        return this.swipeHorizontalOffset;
    }

    @Override
    public int getMaxSequenceSize() {
        return maxSequenceSize;
    }

    @Override
    public int getDefaultBorderShiftPixels() {
        return defaultBorderShiftPixels;
    }

    @Override
    public double getElementShiftPercents() {
        return elementShiftPercents;
    }
}
