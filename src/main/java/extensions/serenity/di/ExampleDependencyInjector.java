package extensions.serenity.di;

import net.serenitybdd.core.di.DependencyInjector;

public class ExampleDependencyInjector implements DependencyInjector {
    @Override
    public void injectDependenciesInto(Object target) {
    }

    @Override
    public void reset() {

    }
}
