package extensions.serenity.fixtures;

import net.thucydides.core.fixtureservices.FixtureException;
import net.thucydides.core.fixtureservices.FixtureService;
import org.openqa.selenium.remote.DesiredCapabilities;

public class ExampleFixtureService implements FixtureService {
    @Override
    public void setup() throws FixtureException {
    }

    @Override
    public void shutdown() throws FixtureException {
    }

    @Override
    public void addCapabilitiesTo(DesiredCapabilities capabilities) {
    }
}
