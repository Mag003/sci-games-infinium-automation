package extensions.factory;

public interface IPageFactory {
    Class<?> getPage(Class<?> clazz);
}
