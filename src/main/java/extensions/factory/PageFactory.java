package extensions.factory;

import net.serenitybdd.core.pages.PageObject;
import org.reflections.Reflections;
import org.reflections.ReflectionsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scigamesinfinium.DiProvider;

import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Set;

public class PageFactory implements IPageFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(PageFactory.class);

    private PageFactory() {
    }

    public static PageFactory getInstance() {
        return new PageFactory();
    }

    @Override
    public Class<?> getPage(Class<?> clazz) {
        String packageName = clazz.getPackage().getName();
        Reflections reflections = new Reflections(packageName);
        try {
            PageTypes pageType = DiProvider.configuration().getPageType();
            Set<Class<?>> pages = new HashSet<>(reflections.getSubTypesOf(PageObject.class));
            return pages.stream()
                    .filter(value -> value.isAnnotationPresent(PageType.class))
                    .filter(value -> value.getAnnotation(PageType.class).name() == pageType)
                    .filter(clazz::isAssignableFrom)
                    .findFirst()
                    .orElse(pages.stream()
                            .filter(page -> page.getName().equalsIgnoreCase(clazz.getName()))
                            .findFirst()
                            .orElse(clazz));
        } catch (ReflectionsException | NoSuchElementException e) {
            LOGGER.warn(String.format("Could not find package \"%s\" with Screens. " +
                    "Please specify value \"screensLocation\" in settings file.", packageName), e);
            return clazz;
        }
    }
}
