package extensions.factory;

import java.util.Arrays;

public enum PageTypes {
    ANDROID
            (
                    "Android",
                    "resource-id",
                    "content-desc",
                    "checked"
            ),
    IOS
            (
                    "iOS",
                    "name",
                    "label",
                    "selected"
            ),
    DESKTOP_WEB
            (
                    "Desktop_Web",
                    "name",
                    "label",
                    "selected"
            ),
    MOBILE_WEB
            (
                    "Mobile_Web",
                    "name",
                    "label",
                    "selected"
            ),
    UNDEFINED();

    private final String filePrefix;
    private final String attributeId;
    private final String attributeLabel;
    private final String attributeChecked;

    PageTypes() {
        String undefined = "Undefined";
        this.filePrefix = undefined;
        this.attributeId = undefined;
        this.attributeLabel = undefined;
        this.attributeChecked = undefined;
    }

    PageTypes(String filePrefix, String attributeId, String attributeLabel, String attributeChecked) {
        this.filePrefix = filePrefix;
        this.attributeId = attributeId;
        this.attributeLabel = attributeLabel;
        this.attributeChecked = attributeChecked;
    }

    public String getFilePrefix() {
        return filePrefix;
    }

    public static PageTypes from(String name) {
        return Arrays.stream(PageTypes.values())
                .filter(value -> value.getFilePrefix().equalsIgnoreCase(name))
                .findFirst()
                .orElse(PageTypes.UNDEFINED);
    }

    public String getAttributeId() {
        return attributeId;
    }

    public String getAttributeLabel() {
        return attributeLabel;
    }

    public String getAttributeChecked() {
        return attributeChecked;
    }
}
