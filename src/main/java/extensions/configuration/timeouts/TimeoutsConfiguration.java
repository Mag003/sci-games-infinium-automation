package extensions.configuration.timeouts;

import utilities.ISettingsFile;

import java.time.Duration;

public class TimeoutsConfiguration implements ITimeoutsConfiguration {
    private final Duration timeoutCondition;
    private final Duration timeoutPolling;

    public TimeoutsConfiguration(ISettingsFile settingsFile) {
        timeoutCondition = Duration.ofSeconds(Long.parseLong(String.valueOf(settingsFile.getValue("/timeouts/condition"))));
        timeoutPolling = Duration.ofMillis(Long.parseLong(String.valueOf(settingsFile.getValue("/timeouts/polling"))));
    }

    public Duration getCondition() {
        return timeoutCondition;
    }

    public Duration getPolling() {
        return timeoutPolling;
    }
}
