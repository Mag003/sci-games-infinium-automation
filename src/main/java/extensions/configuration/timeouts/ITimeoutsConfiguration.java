package extensions.configuration.timeouts;

import java.time.Duration;

public interface ITimeoutsConfiguration {
    Duration getCondition();

    Duration getPolling();
}
