package extensions.configuration;

import com.google.inject.Inject;
import extensions.actions.ITouchActionsConfiguration;
import extensions.actions.TouchActionsConfiguration;
import extensions.configuration.email.EmailConfiguration;
import extensions.configuration.email.IEmailConfiguration;
import extensions.configuration.restassured.IRestAssuredConfiguration;
import extensions.configuration.restassured.RestAssuredConfiguration;
import extensions.configuration.timeouts.ITimeoutsConfiguration;
import extensions.configuration.timeouts.TimeoutsConfiguration;
import extensions.factory.PageTypes;
import extensions.localization.ILocalizationManager;
import extensions.localization.LocalizationManager;
import extensions.retrier.IRetryConfiguration;
import extensions.retrier.RetryConfiguration;
import org.joda.time.DateTimeZone;
import utilities.ISettingsFile;
import utilities.JsonSettingsFile;

import java.time.Duration;
import java.util.Arrays;
import java.util.TimeZone;

public class Configuration implements IConfiguration{
    private  final ITimeoutsConfiguration timeoutsConfiguration;
    private final IEmailConfiguration emailConfiguration;
    private final ILocalizationManager localizationManager;
    private final IRetryConfiguration retryConfiguration;
    private final IRestAssuredConfiguration restAssuredConfiguration;
    private final ITouchActionsConfiguration touchActionsConfiguration;
    private final PageTypes pageType;
    private final DateTimeZone dateTimeZone;

    @Inject
    public Configuration(ISettingsFile settingsFile) {
        JsonSettingsFile jsonSettingsFile = new JsonSettingsFile("config.json");
        this.timeoutsConfiguration = new TimeoutsConfiguration(jsonSettingsFile);
        this.emailConfiguration = new EmailConfiguration(
                String.valueOf(jsonSettingsFile.getValue("/email/email")),
                String.valueOf(jsonSettingsFile.getValue("/email/password")),
                String.valueOf(jsonSettingsFile.getValue("/email/smtp.server")),
                Integer.parseInt(String.valueOf(jsonSettingsFile.getValue("/email/smtp.port"))),
                Arrays.asList(String.valueOf(jsonSettingsFile.getValue("/email/smtp.recipients.default")).split(",")),
                String.valueOf(jsonSettingsFile.getValue("/email/imap.server")),
                String.valueOf(jsonSettingsFile.getValue("/email/imap.protocol")),
                String.valueOf(jsonSettingsFile.getValue("/email/folder.default")),
                Duration.ofMillis(Long.parseLong(String.valueOf(jsonSettingsFile.getValue("/email/wait.timeout"))))
        );
        this.localizationManager = new LocalizationManager(String.valueOf(jsonSettingsFile.getValueOrDefault("/localization", "en")));
        this.retryConfiguration = new RetryConfiguration(jsonSettingsFile);
        this.restAssuredConfiguration = new RestAssuredConfiguration(jsonSettingsFile);
        this.touchActionsConfiguration = new TouchActionsConfiguration(jsonSettingsFile);
        String screenTypePath = "/screens/type";
        if (jsonSettingsFile.isValuePresent(screenTypePath)) {
            this.pageType = PageTypes.from(String.valueOf(jsonSettingsFile.getValue(screenTypePath)));
        } else {
            this.pageType = PageTypes.UNDEFINED;
        }
        this.dateTimeZone = DateTimeZone.forTimeZone(TimeZone.getTimeZone(String.valueOf(jsonSettingsFile.getValue("/timezone"))));
    }

    @Override
    public ITimeoutsConfiguration timeouts() {
        return timeoutsConfiguration;
    }

    @Override
    public IEmailConfiguration email() {
        return emailConfiguration;
    }

    @Override
    public ILocalizationManager localization() {
        return localizationManager;
    }

    @Override
    public IRetryConfiguration retry() {
        return retryConfiguration;
    }

    @Override
    public IRestAssuredConfiguration restAssured() {
        return restAssuredConfiguration;
    }

    @Override
    public ITouchActionsConfiguration touchActions() {
        return touchActionsConfiguration;
    }

    @Override
    public PageTypes getPageType() {
        return pageType;
    }

    @Override
    public DateTimeZone getTimeZone() {
        return dateTimeZone;
    }
}
