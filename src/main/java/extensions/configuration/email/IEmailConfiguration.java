package extensions.configuration.email;

import java.time.Duration;
import java.util.List;

public interface IEmailConfiguration {
    String getEmail();

    String getPassword();

    String getSmtpServer();

    int getSmtpPort();

    List<String> getDefaultRecipients();

    String getImapServer();

    String getImapProtocol();

    String getDefaultFolder();

    Duration getWaitTimeout();
}
