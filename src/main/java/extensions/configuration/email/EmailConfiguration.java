package extensions.configuration.email;

import java.time.Duration;
import java.util.List;

public class EmailConfiguration implements IEmailConfiguration {
    private final String email;
    private final String password;
    private final String smtpServer;
    private final int smtpPort;
    private final List<String> defaultRecipients;

    private final String imapServer;
    private final String imapProtocol;
    private final String defaultFolder;
    private final Duration waitTimeout;

    public EmailConfiguration(String email, String password, String smtpServer, int smtpPort, List<String> defaultRecipients,
                              String imapServer, String imapProtocol, String defaultFolder, Duration waitTimeout) {
        this.email = email;
        this.password = password;
        this.smtpServer = smtpServer;
        this.smtpPort = smtpPort;
        this.defaultRecipients = defaultRecipients;

        this.imapServer = imapServer;
        this.imapProtocol = imapProtocol;
        this.defaultFolder = defaultFolder;
        this.waitTimeout = waitTimeout;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getSmtpServer() {
        return smtpServer;
    }

    @Override
    public int getSmtpPort() {
        return smtpPort;
    }

    @Override
    public List<String> getDefaultRecipients() {
        return defaultRecipients;
    }

    @Override
    public String getImapServer() {
        return imapServer;
    }

    @Override
    public String getImapProtocol() {
        return imapProtocol;
    }

    @Override
    public String getDefaultFolder() {
        return defaultFolder;
    }

    @Override
    public Duration getWaitTimeout() {
        return waitTimeout;
    }
}
