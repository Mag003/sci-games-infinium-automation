package extensions.configuration;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import utilities.ISettingsFile;
import utilities.JsonSettingsFile;

import java.io.File;

public class FrameworkModule extends AbstractModule {
    @Override
    protected void configure() {
        this.bind(ISettingsFile.class).toInstance(getFrameworkSettings());
        this.bind(IConfiguration.class).to(Configuration.class).in(Singleton.class);
    }

    public static ISettingsFile getFrameworkSettings() {
        File configFile = new File("src/test/resources/config.json");
        return new JsonSettingsFile(configFile);
    }
}
