package extensions.configuration;

import extensions.actions.ITouchActionsConfiguration;
import extensions.configuration.email.IEmailConfiguration;
import extensions.configuration.restassured.IRestAssuredConfiguration;
import extensions.configuration.timeouts.ITimeoutsConfiguration;
import extensions.factory.PageTypes;
import extensions.localization.ILocalizationManager;
import extensions.retrier.IRetryConfiguration;
import org.joda.time.DateTimeZone;

public interface IConfiguration {
    ITimeoutsConfiguration timeouts();

    IEmailConfiguration email();

    ILocalizationManager localization();

    IRetryConfiguration retry();

    IRestAssuredConfiguration restAssured();

    ITouchActionsConfiguration touchActions();

    PageTypes getPageType();

    DateTimeZone getTimeZone();
}
