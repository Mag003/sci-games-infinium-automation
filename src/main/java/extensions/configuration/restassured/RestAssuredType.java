package extensions.configuration.restassured;

public enum RestAssuredType {
    SERENITY, DEFAULT;

    public static RestAssuredType getType(String type) {
        return type.equalsIgnoreCase("serenity") ? SERENITY : DEFAULT;
    }
}
