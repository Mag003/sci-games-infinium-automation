package extensions.configuration.restassured;

public interface IRestAssuredConfiguration {
    RestAssuredType getType();
}
