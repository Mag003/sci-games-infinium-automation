package extensions.configuration.restassured;

import utilities.ISettingsFile;

public class RestAssuredConfiguration implements IRestAssuredConfiguration {
    private final RestAssuredType type;

    public RestAssuredConfiguration(ISettingsFile settingsFile) {
        type = RestAssuredType.getType(String.valueOf(settingsFile.getValue("/restassured/type")));
    }

    @Override
    public RestAssuredType getType() {
        return type;
    }
}
