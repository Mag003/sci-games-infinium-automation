package api.impl.login;

import api.impl.Api;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.json.simple.JSONObject;
import utilities.JsonObjectMapper;

public class LoginApi extends Api {

    public OnlineLogin getLogin(String username, String password) {
        JSONObject requestParams = new JSONObject();
        requestParams.put("username", username);
        requestParams.put("password", password);
        requestParams.put("channel", "MOBILE");

        Response response = request()
                .contentType(ContentType.JSON)
                .body(requestParams.toJSONString())
                .post(getApiUrl() + "/players/login");
        return JsonObjectMapper.mapToObject(response.getBody().asString(), OnlineLogin.class);
    }
}