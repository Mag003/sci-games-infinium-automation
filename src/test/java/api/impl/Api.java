package api.impl;

import com.atlassian.oai.validator.restassured.OpenApiValidationFilter;
import extensions.configuration.restassured.RestAssuredType;
import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.rest.SerenityRest;
import scigamesinfinium.DiProvider;

import java.util.Locale;
import java.util.function.Supplier;

public abstract class Api {
    private final Supplier<RequestSpecification> requestSpecificationSupplier;

    /**
     * Serenity method buildThrowable of class net.thucydides.core.model.stacktrace.FailureCause
     * is not able to handle exceptions with non-string/empty constructors
     * (like com.atlassian.oai.validator.restassured.OpenApiValidationException)
     * This is a cause why swagger validation report message is not added to test errors
     * That is why for running validation tests we have to use RestAssured instead of SerenityRest
     * but for integration tests we can use SerenityRest to see requests and responses in the report
     */
    protected Api() {
        RestAssuredType type = DiProvider.configuration().restAssured().getType();
        if (RestAssuredType.SERENITY.equals(type)) {
            requestSpecificationSupplier = SerenityRest::given;
        } else {
            String swaggerUrl = DiProvider.environment().getSwaggerUrl();
            OpenApiValidationFilter validationFilter = new OpenApiValidationFilter(swaggerUrl);
            requestSpecificationSupplier = () -> RestAssured.given().filter(validationFilter);
        }
    }

    protected String getApiUrl() {
        return DiProvider.environment().getApiUrl();
    }

    protected RequestSpecification request() {
        return requestSpecificationSupplier.get().header("Accept", "application/json");
    }

    protected String getAcceptLanguage() {
        Locale locale = DiProvider.configuration().localization().getLocale();
        return locale.getLanguage() + "-" + locale.getCountry();
    }
}
