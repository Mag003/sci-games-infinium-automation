package scigames;

import io.appium.java_client.AppiumDriver;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.webdriver.WebDriverFacade;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import scigamesinfinium.DiProvider;

import java.awt.image.BufferedImage;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

public class SciSteps {
    @Managed
    WebDriver driver;

    public void openUrl() {
        String url = DiProvider.environment().getUrl();
        driver.get(url);
    }

    public void acceptCookie() {
        driver.findElement(By.xpath("//*[contains(@aria-label,'cookie message')]")).click();
    }

    public void closeApp() {
        ((AppiumDriver) ((WebDriverFacade) getDriver()).getProxiedDriver()).closeApp();
    }

    public void launchApp() {
        ((AppiumDriver) ((WebDriverFacade) getDriver()).getProxiedDriver()).launchApp();
    }
}
