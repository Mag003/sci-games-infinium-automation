package scigames;

import com.assertthat.selenium_shutterbug.core.PageSnapshot;
import com.assertthat.selenium_shutterbug.core.Shutterbug;
import com.assertthat.selenium_shutterbug.utils.web.ScrollStrategy;
import extensions.actions.SwipeDirection;
import extensions.actions.TouchActions;
import extensions.configuration.IConfiguration;
import extensions.retrier.ActionRetrier;
import io.appium.java_client.AppiumDriver;
import net.serenitybdd.core.pages.ListOfWebElementFacades;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.core.pages.WebElementState;
import net.thucydides.core.webdriver.WebDriverFacade;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scigamesinfinium.DiProvider;
import scigamesinfinium.environment.SciEnvironment;
import screenshots.ScreenshotHolder;
import utilities.ConditionalWait;
import utilities.StringFunctions;

import java.awt.image.BufferedImage;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;
import java.util.function.Supplier;

import static org.apache.commons.lang3.StringUtils.EMPTY;

public class SciScreen extends PageObject {
    protected static final Logger LOGGER = LoggerFactory.getLogger(SciScreen.class);
    private final IConfiguration configuration = DiProvider.configuration();
    private final ActionRetrier actionRetrier = new ActionRetrier(configuration.retry());
    private final ScreenshotHolder screenshotHolder = ScreenshotHolder.getInstance();

    public WebElementFacade getKeyboardDoneButton() {
        return $(By.name("Done"));
    }

    public String getAlertMessageText() {
        if (SciEnvironment.isIosOS()) {
            return $(By.name("alert_title_textView")).getText();
        } else {
            AtomicReference<String> alertMessage = new AtomicReference<>();
            conditionalWait().waitFor(
                    () -> {
                        String message = StringFunctions.getMatch(getDriver().getPageSource(),
                                "android.widget.Toast.*text=\"(.*?(?=\"))").orElse(EMPTY);
                        alertMessage.set(message);
                        return !message.isEmpty();
                    }, "Alert message should appeared");
            return alertMessage.get();
        }
    }

    public WebElementFacade findElementById(String locator) {
        return SciEnvironment.isMobileWeb() ? $(By.xpath("//*[@id='" + locator + "']")) : $(By.id(locator));
    }

    public String getHexColor(BufferedImage bufferedImage, Point location) {
        return Integer.toHexString(bufferedImage.getRGB(location.x, location.y));
    }

    public void hideMobileKeyboard() {
        ((AppiumDriver) ((WebDriverFacade) getDriver()).getProxiedDriver()).hideKeyboard();
    }

    public boolean areNoElementsFound(Supplier<ListOfWebElementFacades> elementsSupplier) {
        return doWithoutWait(() -> elementsSupplier.get().stream().noneMatch(WebElementState::isPresent));
    }

    protected void swipe(WebElementFacade elementFacade, SwipeDirection direction)  {
        boolean isElementNotDisplayed = areNoElementsFound(() -> new ListOfWebElementFacades(Collections.singletonList(elementFacade)));
        LOGGER.info("element is not displayed: " + isElementNotDisplayed);
        if (isElementNotDisplayed) {
            executeTouch(touchActions -> touchActions.scrollUntil(elementFacade::isDisplayed, direction));
        }
    }

    protected void swipeUntil(BooleanSupplier condition, SwipeDirection direction) {
        executeTouch(touchActions -> touchActions.scrollUntil(condition, direction));
    }

    protected void swipeUntil(BooleanSupplier condition, SwipeDirection direction, int retries) {
        executeTouch(touchActions -> touchActions.scrollUntil(condition, direction, retries));
    }

    public void swipeTo(WebElementFacade elementFrom, WebElementFacade elementTo, SwipeDirection direction) {
        executeTouch(touchActions -> touchActions.swipeToElement(elementFrom, elementTo, direction));
    }

    public void executeTouch(Consumer<TouchActions> action) {
        doWithoutWait(() -> {
            TouchActions touchActions = new TouchActions(((WebDriverFacade) getDriver()).getProxiedDriver());
            action.accept(touchActions);
            return "";
        });
    }

    public <T> T doWithoutWait(Supplier<T> action) {
        setImplicitTimeout(0, ChronoUnit.SECONDS);
        T value = action.get();
        resetImplicitTimeout();
        return value;
    }

    /**
     * takes screenshot of current screen
     *
     * @return buffered image
     */
    public BufferedImage takeScreenshot() {
        PageSnapshot snapshot = Shutterbug.shootPage(getDriver(), ScrollStrategy.VIEWPORT_ONLY, 500);
        BufferedImage image = snapshot.getImage();
        screenshotHolder.addScreenshot(image);
        return image;
    }

    public ConditionalWait conditionalWait() {
        return new ConditionalWait();
    }
}
