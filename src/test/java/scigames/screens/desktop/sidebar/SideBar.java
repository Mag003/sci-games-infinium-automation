package scigames.screens.desktop.sidebar;

import net.serenitybdd.core.pages.ListOfWebElementFacades;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;
import scigames.SciScreen;

import java.util.HashMap;
import java.util.Map;

public class SideBar extends SciScreen {
    private Map<SideTabs, WebElementFacade> getSideTab() {
        ListOfWebElementFacades allTabs = $$(By.xpath("//*[contains(@class, 'MuiList-root')]//descendant::*[contains(@class, 'MuiButtonBase')]"));
        Map<SideTabs, WebElementFacade> footerTab = new HashMap<>();
        footerTab.put(SideTabs.HOME, allTabs.get(0));
        footerTab.put(SideTabs.GAMES, allTabs.get(1));
        footerTab.put(SideTabs.DRAW, allTabs.get(2));
        footerTab.put(SideTabs.PROMOTIONS, allTabs.get(3));
        return footerTab;
    }

    @Step
    public boolean isSideTabSelected(SideTabs tab) {
        System.out.println(getSideTab().get(tab).getAttribute("class"));
        return getSideTab().get(tab).getAttribute("class").contains("selected");
    }

    public enum SideTabs {
        HOME, GAMES, DRAW,
        PROMOTIONS
    }
}
