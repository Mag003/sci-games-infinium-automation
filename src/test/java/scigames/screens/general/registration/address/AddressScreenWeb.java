package scigames.screens.general.registration.address;

import extensions.factory.PageType;
import extensions.factory.PageTypes;
import net.serenitybdd.core.pages.ListOfWebElementFacades;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

@PageType(name = PageTypes.DESKTOP_WEB)
public class AddressScreenWeb extends AddressScreen{
    @Override
    protected WebElementFacade getAddressTitle() {
        return $(By.xpath("//*[contains(@class, 'MuiTypography-h5')]"));
    }

    @Override
    protected WebElementFacade getAddress1LineField() {
        return $(By.xpath("//*[@name='addressLine1']"));
    }

    @Override
    protected WebElementFacade getAddress1LineErrorMessage() {
        return $(By.id("address_line_1_input-helper-text"));
    }

    @Override
    protected WebElementFacade getZipCodeField() {
        return $(By.xpath("//*[@name='zip']"));
    }

    @Override
    protected WebElementFacade getZipCodeErrorMessage() {
        return $(By.id("address_zip_code_input-helper-text"));
    }

    @Override
    protected WebElementFacade getCityField() {
        return $(By.xpath("//*[@name='city']"));
    }

    @Override
    protected WebElementFacade getCityErrorMessage() {
        return $(By.id("address_city_input-helper-text"));
    }

    @Override
    protected WebElementFacade getStateField() {
        return $(By.id("address_state_dropdown"));
    }

    @Override
    protected ListOfWebElementFacades getStateDropdown() {
        return $$(By.xpath("//*[contains(@id, 'A')]"));
    }

    @Override
    protected WebElementFacade getStateErrorMessage() {
        return $(By.id("address_state_dropdown-helper-text"));
    }

    @Override
    protected WebElementFacade getNextButton() {
        return $(By.xpath("//*[@type='submit']"));
    }

    @Override
    public void enterAddress1Line(String address) {
        getAddress1LineField().clear();
        getAddress1LineField().typeAndEnter(address);
    }

    @Override
    public void enterZipCode(String zipCode) {
        getZipCodeField().clear();
        getZipCodeField().typeAndEnter(zipCode);
    }

    @Override
    public void enterCity(String city) {
        getCityField().clear();
        getCityField().typeAndEnter(city);
    }

    @Override
    public void swipeUpToAddress() {
        //exists for mobile device
    }

    @Override
    public String getCityText() {
        return getCityField().getAttribute("value");
    }

    @Override
    public String getZipCodeText() {
        return getZipCodeField().getAttribute("value");
    }
}
