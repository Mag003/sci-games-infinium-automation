package scigames.screens.general.registration.address;

import extensions.actions.SwipeDirection;
import net.serenitybdd.core.pages.ListOfWebElementFacades;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import scigames.SciScreen;

public abstract class AddressScreen extends SciScreen {
    protected abstract WebElementFacade getAddressTitle();

    protected abstract WebElementFacade getAddress1LineField();

    protected abstract WebElementFacade getAddress1LineErrorMessage();

    protected abstract WebElementFacade getZipCodeField();

    protected abstract WebElementFacade getZipCodeErrorMessage();

    protected abstract WebElementFacade getCityField();

    protected abstract WebElementFacade getCityErrorMessage();

    protected abstract WebElementFacade getStateField();

    protected abstract ListOfWebElementFacades getStateDropdown();

    protected abstract WebElementFacade getStateErrorMessage();

    protected abstract WebElementFacade getNextButton();

    @Step
    public boolean isAddressTitleDisplayed() {
        return getAddressTitle().isDisplayed();
    }

    @Step
    public abstract void enterAddress1Line(String address);

    @Step
    public boolean isAddress1LIneErrorMessageDisplayed() {
        return getAddress1LineErrorMessage().isDisplayed();
    }

    @Step
    public abstract void enterZipCode(String zipCode);

    @Step
    public boolean isZipCodeErrorMessageDisplayed() {
        return getZipCodeErrorMessage().isDisplayed();
    }

    @Step
    public abstract void enterCity(String city);

    @Step
    public boolean isCityErrorMessageDisplayed() {
        return getCityErrorMessage().isDisplayed();
    }

    @Step
    public void clickStateField() {
        getStateField().click();
    }

    @Step
    public void selectState() {
        getStateDropdown().get(2).click();
    }

    @Step
    public boolean isStateErrorMessageDisplayed() {
        return getStateErrorMessage().isDisplayed();
    }

    @Step
    public void clickNextButton() {
        swipe(getNextButton(), SwipeDirection.DOWN);
        getNextButton().click();
    }

    @Step
    public abstract void swipeUpToAddress();

    @Step
    public abstract String getCityText();

    @Step
    public abstract String getZipCodeText();
}
