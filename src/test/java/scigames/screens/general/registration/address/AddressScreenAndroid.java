package scigames.screens.general.registration.address;

import extensions.actions.SwipeAction;
import extensions.factory.PageType;
import extensions.factory.PageTypes;
import net.serenitybdd.core.pages.ListOfWebElementFacades;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;

@PageType(name = PageTypes.ANDROID)
public class AddressScreenAndroid extends AddressScreen{
    @Override
    protected WebElementFacade getAddressTitle() {
        return $(By.id("registration_address_title_label"));
    }

    @Override
    protected WebElementFacade getAddress1LineField() {
        return $(By.id("address_line_1_input"));
    }

    @Override
    protected WebElementFacade getAddress1LineErrorMessage() {
        //TODO: add after adding unique ID
        return $(By.id(""));
    }

    @Override
    protected WebElementFacade getZipCodeField() {
        return $(By.id("address_zip_code_input"));
    }

    @Override
    protected WebElementFacade getZipCodeErrorMessage() {
        //TODO: add after adding unique ID
        return $(By.id(""));
    }

    @Override
    protected WebElementFacade getCityField() {
        return $(By.id("address_city_input"));
    }

    @Override
    protected WebElementFacade getCityErrorMessage() {
        //TODO: add after adding unique ID
        return $(By.id(""));
    }

    @Override
    protected WebElementFacade getStateField() {
        //TODO: add after adding unique ID
        return $(By.id("address_state_dropdown"));
    }

    @Override
    protected ListOfWebElementFacades getStateDropdown() {
        return $$(By.id("dropdown_textview"));
    }

    @Override
    protected WebElementFacade getStateErrorMessage() {
        //TODO: add after adding unique ID
        return $(By.id(""));
    }

    @Override
    protected WebElementFacade getNextButton() {
        return $(By.id("address_next_button"));
    }

    @Override
    public void enterAddress1Line(String address) {
        getAddress1LineField().type(address);
        getAddress1LineField().click();
        $(By.id("registration_toolbar")).click();
    }

    @Override
    public void enterZipCode(String zipCode) {
        getZipCodeField().type(zipCode);
        getZipCodeField().click();
        $(By.id("address_zip_code_label")).click();
    }

    @Override
    public void enterCity(String city) {
        getCityField().type(city);
        getCityField().click();
        $(By.id("address_city_label")).click();
    }

    @Override
    public void swipeUpToAddress() {
        Point location = getZipCodeField().getLocation();
        executeTouch(touchActions -> touchActions.swipe(new SwipeAction(new Point(location.x, location.y - 400), new Point(location.x, location.y + 400))));
    }

    @Override
    public String getCityText() {
        return getCityField().getText();
    }

    @Override
    public String getZipCodeText() {
        return getZipCodeField().getText();
    }
}
