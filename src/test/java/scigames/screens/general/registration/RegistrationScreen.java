package scigames.screens.general.registration;

import net.serenitybdd.core.pages.ListOfWebElementFacades;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import scigames.SciScreen;

import java.util.List;
import java.util.stream.Collectors;


public abstract class RegistrationScreen extends SciScreen {
    protected abstract ListOfWebElementFacades getStepContainers();

    protected abstract WebElementFacade getValidationMessage();

    @Step
    public List<Point> getStepLocations() {
        return getStepContainers().stream().map(WebElement::getLocation).collect(Collectors.toList());
    }

    @Step
    public abstract List<String> getStepColor();

    @Step
    public boolean isValidationMessageDisplayed() {
        return getValidationMessage().isDisplayed();
    }
}
