package scigames.screens.general.registration;

import extensions.factory.PageType;
import extensions.factory.PageTypes;
import net.serenitybdd.core.pages.ListOfWebElementFacades;
import net.serenitybdd.core.pages.WebElementFacade;

import java.util.List;

@PageType(name = PageTypes.ANDROID)
public class RegistrationScreenAndroid extends RegistrationScreen{
    @Override
    protected ListOfWebElementFacades getStepContainers() {
        //TODO: add after adding IDs
        return null;
    }

    @Override
    protected WebElementFacade getValidationMessage() {
        return null;
    }

    @Override
    public List<String> getStepColor() {
        //TODO: add after adding IDs
        return null;
    }
}
