package scigames.screens.general.registration.personalinfo;

import extensions.factory.PageType;
import extensions.factory.PageTypes;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

@PageType(name = PageTypes.DESKTOP_WEB)
public class PersonalInfoScreenWeb extends PersonalInfoScreen {
    @Override
    protected WebElementFacade getPersonalInfoTitle() {
        return $(By.xpath("//*[contains(@class, 'MuiTypography-h5')]"));
    }

    @Override
    protected WebElementFacade getFirstNameField() {
        return $(By.id("registration_first_name_input_field"));
    }

    @Override
    protected WebElementFacade getFirstNameErrorMessage() {
        return $(By.id("registration_first_name_input_field-helper-text"));
    }

    @Override
    protected WebElementFacade getLastNameField() {
        return $(By.xpath("//*[@name='lastName']"));
    }

    @Override
    protected WebElementFacade getLastNameErrorMessage() {
        return $(By.id("registration_last_name_input_field-helper-text"));
    }

    @Override
    protected WebElementFacade getMonthField() {
        return $(By.id("registration_dob_month_input_field"));
    }

    @Override
    protected WebElementFacade getDayField() {
        return $(By.id("registration_dob_day_input_field"));
    }

    @Override
    protected WebElementFacade getYearField() {
        return $(By.id("registration_dob_year_input_field"));
    }

    @Override
    protected WebElementFacade getDateOfBirthErrorMessage() {
        return $(By.id("registration_date_error_label"));
    }

    @Override
    protected WebElementFacade getMale() {
        return $(By.xpath("(//*[@id='registration_not_to_say_radio_button'])[1]"));
    }

    @Override
    protected WebElementFacade getFemale() {
        return $(By.xpath("(//*[@id='registration_not_to_say_radio_button'])[2]"));
    }

    @Override
    protected WebElementFacade getPreferNotToSay() {
        return $(By.xpath("(//*[@id='registration_not_to_say_radio_button'])[3]"));
    }

    @Override
    protected WebElementFacade getNextButton() {
        return $(By.id("registration_personal_details_submit_button"));
    }

    @Override
    protected WebElementFacade getGenderErrorMessage() {
        return $(By.id("registration_gender_error_label"));
    }

    @Override
    protected WebElementFacade getMobile() {
        return $(By.id("telephone_input_mobile_number_label"));
    }

    @Override
    protected WebElementFacade getMobileNumberField() {
        return $(By.id("telephone_input_landline_input_field"));
    }

    @Override
    protected WebElementFacade getMobileErrorMessage() {
        return $(By.id("telephone_input_landline_input_field-helper-text"));
    }

    @Override
    public void enterFirstName(String name) {
        getFirstNameField().clear();
        getFirstNameField().typeAndEnter(name);
    }

    @Override
    public void enterLastName(String lastName) {
        getLastNameField().clear();
        getLastNameField().typeAndEnter(lastName);
    }

    @Override
    public void enterDateOfBirth(String month, String day, String year) {
        getMonthField().clear();
        getMonthField().type(month);

        getDayField().clear();
        getDayField().type(day);

        getYearField().clear();
        getYearField().typeAndEnter(year);
    }

    @Override
    public void enterMobileNumber(String number) {
        getMobileNumberField().clear();
        getMobileNumberField().typeAndEnter(number);
    }

    @Override
    public void swipeUpToEmail() {
        //exists for mobile device
    }

    @Override
    public String getFirstNameText() {
        return getFirstNameField().getAttribute("value");
    }

    @Override
    public String getLastNameText() {
        return getLastNameField().getAttribute("value");
    }
}
