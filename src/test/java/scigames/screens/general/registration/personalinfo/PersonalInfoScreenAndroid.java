package scigames.screens.general.registration.personalinfo;

import extensions.actions.SwipeAction;
import extensions.actions.SwipeDirection;
import extensions.factory.PageType;
import extensions.factory.PageTypes;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;

@PageType(name = PageTypes.ANDROID)
public class PersonalInfoScreenAndroid extends PersonalInfoScreen{
    @Override
    protected WebElementFacade getPersonalInfoTitle() {
        return $(By.id("registration_two_title_label"));
    }

    @Override
    protected WebElementFacade getFirstNameField() {
        return $(By.id("registration_first_name_input_field"));
    }

    @Override
    protected WebElementFacade getFirstNameErrorMessage() {
        return $(By.id(""));
    }

    @Override
    protected WebElementFacade getLastNameField() {
        return $(By.id("registration_last_name_input_field"));
    }

    @Override
    protected WebElementFacade getLastNameErrorMessage() {
        return $(By.id(""));
    }

    @Override
    protected WebElementFacade getMonthField() {
        return $(By.id("registration_dob_month_input_field"));
    }

    @Override
    protected WebElementFacade getDayField() {
        return $(By.id("registration_dob_day_input_field"));
    }

    @Override
    protected WebElementFacade getYearField() {
        return $(By.id("registration_dob_year_input_field"));
    }

    @Override
    protected WebElementFacade getDateOfBirthErrorMessage() {
        return $(By.id("registration_date_error_label"));
    }

    @Override
    protected WebElementFacade getMale() {
        return $(By.xpath("//android.widget.RadioButton[1]"));
    }

    @Override
    protected WebElementFacade getFemale() {
        return $(By.xpath("//android.widget.RadioButton[2]"));
    }

    @Override
    protected WebElementFacade getPreferNotToSay() {
        return $(By.xpath("//android.widget.RadioButton[2]"));
    }

    @Override
    protected WebElementFacade getNextButton() {
        return $(By.id("registration_personal_details_submit_button"));
    }

    @Override
    protected WebElementFacade getGenderErrorMessage() {
        return $(By.id("registration_gender_error_label"));
    }

    @Override
    protected WebElementFacade getMobile() {
        return $(By.id("telephone_input_mobile_radio_button"));
    }

    @Override
    protected WebElementFacade getMobileNumberField() {
        return $(By.id("telephone_input_mobile_input_field"));
    }

    @Override
    protected WebElementFacade getMobileErrorMessage() {
        return $(By.id(""));
    }

    @Override
    public void enterFirstName(String name) {
        getFirstNameField().type(name);
        getFirstNameField().click();
        $(By.id("registration_toolbar")).click();
    }

    @Override
    public void enterLastName(String lastName) {
        getLastNameField().type(lastName);
        getLastNameField().click();
        $(By.id("registration_last_name_label")).click();
    }

    @Override
    public void enterDateOfBirth(String month, String day, String year) {
        getMonthField().type(month);
        getDayField().type(day);
        getYearField().type(year);
        getYearField().click();
        $(By.id("registration_dob_hint_label")).click();
    }

    @Override
    public void enterMobileNumber(String number) {
        swipe(getMobileNumberField(), SwipeDirection.DOWN);
        getMobileNumberField().type(number);
        getMobileNumberField().click();
        $(By.id("telephone_input_mobile_number_label")).click();
    }

    @Override
    public void swipeUpToEmail() {
        Point location = $(By.id("registration_month_layout")).getLocation();
        executeTouch(touchActions -> touchActions.swipe(new SwipeAction(new Point(location.x, location.y - 400), new Point(location.x, location.y + 400))));
    }

    @Override
    public String getFirstNameText() {
        return getFirstNameField().getText();
    }

    @Override
    public String getLastNameText() {
        return getLastNameField().getText();
    }
}
