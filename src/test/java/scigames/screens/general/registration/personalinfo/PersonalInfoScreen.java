package scigames.screens.general.registration.personalinfo;

import extensions.actions.SwipeDirection;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import scigames.SciScreen;

public abstract class PersonalInfoScreen extends SciScreen {
    protected abstract WebElementFacade getPersonalInfoTitle();

    protected abstract WebElementFacade getFirstNameField();

    protected abstract WebElementFacade getFirstNameErrorMessage();

    protected abstract WebElementFacade getLastNameField();

    protected abstract WebElementFacade getLastNameErrorMessage();

    protected abstract WebElementFacade getMonthField();

    protected abstract WebElementFacade getDayField();

    protected abstract WebElementFacade getYearField();

    protected abstract WebElementFacade getDateOfBirthErrorMessage();

    protected abstract WebElementFacade getMale();

    protected abstract WebElementFacade getFemale();

    protected abstract WebElementFacade getPreferNotToSay();

    protected abstract WebElementFacade getNextButton();

    protected abstract WebElementFacade getGenderErrorMessage();

    protected abstract WebElementFacade getMobile();

    protected abstract WebElementFacade getMobileNumberField();

    protected abstract WebElementFacade getMobileErrorMessage();

    @Step
    public boolean isPersonalInfoTitleDisplayed() {
        return getPersonalInfoTitle().isDisplayed();
    }

    @Step
    public abstract void enterFirstName(String name);

    @Step
    public boolean isFirstNameErrorMessageDisplayed() {
        return getFirstNameErrorMessage().isDisplayed();
    }

    @Step
    public abstract void enterLastName(String lastName);

    @Step
    public boolean isLastNameErrorMessageDisplayed() {
        return getLastNameErrorMessage().isDisplayed();
    }

    @Step
    public abstract void enterDateOfBirth(String month, String day, String year);

    @Step
    public boolean isGendersDisplayed() {
        return getMale().isDisplayed() && getFemale().isDisplayed() && getPreferNotToSay().isDisplayed();
    }

    @Step
    public void clickNextButton() {
        swipe(getNextButton(), SwipeDirection.DOWN);
        getNextButton().click();
    }

    @Step
    public boolean isGenderErrorMessageDisplayed() {
        return getGenderErrorMessage().isDisplayed();
    }

    @Step
    public void clickMobileRadioButton() {
        swipe(getMobile(), SwipeDirection.DOWN);
        getMobile().click();
    }

    @Step
    public abstract void enterMobileNumber(String number);

    @Step
    public boolean isMobileNumberErrorDisplayed() {
        return getMobileErrorMessage().isDisplayed();
    }

    @Step
    public boolean isDateOfBirthErrorMessageDisplayed() {
        return getDateOfBirthErrorMessage().isDisplayed();
    }

    @Step
    public void selectFemale() {
        getFemale().click();
    }

    @Step
    public abstract void swipeUpToEmail();

    @Step
    public abstract String getFirstNameText();

    @Step
    public abstract String getLastNameText();
}
