package scigames.screens.general.registration.createaccount;

import extensions.actions.SwipeAction;
import extensions.actions.SwipeDirection;
import extensions.factory.PageType;
import extensions.factory.PageTypes;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;

@PageType(name = PageTypes.ANDROID)
public class CreateAccountScreenAndroid extends CreateAccountScreen {
    @Override
    protected WebElementFacade getPromoCodeField() {
        return $(By.id("registration_promo_code_input_field"));
    }

    @Override
    protected WebElementFacade getCreateAccountTitle() {
        return $(By.id("registration_one_title_label"));
    }

    @Override
    protected WebElementFacade getHelpButton() {
        return $(By.id("registration_help_button"));
    }

    @Override
    protected WebElementFacade getShowPasswordButton() {
        return $(By.id("text_input_end_icon"));
    }

    @Override
    protected WebElementFacade getConfirmButton() {
        return $(By.id("registration_marketing_confirm_radio_button"));
    }

    @Override
    protected WebElementFacade getDenyButton() {
        return $(By.id("registration_marketing_deny_radio_button"));
    }

    @Override
    protected WebElementFacade getEmailErrorMessage() {
        return $(By.id("textinput_error"));
    }

    @Override
    protected WebElementFacade getPasswordErrorMessage() {
        return $(By.id("password_strength_instructions_label"));
    }

    @Override
    protected WebElementFacade getPrivacyCheckBox() {
        return $(By.id("registration_t_and_c_check_box"));
    }

    @Override
    public void enterEmail(String email) {
        swipe(getEmailField(), SwipeDirection.UP);
        getEmailField().type(email);
        getEmailField().click();
        getRegistrationToolBar().click();
    }

    @Override
    public void enterPassword(String password) {
        getPasswordField().type(password);
        getPasswordField().click();
        getRegistrationToolBar().click();
    }

    @Override
    public boolean isEmailErrorMessageDisplayed() {
        Point location = getReceiveInfoSection().getLocation();
        executeTouch(touchActions -> touchActions.swipe(new SwipeAction(new Point(location.x, location.y), new Point(location.x, location.y + 500))));
        return getEmailErrorMessage().isDisplayed();
    }

    @Override
    public void enterPromoCode(String promoCode) {
        getPromoCodeField().type(promoCode);
        getPromoCodeField().click();
        getPasswordStrength().click();
    }

    @Override
    public void acceptPrivacy() {
        swipe(getPrivacyCheckBox(), SwipeDirection.DOWN);
        getPrivacyCheckBox().click();
    }
}
