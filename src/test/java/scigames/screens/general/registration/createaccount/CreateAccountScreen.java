package scigames.screens.general.registration.createaccount;

import extensions.actions.SwipeDirection;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;
import scigames.SciScreen;
import scigamesinfinium.DiProvider;

public abstract class CreateAccountScreen extends SciScreen {
    protected abstract WebElementFacade getPromoCodeField();

    protected WebElementFacade getPasswordStrength() {
        return $(By.xpath("//*[contains(@" + DiProvider.configuration().getPageType().getAttributeId() + ", 'password_strength')]"));
    }

    protected abstract WebElementFacade getCreateAccountTitle();

    protected abstract WebElementFacade getHelpButton();

    protected abstract WebElementFacade getShowPasswordButton();

    protected abstract WebElementFacade getConfirmButton();

    protected abstract WebElementFacade getDenyButton();

    protected abstract WebElementFacade getEmailErrorMessage();

    protected abstract WebElementFacade getPasswordErrorMessage();

    protected abstract WebElementFacade getPrivacyCheckBox();

    protected WebElementFacade getEmailField() {
        return $(By.id("registration_email_input_field"));
    }

    protected WebElementFacade getPasswordField() {
        return $(By.id("registration_password_input_field"));
    }

    protected WebElementFacade getRegistrationToolBar() {
        return $(By.id("registration_toolbar"));
    }

    protected WebElementFacade getReceiveInfoSection() {
        return $(By.id("registration_marketing_layout"));
    }

    private WebElementFacade getPromoCodeLabel() {
        return $(By.id("registration_promotion_code_label"));
    }

    private WebElementFacade getReceiveInfoTitle() {
        return $(By.id("registration_marketing_title_label"));
    }

    private WebElementFacade getPrivacyErrorMessage() {
        return $(By.id("registration_terms_error_label"));
    }

    private WebElementFacade getApplyPromoCodeButton() {
        return $(By.id("registration_promo_code_apply_button"));
    }

    private WebElementFacade getStartedButton() {
        return $(By.id("registration_one_submit_button"));
    }

    private WebElementFacade getPromoCodeErrorMessage() {
        return $(By.id("registration_promo_code_error_message"));
    }

    @Step
    public boolean isCreateAccountTitleDisplayed() {
        return getCreateAccountTitle().isDisplayed();
    }

    @Step
    public boolean isHelpButtonDisplayed() {
        return getHelpButton().isDisplayed();
    }

    @Step
    public String getPromoCodeLabelText() {
        return getPromoCodeLabel().getText();
    }

    @Step
    public boolean isShowPasswordButtonDisplayed() {
        return getShowPasswordButton().isDisplayed();
    }

    @Step
    public boolean isReceiveSectionDisplayed() {
        return getReceiveInfoTitle().isDisplayed() && getConfirmButton().isDisplayed() && getDenyButton().isDisplayed();
    }

    @Step
    public abstract void enterEmail(String email);

    @Step
    public void clickGetStartedButton() {
        swipe(getStartedButton(), SwipeDirection.DOWN);
        getStartedButton().click();
    }

    @Step
    public abstract void enterPassword(String password);

    @Step
    public boolean isPasswordStrengthDisplayed() {
        return getPasswordStrength().isDisplayed();
    }

    @Step
    public abstract boolean isEmailErrorMessageDisplayed();

    @Step
    public boolean isPasswordErrorMessageDisplayed() {
        return getPasswordErrorMessage().isDisplayed();
    }

    @Step
    public boolean isPrivacyErrorMessageDisplayed() {
        swipe(getStartedButton(), SwipeDirection.DOWN);
        return getPrivacyErrorMessage().isDisplayed();
    }

    @Step
    public abstract void enterPromoCode(String promoCode);

    @Step
    public void clickApplyPromoCodeButton() {
        getApplyPromoCodeButton().click();
    }

    @Step
    public abstract void acceptPrivacy();

    @Step
    public String getEmailFieldText() {
        return getEmailField().getText();
    }

    @Step
    public String getPasswordFieldText() {
        return getEmailField().getText();
    }

    @Step
    public String getPromoCodeFieldText() {
        return getPromoCodeField().getText();
    }

    @Step
    public boolean isPromoCodeErrorMessageDisplayed() {
        return getPromoCodeErrorMessage().isDisplayed();
    }
}
