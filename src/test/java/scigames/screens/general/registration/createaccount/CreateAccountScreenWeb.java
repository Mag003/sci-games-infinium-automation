package scigames.screens.general.registration.createaccount;

import extensions.factory.PageType;
import extensions.factory.PageTypes;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

@PageType(name = PageTypes.DESKTOP_WEB)
public class CreateAccountScreenWeb extends CreateAccountScreen{
    @Override
    protected WebElementFacade getPromoCodeField() {
        return $(By.id("registration_promo_code_input_field"));
    }

    @Override
    protected WebElementFacade getCreateAccountTitle() {
        return $(By.id("dot_item_title_text_Account"));
    }

    @Override
    protected WebElementFacade getHelpButton() {
        return $(By.xpath("//*[contains(@class, 'jss31')]"));
    }

    @Override
    protected WebElementFacade getShowPasswordButton() {
        return $(By.xpath("//*[contains(@aria-label, 'password')]"));
    }

    @Override
    protected WebElementFacade getConfirmButton() {
        return $(By.xpath("(//*[contains(@id, 'registration_not_to_say_radio_button')])[1]"));
    }

    @Override
    protected WebElementFacade getDenyButton() {
        return $(By.xpath("(//*[contains(@id, 'registration_not_to_say_radio_button')])[2]"));
    }

    @Override
    protected WebElementFacade getEmailErrorMessage() {
        return $(By.xpath("//*[@id='registration_email_input_field-helper-text' or @id='email_error_message']"));
    }

    @Override
    protected WebElementFacade getPasswordErrorMessage() {
        return $(By.id("password_strength_instructions_label"));
    }

    @Override
    protected WebElementFacade getPrivacyCheckBox() {
        return $(By.xpath("//*[contains(@class, 'jss251')]"));
    }

    @Override
    public void enterEmail(String email) {
        getEmailField().clear();
        getEmailField().type(email);
    }

    @Override
    public void enterPassword(String password) {
        getPasswordField().clear();
        getPasswordField().type(password);
    }

    @Override
    public boolean isEmailErrorMessageDisplayed() {
        return getEmailErrorMessage().isDisplayed();
    }

    @Override
    public void enterPromoCode(String promoCode) {
        getPromoCodeField().clear();
        getPromoCodeField().type(promoCode);
    }

    @Override
    public void acceptPrivacy() {
        getPrivacyCheckBox().click();
    }
}
