package scigames.screens.general.registration.createaccount;

import extensions.actions.SwipeDirection;
import extensions.factory.PageType;
import extensions.factory.PageTypes;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

@PageType(name = PageTypes.IOS)
public class CreateAccountIos extends CreateAccountScreen {
    @Override
    protected WebElementFacade getPromoCodeField() {
        //TODO: update name for this element
        return $(By.id("password_textField"));
    }

    @Override
    protected WebElementFacade getCreateAccountTitle() {
        return $(By.id("registration_title"));
    }

    @Override
    protected WebElementFacade getHelpButton() {
        return $(By.id("help_button"));
    }

    @Override
    protected WebElementFacade getShowPasswordButton() {
        return $(By.id("show_password"));
    }

    @Override
    protected WebElementFacade getConfirmButton() {
        //TODO: add name for this element
        return $(By.name("Send Me Offers"));
    }

    @Override
    protected WebElementFacade getDenyButton() {
        //TODO: add name for this element
        return $(By.name("No Thanks"));
    }

    @Override
    protected WebElementFacade getEmailErrorMessage() {
        return $(By.id("email_error_message"));
    }

    @Override
    protected WebElementFacade getPasswordErrorMessage() {
        return $(By.id("password_strength_description_label"));
    }

    @Override
    protected WebElementFacade getPrivacyCheckBox() {
        return $(By.xpath("//*[@name='registration_t_and_c_check_box']//descendant:: XCUIElementTypeButton"));
    }

    @Override
    public void enterEmail(String email) {
        swipe(getEmailField(), SwipeDirection.UP);
        getEmailField().type(email);
        getKeyboardDoneButton().click();
    }

    @Override
    public void enterPassword(String password) {
        getPasswordField().type(password);
        getKeyboardDoneButton().click();
    }

    @Override
    public boolean isEmailErrorMessageDisplayed() {
        swipe(getEmailField(), SwipeDirection.UP);
        return getEmailErrorMessage().isDisplayed();
    }

    @Override
    public void enterPromoCode(String promoCode) {
        getPromoCodeField().type(promoCode);
        getKeyboardDoneButton().click();
    }

    @Override
    public void acceptPrivacy() {
        swipe(getPrivacyCheckBox(), SwipeDirection.DOWN);
        getPrivacyCheckBox().click();
    }
}
