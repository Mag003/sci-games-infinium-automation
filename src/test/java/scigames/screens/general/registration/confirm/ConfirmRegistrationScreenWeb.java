package scigames.screens.general.registration.confirm;

import extensions.factory.PageType;
import extensions.factory.PageTypes;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

@PageType(name = PageTypes.DESKTOP_WEB)
public class ConfirmRegistrationScreenWeb extends ConfirmRegistrationScreen {
    @Override
    protected WebElementFacade getConfirmRegistrationTitle() {
        return $(By.id("confirm_title_label"));
    }

    @Override
    protected WebElementFacade getPersonalInfo() {
        return $(By.id("confirm_personal_info_data_label"));
    }

    @Override
    protected WebElementFacade getAddressInfo() {
        return $(By.id("confirm_address_data_label"));
    }

    @Override
    protected WebElementFacade getCompletedButton() {
        return $(By.id("confirm_next_registration_button"));
    }

    @Override
    protected WebElementFacade getSsnErrorMessage() {
        return $(By.id("//*[@class='jss337']"));
    }
}
