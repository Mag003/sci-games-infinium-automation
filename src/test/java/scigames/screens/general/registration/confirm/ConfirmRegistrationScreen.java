package scigames.screens.general.registration.confirm;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import scigames.SciScreen;

public abstract class ConfirmRegistrationScreen extends SciScreen {
    protected abstract WebElementFacade getConfirmRegistrationTitle();

    protected abstract WebElementFacade getPersonalInfo();

    protected abstract WebElementFacade getAddressInfo();

    protected abstract WebElementFacade getCompletedButton();

    protected abstract WebElementFacade getSsnErrorMessage();

    @Step
    public boolean isConfirmRegistrationTitleDisplayed() {
        return getConfirmRegistrationTitle().isDisplayed();
    }

    @Step
    public String getPersonalInfoText() {
        return getPersonalInfo().getText();
    }

    @Step
    public String getAddressInfoText() {
        return getAddressInfo().getText();
    }

    @Step
    public void clickCompletedButton() {
        getCompletedButton().click();
    }

    @Step
    public boolean isSsnErrorMessageDisplayed() {
        return getSsnErrorMessage().isDisplayed();
    }
}
