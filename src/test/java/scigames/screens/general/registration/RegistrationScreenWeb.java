package scigames.screens.general.registration;

import extensions.factory.PageType;
import extensions.factory.PageTypes;
import net.serenitybdd.core.pages.ListOfWebElementFacades;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

import java.util.List;
import java.util.stream.Collectors;

@PageType(name = PageTypes.DESKTOP_WEB)
public class RegistrationScreenWeb extends RegistrationScreen {
    @Override
    protected ListOfWebElementFacades getStepContainers() {
        //TODO: fix after adding IDs
        return $$(By.xpath("//*[contains(@class, 'iconContainer')]/svg/circle"));
    }

    @Override
    protected WebElementFacade getValidationMessage() {
        return $(By.xpath("//*[contains(@class, 'jss279')]"));
    }

    @Override
    public List<String> getStepColor() {
        return getStepContainers().stream().map(step -> step.getCssValue("color")).collect(Collectors.toList());
    }
}
