package scigames.screens.general.myaccount;

import extensions.factory.PageType;
import extensions.factory.PageTypes;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

@PageType(name = PageTypes.DESKTOP_WEB)
public class MyAccountScreenWeb extends MyAccountScreen{
    @Override
    protected WebElementFacade getMyAccountRoot() {
        return $(By.xpath("//*[contains(@class, 'paperAnchorRight')]"));
    }

    @Override
    protected WebElementFacade getLogoutButton() {
        return $(By.xpath("(//*[contains(@class, 'paperAnchorRight')]//descendant::*[@type='button'])[6]//span[1]"));
    }
}
