package scigames.screens.general.myaccount;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import scigames.SciScreen;

public abstract class MyAccountScreen extends SciScreen {
    protected abstract WebElementFacade getMyAccountRoot();

    protected abstract WebElementFacade getLogoutButton();

    @Step
    public boolean isMyAccountRootDisplayed() {
        return getMyAccountRoot().isDisplayed();
    }

    @Step
    public void clickLogoutButton() {
        getLogoutButton().click();
    }
}
