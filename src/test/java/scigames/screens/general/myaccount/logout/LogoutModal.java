package scigames.screens.general.myaccount.logout;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;
import scigames.SciScreen;

public class LogoutModal extends SciScreen {
    private WebElementFacade getLogoutRoot() {
        return $(By.xpath("//*[contains(@aria-describedby, 'logout')]"));
    }

    private WebElementFacade getCancelButton() {
        return $(By.xpath("(//*[contains(@aria-describedby, 'logout')]//descendant::button)[1]"));
    }

    private WebElementFacade getLogoutButton() {
        return $(By.xpath("(//*[contains(@aria-describedby, 'logout')]//descendant::button)[2]"));
    }

    @Step
    public boolean isLogoutModalDisplayed() {
        return getLogoutRoot().isDisplayed();
    }

    @Step
    public void clickCancelButton() {
        getCancelButton().click();
    }

    @Step
    public void clickLogoutButton() {
        getLogoutButton().click();
    }
}
