package scigames.screens.general.help;

import scigames.SciScreen;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;

public abstract class HelpScreen extends SciScreen {
    protected abstract WebElementFacade getHelpRoot();

    private WebElementFacade getContactUsButton() {
        return $(By.id("help_contact_button"));
    }

    private WebElementFacade getGoToLiveChatButton() {
        return $(By.id("help_live_chat_button"));
    }

    @Step
    public boolean isHelpDisplayed() {
        return getHelpRoot().isDisplayed();
    }

    @Step
    public boolean isContactUsButtonDisplayed() {
        return getContactUsButton().isDisplayed();
    }

    @Step
    public boolean isLiveChatButtonDisplayed() {
        return getGoToLiveChatButton().isDisplayed();
    }
}
