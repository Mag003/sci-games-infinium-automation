package scigames.screens.general.help;

import extensions.factory.PageType;
import extensions.factory.PageTypes;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

@PageType(name = PageTypes.ANDROID)
public class HelpScreenAndroid extends HelpScreen {
    @Override
    protected WebElementFacade getHelpRoot() {
        return $(By.id("help_root"));
    }
}
