package scigames.screens.general.welcome;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;
import scigames.SciScreen;

public class WelcomeModal extends SciScreen {
    private WebElementFacade getWelcomeTitle() {
        return $(By.xpath("//*[contains(@class, 'jss182')]"));
    }

    private WebElementFacade getCashBalance() {
        return $(By.xpath("(//*[contains(@class, 'jss185')])[2]"));
    }

    private WebElementFacade getTotalBalance() {
        return $(By.xpath("(//*[contains(@class, 'jss185')])[1]"));
    }

    private WebElementFacade getBonusBalance() {
        return $(By.xpath("(//*[contains(@class, 'jss185')])[3]"));
    }

    private WebElementFacade getContinueButton() {
        return $(By.xpath("//*[contains(@class, 'jss35')]"));
    }

    private WebElementFacade getDepositButton() {
        return $(By.xpath("(//*[contains(@class, 'jss31')])[1]"));
    }

    private WebElementFacade getClaimButton() {
        return $(By.xpath("(//*[contains(@class, 'jss33')])[2]"));
    }

    @Step
    public boolean isWelcomeTitleDisplayed() {
        return getWelcomeTitle().isDisplayed();
    }

    @Step
    public boolean isCashBalanceDisplayed() {
        return getCashBalance().isDisplayed();
    }

    @Step
    public boolean isTotalBalanceDisplayed() {
        return getTotalBalance().isDisplayed();
    }

    @Step
    public boolean isBonusBalanceDisplayed() {
        return getBonusBalance().isDisplayed();
    }

    @Step
    public boolean isContinueButtonDisplayed() {
        return getContinueButton().isDisplayed();
    }

    @Step
    public void clickDepositButton() {
        getDepositButton().click();
    }

    @Step
    public void clickClaimButton() {
        getClaimButton().click();
    }

    @Step
    public void clickContinueButton() {
        getContinueButton().click();
    }
}
