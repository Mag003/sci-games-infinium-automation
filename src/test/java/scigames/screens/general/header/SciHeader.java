package scigames.screens.general.header;

import scigames.SciScreen;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;

public class SciHeader extends SciScreen {
    private WebElementFacade getLoginButton() {
        return findElementById("login_button");
    }

    private WebElementFacade getJoinButton() {
        return findElementById("join_button");
    }

    private WebElementFacade getMyAccountButton() {
        return $(By.xpath("//*[contains(@class, 'jss15')]"));
    }

    @Step
    public void clickLoginButton() {
        getLoginButton().click();
    }

    @Step
    public void clickJoinButton() {
        getJoinButton().click();
    }

    @Step
    public void clickMyProfileButton() {
        getMyAccountButton().click();
    }
}
