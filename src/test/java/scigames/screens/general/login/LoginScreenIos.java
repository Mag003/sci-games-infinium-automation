package scigames.screens.general.login;

import extensions.factory.PageType;
import extensions.factory.PageTypes;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

@PageType(name = PageTypes.IOS)
public class LoginScreenIos extends LoginScreen {
    @Override
    protected WebElementFacade getEmailValidationMessage() {
        return $(By.name("email_fieldValidationMessage"));
    }

    @Override
    protected WebElementFacade getPasswordValidationMessage() {
        return $(By.name("password_fieldValidationMessage"));
    }

    @Override
    protected WebElementFacade getLoginButton() {
        return $(By.xpath("(//*[@name='login_button'])[2]"));
    }

    @Override
    protected WebElementFacade getCreateAccountButton() {
        //TODO: fix after adding ID
        return $(By.name("Create a new account"));
    }

    @Override
    protected WebElementFacade getForgotPasswordLink() {
        //TODO: fix after adding ID
        return $(By.name("Forgotten password?"));
    }

    @Override
    public void setEmail(String email) {
        getEmailField().type(email);
        getKeyboardDoneButton().click();
    }

    @Override
    public void setPassword(String password) {
        getPasswordField().type(password);
        getKeyboardDoneButton().click();
    }

    @Override
    public void clickLoginButton() {
        getLoginButton().click();
    }

    @Override
    public boolean isValidationMessageDisplayed() {
        return !getAlertMessageText().isEmpty();
    }

    @Override
    public boolean isLogoDisplayed() {
        //exists only for Web
        return false;
    }
}
