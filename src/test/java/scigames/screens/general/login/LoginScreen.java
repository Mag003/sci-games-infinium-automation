package scigames.screens.general.login;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;
import scigames.SciScreen;

public abstract class LoginScreen extends SciScreen {
    protected abstract WebElementFacade getEmailValidationMessage();

    protected abstract WebElementFacade getPasswordValidationMessage();

    protected abstract WebElementFacade getLoginButton();

    protected abstract WebElementFacade getCreateAccountButton();

    protected abstract WebElementFacade getForgotPasswordLink();

    protected WebElementFacade getPasswordField() {
        return findElementById("password_inputField");
    }

    protected WebElementFacade getEmailField() {
        return findElementById("email_inputField");
    }

    private WebElementFacade getLoginTitle() {
        return findElementById("login_title");
    }

    private WebElementFacade getHelpButton() {
        return findElementById("help_button");
    }

    @Step
    public boolean isLoginTitleDisplayed() {
        return getLoginTitle().isDisplayed();
    }

    @Step
    public boolean isForgotPasswordLinkDisplayed() {
        return getForgotPasswordLink().isDisplayed();
    }

    @Step
    public boolean isCreateAccountButtonDisplayed() {
        return getCreateAccountButton().isDisplayed();
    }

    @Step
    public boolean isHelpButtonDisplayed() {
        return getHelpButton().isDisplayed();
    }

    @Step
    public abstract void setEmail(String email);

    @Step
    public abstract void setPassword(String password);

    @Step
    public abstract void clickLoginButton();

    @Step
    public boolean isEmailValidationMessageDisplayed() {
        return getEmailValidationMessage().isDisplayed();
    }

    @Step
    public boolean isPasswordValidationMessageDisplayed() {
        return getPasswordValidationMessage().isDisplayed();
    }

    @Step
    public void clickHelpButton() {
        getHelpButton().click();
    }

    @Step
    public abstract boolean isValidationMessageDisplayed();

    @Step
    public abstract boolean isLogoDisplayed();
}
