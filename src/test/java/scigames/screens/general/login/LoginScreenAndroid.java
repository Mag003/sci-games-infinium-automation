package scigames.screens.general.login;

import extensions.factory.PageType;
import extensions.factory.PageTypes;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

@PageType(name = PageTypes.ANDROID)
public class LoginScreenAndroid extends LoginScreen {
    @Override
    protected WebElementFacade getEmailValidationMessage() {
        return $(By.xpath("//*[contains(@resource-id, 'textinput_error')][@content-desc='email_fieldValidationMessage']"));
    }

    @Override
    protected WebElementFacade getPasswordValidationMessage() {
        return $(By.xpath("//*[contains(@resource-id, 'textinput_error')][@content-desc='password_fieldValidationMessage']"));
    }

    @Override
    protected WebElementFacade getLoginButton() {
        return $(By.id("login_button"));
    }

    @Override
    protected WebElementFacade getCreateAccountButton() {
        return $(By.id("login_register_button"));
    }

    @Override
    protected WebElementFacade getForgotPasswordLink() {
        return $(By.id("login_forgot_password_label"));
    }

    @Override
    public void setEmail(String email) {
        getEmailField().type(email);
    }

    @Override
    public void setPassword(String password) {
        getPasswordField().type(password);
    }

    @Override
    public void clickLoginButton() {
        getLoginButton().click();
    }

    @Override
    public boolean isValidationMessageDisplayed() {
        return !getAlertMessageText().isEmpty();
    }

    @Override
    public boolean isLogoDisplayed() {
        //exists only for Web
        return false;
    }
}
