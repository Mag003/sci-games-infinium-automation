package scigames.screens.general.login;

import extensions.actions.SwipeDirection;
import extensions.factory.PageType;
import extensions.factory.PageTypes;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

@PageType(name = PageTypes.DESKTOP_WEB)
public class LoginScreenDesktopWeb extends LoginScreen{
    @Override
    protected WebElementFacade getEmailValidationMessage() {
        return findElementById("email_fieldValidationMessage");
    }

    @Override
    protected WebElementFacade getPasswordValidationMessage() {
        return findElementById("password_fieldValidationMessage");
    }

    @Override
    protected WebElementFacade getLoginButton() {
        return $(By.xpath("(//*[@id='login_button'])[2]"));
    }

    @Override
    protected WebElementFacade getCreateAccountButton() {
        return $(By.xpath("//*[contains(@class, 'jss33')]"));
    }

    @Override
    protected WebElementFacade getForgotPasswordLink() {
        return $(By.xpath("//*[contains(@class, 'jss153')]"));
    }

    @Override
    public void setEmail(String email) {
        waitFor(ExpectedConditions.elementToBeClickable(getEmailField()));
        getEmailField().clear();
        getEmailField().sendKeys(email);
    }

    @Override
    public void setPassword(String password) {
        getPasswordField().clear();
        getPasswordField().sendKeys(password);
    }

    @Override
    public void clickLoginButton() {
        swipe(getLoginButton(), SwipeDirection.LEFT);
        getLoginButton().click();
    }

    @Override
    public boolean isValidationMessageDisplayed() {
        return $(By.xpath("//*[contains(@class,'colorError')]")).isDisplayed();
    }

    @Override
    public boolean isLogoDisplayed() {
        return $(By.id("logo")).isDisplayed();
    }
}
