package scigames.screens.general.login;

import extensions.actions.SwipeDirection;
import extensions.factory.PageType;
import extensions.factory.PageTypes;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;

@PageType(name = PageTypes.MOBILE_WEB)
public class LoginScreenMobileWeb extends LoginScreen{
    @Override
    protected WebElementFacade getEmailValidationMessage() {
        return $(By.xpath("//*[@id='email_fieldValidationMessage']//descendant::span"));
    }

    @Override
    protected WebElementFacade getPasswordValidationMessage() {
        return $(By.xpath("//*[@id='password_fieldValidationMessage']//descendant::span"));
    }

    @Override
    protected WebElementFacade getLoginButton() {
        return $(By.xpath("(//*[@id='login_button'])[2]"));
    }

    @Override
    protected WebElementFacade getCreateAccountButton() {
        return $(By.xpath("//*[contains(@class, 'jss33')]"));
    }

    @Override
    protected WebElementFacade getForgotPasswordLink() {
        return $(By.xpath("//*[contains(@class, 'jss153')]"));
    }

    @Override
    public void setEmail(String email) {
        waitFor(ExpectedConditions.elementToBeClickable(getEmailField()));
        getEmailField().sendKeys(Keys.chord(Keys.CONTROL,"a", Keys.DELETE));
        getEmailField().type(email);
        hideMobileKeyboard();
    }

    @Override
    public void setPassword(String password) {
        getPasswordField().sendKeys(Keys.chord(Keys.CONTROL,"a", Keys.DELETE));;
        getPasswordField().type(password);
        hideMobileKeyboard();
    }

    @Override
    public void clickLoginButton() {
        swipe(getLoginButton(), SwipeDirection.LEFT);
        getLoginButton().click();
    }

    @Override
    public boolean isValidationMessageDisplayed() {
        return $(By.xpath("//*[contains(@class,'colorError')]")).isDisplayed();
    }

    @Override
    public boolean isLogoDisplayed() {
        return findElementById("logo").isDisplayed();
    }
}
