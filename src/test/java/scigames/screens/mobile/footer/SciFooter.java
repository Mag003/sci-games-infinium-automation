package scigames.screens.mobile.footer;

import scigames.SciScreen;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;

import java.util.Map;

public abstract class SciFooter extends SciScreen {
    protected abstract Map<FooterTab, WebElementFacade> getFooterTab();

    @Step
    public boolean isFooterTabSelected(FooterTab footerTab) {
        return getFooterTab().get(footerTab).isSelected();
    }

    public enum FooterTab {
        HOME, GAMES, DRAW,
        BONUSES, MORE
    }
}
