package scigames.screens.mobile.footer;

import extensions.factory.PageType;
import extensions.factory.PageTypes;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

import java.util.HashMap;
import java.util.Map;

@PageType(name = PageTypes.ANDROID)
public class SciFooterAndroid extends SciFooter {
    @Override
    protected Map<FooterTab, WebElementFacade> getFooterTab() {
        Map<FooterTab, WebElementFacade> footerTab = new HashMap<>();
        footerTab.put(FooterTab.HOME, $(By.id("home_tab")));
        footerTab.put(FooterTab.GAMES, $(By.id("games_tab")));
        footerTab.put(FooterTab.DRAW, $(By.id("draw_tab")));
        footerTab.put(FooterTab.BONUSES, $(By.id("bonuses_tab")));
        footerTab.put(FooterTab.MORE, $(By.id("more_tab")));
        return footerTab;
    }
}
