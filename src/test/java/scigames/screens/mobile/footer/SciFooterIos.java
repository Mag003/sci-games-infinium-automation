package scigames.screens.mobile.footer;

import extensions.factory.PageType;
import extensions.factory.PageTypes;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

import java.util.HashMap;
import java.util.Map;

@PageType(name = PageTypes.IOS)
public class SciFooterIos extends SciFooter {
    @Override
    protected Map<FooterTab, WebElementFacade> getFooterTab() {
        //TODO: fix with unique IDs
        Map<FooterTab, WebElementFacade> footerTab = new HashMap<>();
        footerTab.put(FooterTab.HOME, $(By.name("Home")));
        footerTab.put(FooterTab.GAMES, $(By.name("Games")));
        footerTab.put(FooterTab.DRAW, $(By.name("Draws")));
        footerTab.put(FooterTab.BONUSES, $(By.name("Bonuses")));
        footerTab.put(FooterTab.MORE, $(By.name("More")));
        return footerTab;
    }
}
