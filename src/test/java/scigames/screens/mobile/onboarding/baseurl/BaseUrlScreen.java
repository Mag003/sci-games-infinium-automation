package scigames.screens.mobile.onboarding.baseurl;

import scigames.SciScreen;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;

public class BaseUrlScreen extends SciScreen {
    private WebElementFacade getUrlField() {
        return $(By.id("urlSwitcherEdittext"));
    }

    private WebElementFacade getResetButton() {
        return $(By.id("urlSwitcherResetButton"));
    }

    private WebElementFacade getLaunchButton() {
        return $(By.id("urlSwitcherLaunchButton"));
    }

    @Step
    public void setBaseUrl(String baseUrl) {
        getUrlField().click();
        getUrlField().clear();
        getUrlField().type(baseUrl);
    }

    @Step
    public void clickResetButton() {
        getResetButton().click();
    }

    @Step
    public void clickLaunchButton() {
        getLaunchButton().click();
    }
}
