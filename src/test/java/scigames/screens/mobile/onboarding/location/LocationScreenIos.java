package scigames.screens.mobile.onboarding.location;

import extensions.factory.PageType;
import extensions.factory.PageTypes;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

@PageType(name = PageTypes.IOS)
public class LocationScreenIos extends LocationScreen {
    @Override
    protected WebElementFacade getLocationEnableButton() {
        return $(By.name("enable_services_button"));
    }

    @Override
    public void acceptLocation() {
        waitFor(ExpectedConditions.alertIsPresent());
        $(By.name("Allow While Using App")).click();
    }
}
