package scigames.screens.mobile.onboarding.location;

import scigames.SciScreen;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;

public abstract class LocationScreen extends SciScreen {
    protected abstract WebElementFacade getLocationEnableButton();

    @Step
    public void clickLocationEnableButton() {
        getLocationEnableButton().click();
    }

    @Step
    public abstract void acceptLocation();
}
