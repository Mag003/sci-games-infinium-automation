package scigames.screens.mobile.onboarding.location;

import extensions.factory.PageType;
import extensions.factory.PageTypes;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

@PageType(name = PageTypes.ANDROID)
public class LocationScreenAndroid extends LocationScreen {
    @Override
    protected WebElementFacade getLocationEnableButton() {
        return $(By.id("location_services_enable_button"));
    }

    @Override
    public void acceptLocation() {
        waitFor(ExpectedConditions.alertIsPresent());
        getAlert().accept();
    }
}
