package scigames.screens.mobile.onboarding.favourite;

import extensions.factory.PageType;
import extensions.factory.PageTypes;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

@PageType(name = PageTypes.ANDROID)
public class OnboardingFavouriteScreenAndroid extends OnboardingFavouriteScreen {
    @Override
    protected WebElementFacade getNextButton() {
        return $(By.id("onboarding_start_button"));
    }
}
