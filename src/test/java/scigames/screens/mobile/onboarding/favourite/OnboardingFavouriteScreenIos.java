package scigames.screens.mobile.onboarding.favourite;

import extensions.factory.PageType;
import extensions.factory.PageTypes;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

@PageType(name = PageTypes.IOS)
public class OnboardingFavouriteScreenIos extends OnboardingFavouriteScreen {
    @Override
    protected WebElementFacade getNextButton() {
        return $(By.name("get_started_button"));
    }
}
