package scigames.screens.mobile.onboarding.favourite;

import scigames.SciScreen;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;

public abstract class OnboardingFavouriteScreen extends SciScreen {
    protected abstract WebElementFacade getNextButton();

    @Step
    public void clickNextButton() {
        getNextButton().click();
    }
}
