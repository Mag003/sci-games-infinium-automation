package scigames.screens.mobile.onboarding.depositandplay;

import extensions.factory.PageType;
import extensions.factory.PageTypes;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

@PageType(name = PageTypes.ANDROID)
public class DepositAndPlayScreenAndroid extends DepositAndPlayScreen {
    @Override
    protected WebElementFacade getStartButton() {
        return $(By.id("onboarding_start_button"));
    }
}
