package scigames.screens.mobile.onboarding.depositandplay;

import scigames.SciScreen;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;

public abstract class DepositAndPlayScreen extends SciScreen {
    protected abstract WebElementFacade getStartButton();

    @Step
    public void clickStartButton() {
        getStartButton().click();
    }
}
