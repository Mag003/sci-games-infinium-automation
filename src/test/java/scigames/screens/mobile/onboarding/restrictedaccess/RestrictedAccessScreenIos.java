package scigames.screens.mobile.onboarding.restrictedaccess;

import extensions.factory.PageType;
import extensions.factory.PageTypes;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

@PageType(name = PageTypes.IOS)
public class RestrictedAccessScreenIos extends RestrictedAccessScreen {
    @Override
    protected WebElementFacade getBrowserButton() {
        return $(By.xpath("//*[@label='Browse Website']"));
    }

    @Override
    protected WebElementFacade getBrowserUrl() {
        return null;
    }
}
