package scigames.screens.mobile.onboarding.restrictedaccess;

import scigames.SciScreen;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;

public abstract class RestrictedAccessScreen extends SciScreen {
    protected abstract WebElementFacade getBrowserButton();

    protected abstract WebElementFacade getBrowserUrl();

    private WebElementFacade getRestrictedAccessTitle() {
        return $(By.id("restricted_services_title_label"));
    }

    private WebElementFacade getRestrictedAccessDesc() {
        return $(By.id("restricted_services_description_label"));
    }

    @Step
    public boolean isRestrictedAccessTitleDisplayed() {
        return getRestrictedAccessTitle().isDisplayed();
    }

    @Step
    public String getRestrictedAccessDescText() {
        return getRestrictedAccessDesc().getText();
    }

    @Step
    public void clickBrowserButton() {
        getBrowserButton().click();
    }

    @Step
    public String getBrowserUrlText() {
        getBrowserUrl().waitUntilPresent();
        return getBrowserUrl().getText();
    }

    @Step
    public boolean isRestrictedAccessTitlePresent() {
        return getRestrictedAccessTitle().isPresent();
    }
}
