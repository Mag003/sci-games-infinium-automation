package scigames.screens.mobile.onboarding.restrictedaccess;

import extensions.factory.PageType;
import extensions.factory.PageTypes;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

@PageType(name = PageTypes.ANDROID)
public class RestrictedAccessScreenAndroid extends RestrictedAccessScreen {
    @Override
    protected WebElementFacade getBrowserButton() {
        return $(By.id("restricted_services_button"));
    }

    @Override
    protected WebElementFacade getBrowserUrl() {
        return $(By.xpath("//*[contains(@resource-id, url_bar)]"));
    }
}
