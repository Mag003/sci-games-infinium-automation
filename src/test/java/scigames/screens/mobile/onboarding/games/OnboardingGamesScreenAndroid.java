package scigames.screens.mobile.onboarding.games;

import extensions.factory.PageType;
import extensions.factory.PageTypes;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

@PageType(name = PageTypes.ANDROID)
public class OnboardingGamesScreenAndroid extends OnboardingGamesScreen {
    @Override
    protected WebElementFacade getNextButton() {
        return $(By.id("onboarding_start_button"));
    }
}
