package scigames.screens.mobile.onboarding.scanticket;

import extensions.factory.PageType;
import extensions.factory.PageTypes;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

@PageType(name = PageTypes.IOS)
public class ScanTicketsScreenIos extends ScanTicketsScreen {
    @Override
    protected WebElementFacade getStartButton() {
        return $(By.name("get_started_button"));
    }
}
