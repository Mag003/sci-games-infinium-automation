package scigames.steps;

import assertions.SoftAssert;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import scigames.SciSteps;
import scigames.screens.general.myaccount.logout.LogoutModal;

public class LogoutSteps extends SciSteps {
    @Steps
    LogoutModal logoutModal;

    @Then("Logout modal should be opened")
    public void isLogoutModalOpened() {
        SoftAssert.assertTrue("Logout modal should be opened", logoutModal.isLogoutModalDisplayed());
    }

    @When("I click Cancel button on Logout modal")
    public void clickCancelButton() {
        logoutModal.clickCancelButton();
    }

    @When("I click Logout button on Logout modal")
    public void clickLogoutButton() {
        logoutModal.clickLogoutButton();
    }
}
