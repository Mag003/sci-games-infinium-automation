package scigames.steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import scigames.SciSteps;

public class OpenSciGamesWeb extends SciSteps {
    @Given("I go to SciGame url")
    public void goToSciGameUrl() {
        openUrl();
    }

    @When("I accept cookie")
    public void acceptWebCookie() {
        acceptCookie();
    }
}
