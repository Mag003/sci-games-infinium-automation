package scigames.steps;

import io.cucumber.java.en.Given;
import scigames.SciSteps;
import scigames.screens.mobile.onboarding.baseurl.BaseUrlScreen;
import scigames.screens.mobile.onboarding.depositandplay.DepositAndPlayScreen;
import scigames.screens.mobile.onboarding.favourite.OnboardingFavouriteScreen;
import scigames.screens.mobile.onboarding.games.OnboardingGamesScreen;
import scigames.screens.mobile.onboarding.location.LocationScreen;
import scigames.screens.mobile.onboarding.scanticket.ScanTicketsScreen;
import net.thucydides.core.annotations.Steps;
import scigamesinfinium.DiProvider;
import scigamesinfinium.environment.SciEnvironment;

public class OpenMobileAndWebApp extends SciSteps {
    @Steps
    BaseUrlScreen baseUrlScreen;

    @Steps
    LocationScreen locationScreen;

    @Steps
    DepositAndPlayScreen depositAndPlayScreen;

    @Steps
    OnboardingFavouriteScreen onboardingFavouriteScreen;

    @Steps
    OnboardingGamesScreen onboardingGamesScreen;

    @Steps
    ScanTicketsScreen scanTicketsScreen;

    @Given("I open SciGames mobile app")
    public void openMobileApp() {
        if (!SciEnvironment.isIosOS()) {
            //here steps for baseUrl screen - only for Android build
            baseUrlScreen.setBaseUrl(DiProvider.environment().getUrl());
            baseUrlScreen.clickLaunchButton();
        }

        //here starts main onboarding steps
        locationScreen.clickLocationEnableButton();
        locationScreen.acceptLocation();
        depositAndPlayScreen.clickStartButton();

        if (SciEnvironment.isIosOS()) {
            onboardingFavouriteScreen.clickNextButton();
            onboardingGamesScreen.clickNextButton();
            scanTicketsScreen.clickStartButton();
        }
    }

    @Given("I open mobile app in not-permitted region")
    public void openInNotPermittedRegion() {
        if (!SciEnvironment.isIosOS()) {
            //here steps for baseUrl screen - only for Android build
            baseUrlScreen.setBaseUrl(DiProvider.environment().getUrl());
            baseUrlScreen.clickLaunchButton();
        }
        locationScreen.clickLocationEnableButton();
        locationScreen.acceptLocation();
    }
}
