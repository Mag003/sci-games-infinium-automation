package scigames.steps;

import assertions.SoftAssert;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import scigames.SciSteps;
import scigames.screens.general.myaccount.MyAccountScreen;

public class MyAccountScreenSteps extends SciSteps {
    @Steps
    MyAccountScreen myAccountScreen;

    @Then("Then Account screen should be opened")
    public void isMyAccountScreenJpened() {
        SoftAssert.assertTrue("Then Account screen should be opened", myAccountScreen.isMyAccountRootDisplayed());
    }

    @When("I click Logout button on Account screen")
    public void clickLogoutButton() {
        myAccountScreen.clickLogoutButton();
    }
}
