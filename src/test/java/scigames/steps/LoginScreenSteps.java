package scigames.steps;

import assertions.SoftAssert;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import scigames.SciSteps;
import scigames.screens.general.login.LoginScreen;
import scigamesinfinium.environment.SciEnvironment;

public class LoginScreenSteps extends SciSteps {
    @Steps
    LoginScreen loginScreen;

    @Then("Login screen should be opened")
    public void isLoginScreenOpened() {
        SoftAssert.assertTrue("Login screen should be opened", loginScreen.isLoginTitleDisplayed());
    }

    @And("Create account and Help buttons, Forgotten password link should be displayed")
    public void isAllButtonsDisplayed() {
        SoftAssert.assertTrue("Create account button should be displayed", loginScreen.isCreateAccountButtonDisplayed());
        SoftAssert.assertTrue("Forgotten password link should be displayed", loginScreen.isForgotPasswordLinkDisplayed());
        SoftAssert.assertTrue("Help button should be displayed", loginScreen.isHelpButtonDisplayed());
    }

    @And("I enter username {string} and password {string} on Login screen")
    public void loginToAccount(String username, String password) {
        loginScreen.setEmail(username);
        loginScreen.setPassword(password);
        loginScreen.clickLoginButton();
    }

    @And("Email and password validation messages should be displayed")
    public void isEmailAndPasswordValidationMessagesDisplayed() {
        SoftAssert.assertTrue("Email validation message should be displayed", loginScreen.isEmailValidationMessageDisplayed());
        SoftAssert.assertTrue("Password validation message should be displayed", loginScreen.isPasswordValidationMessageDisplayed());
    }

    @Then("Login validation message should be displayed")
    public void isLoginValidationMessageDisplayed() {
        //TODO: update when access has been added for android and iOS
        if (SciEnvironment.isDesktopWeb() && SciEnvironment.isMobileWeb()) {
            SoftAssert.assertTrue("Login validation message should be displayed", loginScreen.isValidationMessageDisplayed());
        }
    }

    @Then("Email validation message should be displayed")
    public void isEmailValidationDisplayed() {
        SoftAssert.assertTrue("Email validation message should be displayed", loginScreen.isEmailValidationMessageDisplayed());
    }

    @When("I click Help button on Login screen")
    public void clickHelpButton() {
        loginScreen.clickHelpButton();
    }

    @Then("Login screen should be displayed with logo")
    public void isLogoDisplayed() {
        SoftAssert.assertTrue("Login screen should be displayed with logo", loginScreen.isLogoDisplayed());
    }
}
