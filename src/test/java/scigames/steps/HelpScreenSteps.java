package scigames.steps;

import assertions.SoftAssert;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import scigames.SciSteps;
import scigames.screens.general.help.HelpScreen;
import net.thucydides.core.annotations.Steps;

public class HelpScreenSteps extends SciSteps {
    @Steps
    HelpScreen helpScreen;

    @Then("Help screen should be displayed with suggestion sections")
    public void isHelpWithSuggestionsOpened() {
        //TODO: update after adding functionality
        SoftAssert.assertTrue("Help screen should be displayed with suggestion sections", helpScreen.isHelpDisplayed());
    }

    @And("'Contact Us' and 'Go To Live Chat' buttons  should be displayed")
    public void isAllButtonsDisplayed() {
        SoftAssert.assertTrue("'Contact Us' button should be displayed", helpScreen.isContactUsButtonDisplayed());
        SoftAssert.assertTrue("'Go To Live Chat' button should be displayed", helpScreen.isLiveChatButtonDisplayed());
    }
}
