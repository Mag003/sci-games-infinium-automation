package scigames.steps;

import assertions.SoftAssert;
import io.cucumber.java.en.Then;
import scigames.SciSteps;
import scigames.screens.mobile.footer.SciFooter;
import net.thucydides.core.annotations.Steps;

public class HomeScreenSteps extends SciSteps {
    @Steps
    SciFooter sciFooter;

    @Then("Home screen should be opened on {footerTab} tab")
    public void isHopeScreenOpened(SciFooter.FooterTab footerTab) {
        SoftAssert.assertTrue(String.format("Home screen should be opened on %1s tab", footerTab), sciFooter.isFooterTabSelected(footerTab));
    }
}
