package scigames.steps.registration;

import assertions.SoftAssert;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import scigames.SciSteps;
import scigames.screens.general.registration.createaccount.CreateAccountScreen;
import scigamesinfinium.DiProvider;
import scigamesinfinium.environment.SciEnvironment;

import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

public class CreateAccountScreenSteps extends SciSteps {
    @Steps
    CreateAccountScreen createAccountScreen;

    @Then("Create an account screen should be opened")
    public void isCreateAccountOpened() {
        SoftAssert.assertTrue("Create an account screen should be opened", createAccountScreen.isCreateAccountTitleDisplayed());
    }

    @And("Help button should be displayed and Promo marked as optional")
    public void isCreateAccountElementsDisplayed() {
        SoftAssert.assertTrue("Help button should be displayed", createAccountScreen.isHelpButtonDisplayed());
        SoftAssert.assertThat("Promo marked as optional", createAccountScreen.getPromoCodeLabelText(), containsString(DiProvider.configuration().localization().getLocalizedMessage("registration.step.optional")));
    }

    @And("Show password button should be displayed on Create an account screen")
    public void isShowPasswordDisplayed() {
        SoftAssert.assertTrue("Show password button should be displayed on Create an account screen", createAccountScreen.isShowPasswordButtonDisplayed());
    }

    @And("Receive info should be displayed with 'Send me offers' and 'No thanks' buttons")
    public void isReceiveSectionDisplayedWithButtons() {
        SoftAssert.assertTrue("Receive info should be displayed with 'Send me offers' and 'No thanks' buttons", createAccountScreen.isReceiveSectionDisplayed());
    }

    @When("I enter email {string} on Create an account screen")
    public void enterEmail(String email) {
        createAccountScreen.enterEmail(email);
    }

    @Then("Email error message should be displayed on Create an account screen")
    public void isEmailErrorMessageDisplayed() {
        SoftAssert.assertTrue("Email error message should be displayed on Create an account screen", createAccountScreen.isEmailErrorMessageDisplayed());
    }

    @And("I click Get Started button on Create an account screen")
    public void clickGetStartedButton() {
        createAccountScreen.clickGetStartedButton();
    }

    @Then("Email, password and privacy errors should be displayed")
    public void isFieldsErrorsDisplayed() {
        SoftAssert.assertTrue("Email error message should be displayed", createAccountScreen.isEmailErrorMessageDisplayed());
        SoftAssert.assertTrue("Password error message should be displayed", createAccountScreen.isPasswordErrorMessageDisplayed());
        SoftAssert.assertTrue("Privacy error message should be displayed", createAccountScreen.isPrivacyErrorMessageDisplayed());
    }

    @When("I enter password {string} on Create an account screen")
    public void enterPassword(String password) {
        createAccountScreen.enterPassword(password);
    }

    @Then("Password strength should be displayed on Create an account screen")
    public void isPasswordStrengthDisplayed() {
        //TODO: not supported
        if (!SciEnvironment.isDesktopWeb()) {
            SoftAssert.assertTrue("Password strength should be displayed on Create an account screen", createAccountScreen.isPasswordStrengthDisplayed());
        }
    }

    @When("I enter promo code {string} and click Apply button on Create an account screen")
    public void enterPromoAndApply(String promoCode) {
        createAccountScreen.enterPromoCode(promoCode);
        createAccountScreen.clickApplyPromoCodeButton();
    }

    @Then("Promo code error message should be displayed on Create an account screen")
    public void isPromoCodeErrorMessageDisplayed() {
        //TODO: add after adding functionality - Web
        if (!SciEnvironment.isDesktopWeb()) {
            SoftAssert.assertTrue("Promo code error message should be displayed on Create an account screen", createAccountScreen.isPromoCodeErrorMessageDisplayed());
        }
    }

    @Then("Promo code validation message should be displayed on Create an account screen")
    public void isPromoCodeValidationMessageDisplayed() {
        //TODO: add after adding functionality
    }

    @And("I checked Privacy checkbox on Create an account screen")
    public void checkedPrivacy() {
        createAccountScreen.acceptPrivacy();
    }

    @And("I close Registration")
    public void closeRegistration() {
        createAccountScreen.getDriver().navigate().back();
    }

    @Then("Email, password and promo code should be empty on Create an account screen")
    public void isAllFieldsEmpty() {
        SoftAssert.assertThat("Email should be empty on Create an account screen", createAccountScreen.getEmailFieldText(), equalTo(EMPTY));
        SoftAssert.assertThat("Password should be empty on Create an account screen", createAccountScreen.getPasswordFieldText(), equalTo(EMPTY));
        SoftAssert.assertThat("Promo code should be empty on Create an account screen", createAccountScreen.getPromoCodeFieldText(), equalTo(EMPTY));
    }
}
