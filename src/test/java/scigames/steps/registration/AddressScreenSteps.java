package scigames.steps.registration;

import assertions.SoftAssert;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;
import scigames.SciSteps;
import scigames.screens.general.registration.address.AddressScreen;
import scigamesinfinium.environment.SciEnvironment;

public class AddressScreenSteps extends SciSteps {
    @Steps
    AddressScreen addressScreen;

    @Then("Address screen should be opened")
    public void isAddressScreenOpened() {
        SoftAssert.assertTrue("Address screen should be opened", addressScreen.isAddressTitleDisplayed());
    }

    @When("I enter Address 1 {string} on Address screen")
    public void enterAddress1Line(String address) {
        addressScreen.enterAddress1Line(address);
    }

    @Then("Address error message should be displayed")
    public void isAddressErrorMessageDisplayed() {
        //TODO: add for Android and iOS after adding unique ID
        if (SciEnvironment.isDesktopWeb()) {
            SoftAssert.assertTrue("Address error message should be displayed", addressScreen.isAddress1LIneErrorMessageDisplayed());
        }
    }

    @When("I enter Zip Code {string} on Address screen")
    public void enterZipCode(String zipCode) {
        addressScreen.enterZipCode(zipCode);
    }

    @Then("Zip Code error message should be displayed")
    public void isZipCodeErrorMessageDisplayed() {
        //TODO: add for Android and iOS after adding unique ID
        if (SciEnvironment.isDesktopWeb()) {
            SoftAssert.assertTrue("Zip Code error message should be displayed", addressScreen.isZipCodeErrorMessageDisplayed());
        }
    }

    @When("I enter City {string} on Address screen")
    public void enterCity(String city) {
        addressScreen.enterCity(city);
    }

    @Then("City error message should be displayed")
    public void isCityErrorMessageDisplayed() {
        //TODO: add for Android and iOS after adding unique ID
        if (SciEnvironment.isDesktopWeb()) {
            SoftAssert.assertTrue("City error message should be displayed", addressScreen.isCityErrorMessageDisplayed());
        }
    }

    @When("I click Next button on Address screen")
    public void clickNextButton() {
        addressScreen.clickNextButton();
    }

    @Then("State error message should be displayed")
    public void isStateErrorMessageDisplayed() {
        //TODO: add for Android and iOS after adding unique ID
        if (SciEnvironment.isDesktopWeb()) {
            SoftAssert.assertTrue("State error message should be displayed", addressScreen.isStateErrorMessageDisplayed());
        }
    }

    @When("I select any State on Address screen")
    public void selectState() {
        addressScreen.clickStateField();
        addressScreen.selectState();
    }

    @When("I swipe to Address line on Address screen")
    public void swipeToAddressLine() {
        addressScreen.swipeUpToAddress();
    }

    @When("I remember Address as {string} and {string}")
    public void rememberAddress(String street, String zipCode) {
        Serenity.setSessionVariable(street).to(addressScreen.getCityText());
        Serenity.setSessionVariable(zipCode).to(addressScreen.getZipCodeText());
    }
}
