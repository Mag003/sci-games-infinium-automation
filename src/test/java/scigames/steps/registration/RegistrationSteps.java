package scigames.steps.registration;

import assertions.SoftAssert;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import scigames.SciSteps;
import scigames.screens.general.registration.RegistrationScreen;

public class RegistrationSteps extends SciSteps {
    private static final String GREEN_COLOR = "#42BE6C";
    private static final String BLUE_COLOR = "#004B87";
    private static final String GRAY_COLOR = "#AEAEAE";

    @Steps
    RegistrationScreen registrationScreen;

    @And("{int} registration steps should be displayed on Create an account screen")
    public void isAllRegistrationStepsDisplayed(int stepsCount) {
        //TODO: fix after adding IDs
    }

    @Then("Progress bar should be highlighted with different colors")
    public void isColorDifferent() {
        //TODO: add after adding IDs
        /*SoftAssert.assertThat("Previous step should be green", registrationScreen.getStepColor().get(0), equalTo(GREEN_COLOR));
        SoftAssert.assertThat("Current step should be green", registrationScreen.getStepColor().get(1), equalTo(BLUE_COLOR));
        SoftAssert.assertThat("Next step should be green", registrationScreen.getStepColor().get(2), equalTo(GRAY_COLOR));*/
    }

    @When("I click on not completed registration step")
    public void clickOnNotCompletedStep() {
        //TODO: fix after adding IDs
    }

    @Then("Users should not be navigated to not completed step")
    public void isUserNavigateToNotCompletedStep() {
        //TODO: fix after adding IDs
    }

    @Then("Registration validation message should be displayed")
    public void isRegistrationValidationMessageDisplayed() {
        SoftAssert.assertTrue("Registration validation message should be displayed", registrationScreen.isValidationMessageDisplayed());
    }
}
