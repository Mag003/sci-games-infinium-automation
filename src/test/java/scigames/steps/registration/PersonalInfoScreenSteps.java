package scigames.steps.registration;

import assertions.SoftAssert;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;
import scigames.SciSteps;
import scigames.screens.general.registration.personalinfo.PersonalInfoScreen;
import scigamesinfinium.environment.SciEnvironment;

import java.util.List;

import static org.apache.commons.lang3.StringUtils.EMPTY;

public class PersonalInfoScreenSteps extends SciSteps {
    @Steps
    PersonalInfoScreen personalInfoScreen;

    @Then("Personal Info screen should be opened")
    public void isPersonalInfoScreenOpened() {
        SoftAssert.assertTrue("Personal Info screen should be opened", personalInfoScreen.isPersonalInfoTitleDisplayed());
    }

    @When("^I enter first name on Personal Info screen and error message should be displayed:$")
    public void enterFirstName(DataTable dataTable) {
        List<String> names = dataTable.asList();
        names.forEach(name -> {
            if (name == null) {
                personalInfoScreen.enterFirstName(EMPTY);
            } else {
                personalInfoScreen.enterFirstName(name);
            }
            //TODO: add for Android and iOS after adding unique ID
            if (SciEnvironment.isDesktopWeb()) {
                SoftAssert.assertTrue("First name error message should be displayed", personalInfoScreen.isFirstNameErrorMessageDisplayed());
            }
        });
    }

    @When("^I enter last name on Personal Info screen and error message should be displayed:$")
    public void enterLastName(DataTable dataTable) {
        List<String> names = dataTable.asList();
        names.forEach(name -> {
            if (name == null) {
                personalInfoScreen.enterLastName(EMPTY);
            } else {
                personalInfoScreen.enterLastName(name);
            }
            //TODO: add for Android and iOS after adding unique ID
            if (SciEnvironment.isDesktopWeb()) {
                SoftAssert.assertTrue("Last name error message should be displayed", personalInfoScreen.isLastNameErrorMessageDisplayed());
            }
        });
    }

    @When("^I enter date of birth on Personal Info screen and error message should be displayed:$")
    public void enterDateOfBirth(DataTable dataTable) {
        if (!SciEnvironment.isDesktopWeb()) {
            List<String> dates = dataTable.asList();
            dates.forEach(date -> {
                if (date == null) {
                    personalInfoScreen.enterDateOfBirth(EMPTY, EMPTY, EMPTY);
                } else {
                    personalInfoScreen.enterDateOfBirth(date.replaceAll("\\.(.*)", EMPTY), date.replaceAll("^([^.]+.)", EMPTY).replaceAll("\\.([^.]*)$", EMPTY), date.replaceAll("(.*)\\.", EMPTY));
                }
                SoftAssert.assertTrue("Date of birth error message should be displayed", personalInfoScreen.isDateOfBirthErrorMessageDisplayed());
            });
        }
    }

    @Then("Genders should be displayed on Personal Info screen")
    public void isGendersDisplayed() {
        SoftAssert.assertTrue("Genders should be displayed on Personal Info screen", personalInfoScreen.isGendersDisplayed());
    }

    @When("I click Next button on Personal Info screen")
    public void clickNextButton() {
        personalInfoScreen.clickNextButton();
    }

    @Then("Gender error message should be displayed on Personal Info screen")
    public void isGenderErrorMessageDisplayed() {
        //TODO: add for Android and iOS after adding unique ID
        if (SciEnvironment.isDesktopWeb()) {
            SoftAssert.assertTrue("Gender error message should be displayed on Personal Info screen", personalInfoScreen.isGenderErrorMessageDisplayed());
        }
    }

    @When("^I enter phone number on Personal Info screen and error message should be displayed:$")
    public void enterPhoneNumber(DataTable dataTable) {
        List<String> numbers = dataTable.asList();
        personalInfoScreen.clickMobileRadioButton();
        numbers.forEach(number -> {
            if (number == null) {
                personalInfoScreen.enterMobileNumber(EMPTY);
            } else {
                personalInfoScreen.enterMobileNumber(number);
            }
            //TODO: add for Android and iOS after adding unique ID
            if (SciEnvironment.isDesktopWeb()) {
                SoftAssert.assertTrue("Mobile number error message should be displayed", personalInfoScreen.isMobileNumberErrorDisplayed());
            }
        });
    }

    @When("I enter first name {string} on Personal Info screen")
    public void enterFirstName(String firstName) {
        personalInfoScreen.swipeUpToEmail();
        personalInfoScreen.enterFirstName(firstName);
    }

    @And("I enter last name {string} on Personal Info screen")
    public void enterLastName(String lastName) {
        personalInfoScreen.enterLastName(lastName);
    }

    @And("I enter date of birth {string}, {string}, {string} on Personal Info screen")
    public void enterDateOfBirth(String month, String day, String year) {
        personalInfoScreen.enterDateOfBirth(month, day, year);
    }

    @And("I select gender on Personal Info screen")
    public void selectGender() {
        personalInfoScreen.selectFemale();
    }

    @And("I enter phone number {string} on Personal Info screen")
    public void enterPhoneNumber(String number) {
        personalInfoScreen.clickMobileRadioButton();
        personalInfoScreen.enterMobileNumber(number);
    }

    @When("I remember personal info as {string}")
    public void rememberPersonalInfo(String personalInfo) {
        Serenity.setSessionVariable(personalInfo).to(personalInfoScreen.getFirstNameText() + " " + personalInfoScreen.getLastNameText());
    }
}
