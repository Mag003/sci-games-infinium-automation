package scigames.steps.registration;


import assertions.SoftAssert;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;
import scigames.SciSteps;
import scigames.screens.general.registration.confirm.ConfirmRegistrationScreen;

public class ConfirmRegistrationScreenSteps extends SciSteps {
    @Steps
    ConfirmRegistrationScreen confirmRegistrationScreen;

    @Then("Confirm registration screen should be opened")
    public void isConfirmRegistrationScreenOpened() {
        SoftAssert.assertTrue("Confirm registration screen should be opened", confirmRegistrationScreen.isConfirmRegistrationTitleDisplayed());
    }

    @Then("Personal info {string} and Address {string} and {string} should be displayed")
    public void isPersonalInfoAndAddressCorrect(String personalInfo, String street, String zipCode) {
        String storePersonalInfo = (String) Serenity.getCurrentSession().get(personalInfo);
        String storeStreet = (String) Serenity.getCurrentSession().get(street);
        String storeZipCode = (String) Serenity.getCurrentSession().get(zipCode);
        SoftAssert.assertTrue(String.format("Personal info %1s should be displayed %2s", storePersonalInfo, confirmRegistrationScreen.getPersonalInfoText()), confirmRegistrationScreen.getPersonalInfoText().contains(storePersonalInfo));
        SoftAssert.assertTrue(String.format("Address %1s should be displayed %2s", storeStreet, confirmRegistrationScreen.getAddressInfoText()), confirmRegistrationScreen.getAddressInfoText().contains(storeStreet));
        SoftAssert.assertTrue(String.format("Address %1s should be displayed %2s", storeZipCode, confirmRegistrationScreen.getAddressInfoText()), confirmRegistrationScreen.getAddressInfoText().contains(storeZipCode));
    }

    @When("I click Complete button on Confirm registration screen")
    public void clickCompletedButton() {
        confirmRegistrationScreen.clickCompletedButton();
    }

    @Then("SSN error message should be displayed on Confirm registration screen")
    public void isSsnErrorMessageDisplayed() {

    }
}
