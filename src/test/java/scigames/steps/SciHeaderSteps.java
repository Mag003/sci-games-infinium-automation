package scigames.steps;

import io.cucumber.java.en.When;
import scigames.SciSteps;
import scigames.screens.general.header.SciHeader;
import net.thucydides.core.annotations.Steps;

public class SciHeaderSteps extends SciSteps {
    @Steps
    SciHeader sciHeader;

    @When("I click Login button on app header")
    public void clickLoginButton() {
        sciHeader.clickLoginButton();
    }

    @When("I click Join button on app header")
    public void clickJoinButton() {
        sciHeader.clickJoinButton();
    }

    @When("I click My Account button on app header")
    public void clickMyAccountButton() {
        sciHeader.clickMyProfileButton();
    }
}
