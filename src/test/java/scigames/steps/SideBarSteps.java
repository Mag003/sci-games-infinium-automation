package scigames.steps;

import assertions.SoftAssert;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import scigames.screens.desktop.sidebar.SideBar;
import scigamesinfinium.environment.SciEnvironment;

public class SideBarSteps {
    @Steps
    SideBar sideBar;

    @Then("Screen should be opened on {sideTab} tab")
    public void isScreenOpenedOnCorrectTab(SideBar.SideTabs tab) {
        if (SciEnvironment.isDesktopWeb()) {
            SoftAssert.assertTrue("Screen should be opened on " + tab + " tab", sideBar.isSideTabSelected(tab));
        }
    }
}
