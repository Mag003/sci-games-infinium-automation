package scigames.steps;

import assertions.SoftAssert;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import scigames.SciSteps;
import scigames.screens.general.welcome.WelcomeModal;

public class WelcomeModalSteps extends SciSteps {
    @Steps
    WelcomeModal welcomeModal;

    @Then("Welcome modal should be opened with welcome message")
    public void isWelcomeModalDisplayed() {
        SoftAssert.assertTrue("Welcome modal should be opened with welcome message", welcomeModal.isWelcomeTitleDisplayed());
    }

    @And("Cash balance, total balance and bonus balance should be displayed")
    public void isCashTotalAndBonusValuesDisplayed() {
        SoftAssert.assertTrue("Cash balance should be displayed", welcomeModal.isCashBalanceDisplayed());
        SoftAssert.assertTrue("Total balance should be displayed", welcomeModal.isTotalBalanceDisplayed());
        SoftAssert.assertTrue("Bonus balance should be displayed", welcomeModal.isBonusBalanceDisplayed());
    }

    @And("Game icon with banners should be displayed")
    public void isGameIconAndPromotionsDisplayed() {
        //TODO: add Game Icon after adding element to UI
    }

    @And("Continue button should be displayed")
    public void isContinueButtonDisplayed() {
        SoftAssert.assertTrue("Continue button should be displayed", welcomeModal.isContinueButtonDisplayed());
    }

    @When("I click Deposit button on Welcome modal")
    public void clickDepositButton() {
        welcomeModal.clickDepositButton();
    }

    @When("I click Promo code button on Welcome modal")
    public void clickPromotionsButton() {
        //TODO: add Game Icon after adding element to UI
    }

    @When("I click on Claim button on Welcome modal")
    public void clickClaimButton() {
        welcomeModal.clickClaimButton();
    }

    @When("I click Continue button on Welcome modal")
    public void clickContinueButton() {
        welcomeModal.clickContinueButton();
    }
}
