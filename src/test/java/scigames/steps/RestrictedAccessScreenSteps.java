package scigames.steps;

import assertions.SoftAssert;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import scigames.SciScreen;
import scigames.screens.mobile.onboarding.restrictedaccess.RestrictedAccessScreen;
import net.thucydides.core.annotations.Steps;
import scigamesinfinium.DiProvider;
import scigamesinfinium.environment.SciEnvironment;
import utilities.StringFunctions;

import static org.apache.commons.lang3.StringUtils.SPACE;
import static org.hamcrest.Matchers.equalTo;

public class RestrictedAccessScreenSteps extends SciScreen {
    @Steps
    RestrictedAccessScreen restrictedAccessScreen;

    @Then("Restricted Access to Services screen should be displayed")
    public void isRestrictedScreenOpened() {
        SoftAssert.assertTrue("Restricted Access to Services screen should be displayed", restrictedAccessScreen.isRestrictedAccessTitleDisplayed());
        SoftAssert.assertThat("Restricted Access description should be correct", StringFunctions.replaceNewLinesAndTrim(restrictedAccessScreen.getRestrictedAccessDescText().replaceAll("\\s+", SPACE)),
                equalTo(DiProvider.configuration().localization().getLocalizedMessage("restricted.access.services.desc")));
    }

    @When("I click 'Browser The Website' button on Restricted Access to Services screen")
    public void clickBrowserButton() {
        restrictedAccessScreen.clickBrowserButton();
    }

    @Then("Browser should be opened with correct link")
    public void isBrowserLinkCorrect() {
        //TODO: add for iOS after adding functionality
        if (!SciEnvironment.isIosOS()) {
            SoftAssert.assertThat("Browser should be opened with correct link", restrictedAccessScreen.getBrowserUrlText(),
                    equalTo(DiProvider.environment().getApiUrl()));
        }
    }

    @Then("Restricted Access to Services screen shouldn't be displayed")
    public void isRestrictedScreenNotOpened() {
        SoftAssert.assertTrue("Restricted Access to Services screen shouldn't be displayed", !restrictedAccessScreen.isRestrictedAccessTitlePresent());
    }
}
