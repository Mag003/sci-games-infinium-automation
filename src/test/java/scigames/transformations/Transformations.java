package scigames.transformations;

import io.cucumber.java.ParameterType;
import scigames.screens.mobile.footer.SciFooter;
import scigames.screens.desktop.sidebar.SideBar;

public class Transformations {
    @ParameterType("HOME|GAMES|DRAW|BONUSES|MORE")
    public SciFooter.FooterTab footerTab(String tab) {
        return SciFooter.FooterTab.valueOf(tab);
    }

    @ParameterType("HOME|GAMES|DRAW|PROMOTIONS")
    public SideBar.SideTabs sideTab(String tab) {
        return SideBar.SideTabs.valueOf(tab);
    }
}
