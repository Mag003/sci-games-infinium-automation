package utilities;

import org.joda.time.*;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import scigamesinfinium.DiProvider;

import java.util.*;
import java.util.function.BiFunction;

public class DateTimeFunctions {
    private DateTimeFunctions() {
    }

    public static final DateTimeFormatter FORMAT_YYYY_MM_DD = ISODateTimeFormat.yearMonthDay();
    public static final DateTimeFormatter FORMAT_YYYY_MM_DD_HH_MM = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm");
    public static final DateTimeZone TIME_ZONE_UTC = DateTimeZone.UTC;
    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss");
    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ssZ");
    public static final DateTimeFormatter HOURS_AND_MINUTES_FORMATTER = DateTimeFormat.forPattern("HH:mm");
    public static final DateTimeFormatter DAY_MONTH_YEAR = DateTimeFormat.forPattern("dd.MM.yyyy");

    public static int minutesBetween(DateTime actual, DateTime expected) {
        return Minutes.minutesBetween(actual, expected).getMinutes();
    }

    /**
     * @param actual           actual time
     * @param expected         interval where actual time will be checked will be [expeced - precisionMinutes; expected + precisionMinutes]
     * @param precisionMinutes precision
     * @return true if actual and expected are the same in precision
     */
    public static boolean areTimesEqual(DateTime actual, DateTime expected, int precisionMinutes) {
        Interval interval = new Interval(expected.minus(Duration.standardMinutes(precisionMinutes)),
                expected.plus(Duration.standardMinutes(precisionMinutes)));
        return interval.contains(actual);
    }

    public static boolean areTimesEqual(DateTime actual, DateTime expected) {
        return areTimesEqual(actual, expected, 2);
    }

    /**
     * return month name like 'January' based on date
     *
     * @param dateTime date time
     * @return localized month's name
     */
    public static String getMonthName(DateTime dateTime) {
        Locale locale = DiProvider.configuration().localization().getLocale();
        return dateTime.monthOfYear().getAsText(locale);
    }

    public static String getToday() {
        return dateToString(DateTime.now(), FORMAT_YYYY_MM_DD);
    }

    public static List<String> getPastWeekDates() {
        return getPastDates(DateTimeConstants.DAYS_PER_WEEK, FORMAT_YYYY_MM_DD);
    }

    public static String getDateForLastMonths(int months) {
        return dateToString(DateTime.now().minusMonths(new Random().nextInt(months)), FORMAT_YYYY_MM_DD);
    }

    public static List<String> getFutureWeekDates() {
        return getFutureDates(DateTimeConstants.DAYS_PER_WEEK, FORMAT_YYYY_MM_DD);
    }

    public static String getYesterday() {
        return getPastDates(1, FORMAT_YYYY_MM_DD).get(0);
    }

    public static String getTomorrow() {
        return getFutureDates(1, FORMAT_YYYY_MM_DD).get(0);
    }

    public static List<String> getFutureDates(int number, DateTimeFormatter formatter) {
        return getDates(number, formatter, DateTime::plusDays);
    }

    public static List<String> getPastDates(int number, DateTimeFormatter formatter) {
        return getDates(number, formatter, DateTime::minusDays);
    }

    private static List<String> getDates(int number, DateTimeFormatter formatter, BiFunction<DateTime, Integer, DateTime> plusOrMinus) {
        DateTime current = DateTime.now();
        List<String> dates = new ArrayList<>();
        for (int day = 1; day <= number; day++) {
            dates.add(formatter.print(plusOrMinus.apply(current, day)));
        }
        return dates;
    }

    private static String dateToString(DateTime dateTime, DateTimeFormatter formatter) {
        return formatter.print(dateTime);
    }

    public static String getTime(String dateTime) {
        return tryParseUtcOrAsIs(dateTime, DATE_TIME_FORMATTER, HOURS_AND_MINUTES_FORMATTER, false);
    }

    public static String getDate(String dateTime) {
        return tryParseUtcOrAsIs(dateTime, DATE_TIME_FORMATTER, FORMAT_YYYY_MM_DD, true);
    }

    public static String getDate(String dateTime, DateTimeFormatter outFormatter) {
        return tryParseUtcOrAsIs(dateTime, DATE_FORMATTER, outFormatter, true);
    }

    public static String getDayMonthYear(String dateTime) {
        return DAY_MONTH_YEAR.print(DATE_TIME_FORMATTER.parseDateTime(dateTime));
    }

    private static String tryParseUtcOrAsIs(String dateTime, DateTimeFormatter inFormatter, DateTimeFormatter outFormatter, boolean retainTimeZone) {
        if (tryParse(dateTime, inFormatter).isPresent()) {
            String dateTimeUtc = getDateTime(dateTime, inFormatter, inFormatter, TIME_ZONE_UTC, retainTimeZone);
            return getDateTime(
                    dateTimeUtc,
                    inFormatter,
                    outFormatter,
                    getDeviceTimeZone(),
                    retainTimeZone
            );
        } else {
            return dateTime;
        }
    }

    public static DateTime getDateTime(String dateTime, DateTimeFormatter inFormatter, boolean retainZone) {
        DateTime parsed = inFormatter.withOffsetParsed().parseDateTime(dateTime);
        return retainZone ? parsed.withZoneRetainFields(getDeviceTimeZone()) : parsed.withZone(getDeviceTimeZone());
    }

    public static DateTime getCurrentDateTime() {
        return FORMAT_YYYY_MM_DD_HH_MM.parseDateTime(dateToString(DateTime.now(), FORMAT_YYYY_MM_DD_HH_MM)).withZone(getDeviceTimeZone());
    }

    public static DateTime getCurrentTime() {
        DateTime now = DateTime.now();
        return HOURS_AND_MINUTES_FORMATTER.parseDateTime(String.format("%1$s:%2$s", now.hourOfDay().get(), now.minuteOfHour().get()));
    }

    public static String getDateTime(String dateTime, DateTimeFormatter inFormatter, DateTimeFormatter outFormatter) {
        DateTime parsed = tryParse(dateTime, outFormatter).orElseGet(() -> inFormatter.withOffsetParsed().parseDateTime(dateTime));
        return outFormatter.print(parsed);
    }

    /**
     * checks if datetime in expected format and formats it if false
     *
     * @param dateTime     datetime to format
     * @param inFormatter  input formatter
     * @param outFormatter output formatter
     * @param timeZone     timezone
     * @return date time in necessary format
     */
    private static String getDateTime(String dateTime, DateTimeFormatter inFormatter, DateTimeFormatter outFormatter, DateTimeZone timeZone, boolean retainTimeZone) {
        DateTime parsed = tryParse(dateTime, outFormatter).orElseGet(() -> inFormatter.withOffsetParsed().parseDateTime(dateTime));
        return outFormatter.print(retainTimeZone ? parsed.withZoneRetainFields(timeZone) : parsed.withZone(timeZone));
    }

    private static Optional<DateTime> tryParse(String dateTime, DateTimeFormatter formatter) {
        try {
            return Optional.of(formatter.parseDateTime(dateTime));
        } catch (IllegalArgumentException e) {
            return Optional.empty();
        }
    }

    private static DateTimeZone getDeviceTimeZone() {
        return DiProvider.configuration().getTimeZone();
    }

    public static DateTime getDayBefore(DateTime dateTime) {
        return getDaysBefore(dateTime, 1);
    }

    public static DateTime getDaysBefore(DateTime dateTime, int days) {
        return dateTime.minusDays(days);
    }
}
