package utilities.gmail;

import javax.mail.Address;
import javax.mail.internet.InternetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

class MessageAddressReader {
    static final String DELIVERED_TO_HEADER = "Delivered-To";

    static List<String> getAddresses(Address[] addresses) {
        List<String> emails = new ArrayList<>();
        if (addresses != null) {
            for (Address address : addresses) {
                emails.add(((InternetAddress)address).getAddress());
            }
        }
        return emails;
    }

    static String getFirstAddress(Address[] addresses){
        return getAddresses(addresses).stream().findFirst()
                .orElseThrow(() -> new NoSuchElementException("Cannot find message address"));
    }
}
