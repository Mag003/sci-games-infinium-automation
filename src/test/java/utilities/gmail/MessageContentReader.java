package utilities.gmail;

import org.jsoup.Jsoup;

import javax.mail.Flags;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;

class MessageContentReader {
    static String getHtmlMessageContent(Message message) {
        return getMessageContent(message, "text/html", mess -> Jsoup.parse((String) message.getContent()).text(), true);
    }

    static String getPlainTextMessageContent(Message message) {
        return getMessageContent(message, "text/plain", mess -> message.getContent().toString(), true);
    }

    static String getMultipartMessageContent(Message message, int part) {
        return getMessageContent(message, "multipart/*", mess -> {
            MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
            return mimeMultipart.getBodyPart(part).getContent().toString();
        }, true);
    }

    private static String getMessageContent(Message message, String mimeType, MessageContentFetcher getContent, boolean markAsRead) {
        try {
            if (!message.isMimeType(mimeType)) {
                throw new IllegalStateException("To use this function message type should be " + mimeType);
            } else {
                String content = getContent.getContent(message);
                message.setFlag(Flags.Flag.SEEN, false);
                return content;
            }
        } catch (IOException | MessagingException e) {
            throw new IllegalStateException("Cannot get content from message with mime type " + mimeType);
        }
    }

    @FunctionalInterface
    private interface MessageContentFetcher {
        String getContent(Message message) throws IOException, MessagingException;
    }
}
