package utilities.gmail;

import org.slf4j.LoggerFactory;
import utilities.DateTimeFunctions;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.search.SearchTerm;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.Optional;

import static org.apache.commons.lang3.StringUtils.containsIgnoreCase;
import static utilities.gmail.MessageAddressReader.DELIVERED_TO_HEADER;
import static utilities.gmail.MessageContentReader.getPlainTextMessageContent;

public enum SearchCriterion {
    SEEN((message, searchKey, userName) -> true,
            SearchTermType.SEEN),

    UNSEEN((message, searchKey, userName) -> true,
            SearchTermType.UNSEEN),

    SUBJECT((message, searchKey, userName) -> message.getSubject().equalsIgnoreCase(searchKey) && Arrays.stream(message.getHeader(DELIVERED_TO_HEADER))
            .anyMatch(user -> user.equalsIgnoreCase(userName)),
            SearchTermType.SUBJECT),

    SUBJECT_PART(((message, searchKey, userName) -> containsIgnoreCase(message.getSubject(), searchKey) && Arrays.stream(message.getHeader(DELIVERED_TO_HEADER))
            .anyMatch(user -> user.equalsIgnoreCase(userName))),
            SearchTermType.SUBJECT),

    SENDER(((message, searchKey, userName) -> ((InternetAddress) message.getFrom()[0]).getAddress().equals(searchKey) && Arrays.stream(message.getHeader(DELIVERED_TO_HEADER))
            .anyMatch(user -> user.equalsIgnoreCase(userName))),
            SearchTermType.FROM),

    BODY_MIME_TEXT(((message, searchKey, userName) -> containsIgnoreCase(getPlainTextMessageContent(message), searchKey)),
            SearchTermType.BODY),

    BODY_MIME_MULTIPART_1(((message, searchKey, userName) -> containsIgnoreCase(MessageContentType.MULTIPART_BODY_1.getContent(message), searchKey)),
            SearchTermType.NONE),

    RECIPIENT_TO(((message, searchKey, userName) -> MessageAddressReader.getAddresses(message.getRecipients(Message.RecipientType.TO)).contains(searchKey)),
            SearchTermType.TO),

    RECIPIENT_CC(((message, email, userName) -> MessageAddressReader.getAddresses(message.getRecipients(Message.RecipientType.CC)).contains(email)),
            SearchTermType.CC),

    DATE_AFTER(((message, searchKey, userName) -> {
        Date date = DateTimeFunctions.getDateTime(searchKey, DateTimeFunctions.FORMAT_YYYY_MM_DD, false).toDate();
        return message.getSentDate().after(date);
    }), SearchTermType.DATE_AFTER);


    private final SearchTermMatcher matcher;
    private final SearchTermType searchTermType;


    SearchCriterion(SearchTermMatcher matcher, SearchTermType searchTermType) {
        this.matcher = matcher;
        this.searchTermType = searchTermType;
    }

    public SearchTerm getTerm(String searchKey, String userName) {
        return new SearchTerm() {
            public boolean match(Message message) {
                try {
                    return matcher.match(message, searchKey, userName);
                } catch (MessagingException | IOException e) {
                    LoggerFactory.getLogger(SearchCriterion.class).error("Unable to get data of an email message", e);
                    return false;
                }
            }
        };
    }

    public Optional<SearchTerm> getTerm(String searchKey) {
        return searchTermType.getTerm(searchKey);
    }
}