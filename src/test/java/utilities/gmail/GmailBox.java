package utilities.gmail;

import extensions.configuration.email.IEmailConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utilities.ConditionalWait;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.AndTerm;
import javax.mail.search.SearchTerm;
import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;

public class GmailBox {
    private final String email;
    private final String password;
    private final String smtpServer;
    private final int smtpPort;
    private final List<String> defaultRecipients;
    private final String imapServer;
    private final String imapProtocol;
    private final String defaultFolder;
    private final Duration waitTimeout;

    private static final Logger LOGGER = LoggerFactory.getLogger(GmailBox.class);

    public GmailBox(IEmailConfiguration configuration) {
        this.email = configuration.getEmail();
        this.password = configuration.getPassword();
        this.smtpServer = configuration.getSmtpServer();
        this.smtpPort = configuration.getSmtpPort();
        this.defaultRecipients = configuration.getDefaultRecipients();
        this.imapServer = configuration.getImapServer();
        this.imapProtocol = configuration.getImapProtocol();
        this.defaultFolder = configuration.getDefaultFolder();
        this.waitTimeout = configuration.getWaitTimeout();
    }

    /**
     * sends email to list of recipients defined in the src/test/resources/config.json
     *
     * @param subject subject of the email
     * @param text    content of the email
     */
    public void sendToDefault(String subject, String text) {
        send(subject, text, defaultRecipients, null);
    }

    public void sendToDefault(String subject, String text, File attachment) {
        send(subject, text, defaultRecipients, attachment);
    }

    /**
     * sends email to list of recipients
     *
     * @param subject    subject of the email
     * @param text       content of the email
     * @param recipients list of recipients
     */
    public void send(String subject, String text, List<String> recipients, File attachment) {
        String recipientsAsString = String.join(",", recipients);
        Session session = getSmtpSession();
        String loggingMessage = String.format("Email'%1$s' has been sent from '%2$s' to '%3$s'", subject, email, recipientsAsString);
        try {
            Message message = createMessage(session, email, recipientsAsString, subject, text, attachment);
            Transport.send(message);

        } catch (MessagingException e) {
            throw new IllegalStateException("Error during operation: " + loggingMessage + "\n" + e.getMessage());
        }
    }

    public List<MailMessage> getMessages(Map<SearchCriterion, String> criteria, MessageContentType messageContentType) {
        return getMessages(defaultFolder, criteria, messageContentType);
    }

    private List<MailMessage> getMessages(String folderName, Map<SearchCriterion, String> criteria, MessageContentType messageContentType) {
        Store store = getStore();
        try {
            store.connect(imapServer, email, password);
            Folder folder = store.getFolder(folderName);
            if(!folder.isOpen()){
                folder.open(Folder.READ_WRITE);
            }
            Message[] messages = waitAndGetMessages(folder, criteria);
            if(messages.length == 0){
                throw new IllegalStateException("Letter was not found by criteria: " +
                        criteria.entrySet().stream().map(entry -> entry.getKey() + "=" + entry.getValue())
                                .collect(Collectors.joining(",")));
            }
            List<MailMessage> mailMessages = new ArrayList<>();
            for(Message message : messages){
                String subject = message.getSubject();
                String content = messageContentType.getContent(message);
                List<String> recipients = MessageAddressReader.getAddresses(message.getRecipients(Message.RecipientType.TO));
                String sender = MessageAddressReader.getFirstAddress(message.getFrom());
                MailMessage mailMessage = new MailMessage(subject, content, recipients, sender);
                mailMessages.add(mailMessage);
            }
            folder.setFlags(messages, new Flags(Flags.Flag.SEEN), true);
            logSearch("Finally", messages.length, criteria);
            return mailMessages;
        } catch (MessagingException e) {
            throw new IllegalStateException(
                    String.format("Cannot connect to mailbox with data: %1$s/$2$s (server: %3$s, protocol: %4$s). %5$s",
                            email, password, imapServer, imapProtocol, e.getMessage())
            );
        } finally {
            closeConnection(store);
        }
    }

    private Store getStore() {
        try {
            return getImapSession().getStore(imapProtocol);
        } catch (NoSuchProviderException e) {
            throw new IllegalStateException(e.getMessage());
        }
    }

    private void closeConnection(Store store) {
        if (store.isConnected()) {
            try {
                store.close();
            } catch (MessagingException e) {
                throw new IllegalStateException("Cannot close connection to  mailbox. " + e.getMessage());
            }
        }
    }

    private Message[] waitAndGetMessages(Folder folder, Map<SearchCriterion, String> criteria) {
        ConditionalWait waiter = new ConditionalWait();
        final List<Message>[] searchResult = new List[]{new ArrayList<Message>()};
        waiter.waitFor(() -> {
                    Optional<List<Message>> optionalMessages = getMessages(folder, criteria);
                    if(optionalMessages.isPresent()){
                        searchResult[0] = optionalMessages.get();
                        return true;
                    }
                    return false;
                }, waitTimeout, Duration.ofMillis(5000),
                "Finding email by search terms " + criteria.keySet().stream().map(Enum::name).collect(Collectors.joining(", ")));
        return searchResult[0].toArray(new Message[0]);
    }

    private Optional<List<Message>> getMessages(Folder folder, Map<SearchCriterion, String> criteria) {
        List<Message> foundMessages = new ArrayList<>();
        Message[] recentMessages = getMessagesByType(folder, criteria);
        logSearch("Before additional checks", recentMessages.length, criteria);
        List<SearchTerm> searchTerms = criteria.entrySet().stream()
                .map(entry -> entry.getKey().getTerm(entry.getValue(), email))
                .collect(Collectors.toList());
        for (Message message : recentMessages) {
            if (searchTerms.stream().allMatch(searchTerm -> searchTerm.match(message))) {
                foundMessages.add(message);
            }
        }
        if (!foundMessages.isEmpty()) {
            return Optional.of(foundMessages);
        }
        return Optional.empty();
    }

    private void logSearch(String stageName, int length, Map<SearchCriterion, String> criteria){
        LOGGER.info(String.format("%1$s %2$s found messages by criteria:%3$s",
                stageName,
                length,
                criteria.entrySet().stream().map(entry -> String.format("%ntype='%1$s', key='%2$s'", entry.getKey(), entry.getValue())).collect(Collectors.joining(","))));
    }

    private Message[] getMessagesByType(Folder folder, Map<SearchCriterion, String> criteria) {
        SearchTerm searchTerm = null;
        for (Map.Entry<SearchCriterion, String> searchEntry : criteria.entrySet()) {
            Optional<SearchTerm> optionalTerm = searchEntry.getKey().getTerm(searchEntry.getValue());
            if(optionalTerm.isPresent()){
                SearchTerm term = optionalTerm.get();
                if (searchTerm == null) {
                    searchTerm = term;
                } else {
                    searchTerm = new AndTerm(searchTerm, term);
                }
            }
        }
        try {
            return folder.search(searchTerm);
        } catch (MessagingException e) {
            return new Message[]{};
        }
    }

    private Message createMessage(Session session, String from, String to, String subject, String text, File attachment) throws MessagingException {
        Message message = new MimeMessage(session);
        message.setSubject(subject);
        message.setFrom(new InternetAddress(from));
        message.setRecipients(
                Message.RecipientType.TO,
                InternetAddress.parse(to)
        );

        Multipart multipart = new MimeMultipart();
        MimeBodyPart textPart = new MimeBodyPart();
        textPart.setText(text);

        MimeBodyPart attachmentPart = new MimeBodyPart();
        if (attachment != null) {
            try {
                attachmentPart.attachFile(attachment);
            } catch (IOException e) {
                LOGGER.error("Can't attach file " + attachment.getAbsolutePath() + "\n" + e.getMessage());
            }
        }

        multipart.addBodyPart(textPart);
        multipart.addBodyPart(attachmentPart);
        message.setContent(multipart);
        return message;
    }

    private Session getSmtpSession() {
        Properties props = new Properties();
        props.put("mail.smtp.host", smtpServer);
        props.put("mail.smtp.port", smtpPort);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        return getSession(props);
    }

    private Session getImapSession() {
        Properties props = new Properties();
        props.put(imapServer, imapProtocol);
        return getSession(props);
    }

    private Session getSession(Properties props) {
        return Session.getDefaultInstance(props, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(email, password);
            }
        });
    }
}
