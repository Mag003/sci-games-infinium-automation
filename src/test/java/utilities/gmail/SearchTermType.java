package utilities.gmail;

import utilities.DateTimeFunctions;

import javax.mail.Flags;
import javax.mail.Message;
import javax.mail.search.*;
import java.util.Optional;
import java.util.function.Function;

enum SearchTermType {
    SEEN(seen -> Optional.of(new FlagTerm(new Flags(Flags.Flag.SEEN), true))),
    UNSEEN(seen -> Optional.of(new FlagTerm(new Flags(Flags.Flag.SEEN), false))),
    SUBJECT(subject -> Optional.of(new SubjectTerm(subject))),
    FROM(from -> Optional.of(new FromStringTerm(from))),
    BODY(body -> Optional.of(new BodyTerm(body))),
    NONE(none -> Optional.empty()),
    TO(email -> Optional.of(new RecipientStringTerm(Message.RecipientType.TO, email))),
    CC(email -> Optional.of(new RecipientStringTerm(Message.RecipientType.CC, email))),
    DATE_AFTER(date -> Optional.of(new ReceivedDateTerm(ComparisonTerm.GT,
            DateTimeFunctions.getDateTime(date, DateTimeFunctions.FORMAT_YYYY_MM_DD, false).toDate())));

    private final Function<String, Optional<SearchTerm>> provider;

    SearchTermType(Function<String, Optional<SearchTerm>> provider) {
        this.provider = provider;
    }

    public Optional<SearchTerm> getTerm(String searchKey) {
        return provider.apply(searchKey);
    }
}
