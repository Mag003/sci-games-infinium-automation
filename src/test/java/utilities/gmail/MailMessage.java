package utilities.gmail;

import java.util.List;

public class MailMessage {
    private final String subject;
    private final String content;
    private final List<String> recipients;
    private final String sender;

    public MailMessage(String subject, String content, List<String> recipients, String sender) {
        this.subject = subject;
        this.content = content;
        this.recipients = recipients;
        this.sender = sender;
    }

    public String getSubject() {
        return subject;
    }

    public String getContent() {
        return content;
    }

    public List<String> getRecipients() {
        return recipients;
    }

    public String getSender() {
        return sender;
    }
}
