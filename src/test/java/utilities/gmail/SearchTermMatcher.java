package utilities.gmail;

import javax.mail.Message;
import javax.mail.MessagingException;
import java.io.IOException;

@FunctionalInterface
public interface SearchTermMatcher {
    boolean match(Message message, String searchKey, String userName) throws MessagingException, IOException;
}
