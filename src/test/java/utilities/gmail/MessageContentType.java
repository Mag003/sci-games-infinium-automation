package utilities.gmail;

import javax.mail.Message;
import java.util.function.Function;

public enum MessageContentType {
    PLAIN_TEXT(MessageContentReader::getPlainTextMessageContent),
    HTML(MessageContentReader::getHtmlMessageContent),
    MULTIPART_BODY_1(message -> MessageContentReader.getMultipartMessageContent(message, 1));

    private final Function<Message, String> contentParser;

    MessageContentType(Function<Message, String> contentParser) {
        this.contentParser = contentParser;
    }

    public String getContent(Message message) {
        return contentParser.apply(message);
    }
}
