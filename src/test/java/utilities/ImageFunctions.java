package utilities;

import extensions.retrier.ActionRetrier;
import org.openqa.selenium.StaleElementReferenceException;
import scigamesinfinium.DiProvider;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;

public class ImageFunctions {
    private ImageFunctions() {
    }

    /**
     * Concatenates images one by one by vertical
     *
     * @param images     list of images to concat
     * @param pathToSave path to save concat(result) image
     */
    public static void concat(List<BufferedImage> images, Path pathToSave) {
        if(images.size() > 0){
            ActionRetrier actionRetrier = new ActionRetrier(DiProvider.configuration().retry());
            actionRetrier.doWithRetry(() -> {
                int heightTotal = images.stream().map(BufferedImage::getHeight).reduce(0, Integer::sum);
                int widthTotal = images.stream().map(BufferedImage::getWidth).max(Integer::compareTo)
                        .orElseThrow(() -> new IllegalArgumentException("Cannot get widths of the images"));

                BufferedImage concatImage = new BufferedImage(widthTotal, heightTotal, BufferedImage.TYPE_INT_RGB);
                Graphics2D g2d = concatImage.createGraphics();
                int heightCurr = 0;
                for (BufferedImage image : images) {
                    g2d.drawImage(image, 0, heightCurr, null);
                    heightCurr += image.getHeight();
                }
                g2d.dispose();

                try {
                    ImageIO.write(concatImage, "png", pathToSave.toFile());
                } catch (IOException e) {
                    throw new IllegalArgumentException("Can't save image by path: " + pathToSave.toAbsolutePath() + "\nError: " + e.getMessage());
                }
            }, Collections.singletonList(StaleElementReferenceException.class));
        }
    }
}
