package utilities;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.apache.commons.lang3.StringUtils.*;

public class StringFunctions {
    private StringFunctions() {
    }

    public static Optional<String> getMatch(String text, String regex) {
        return getMatch(text, regex, 1);
    }

    public static Optional<String> getMatch(String text, String regex, int groupInd) {
        return getMatch(text, regex, groupInd, false);
    }

    public static Optional<String> getMatch(String text, String regex, int groupIndex, boolean matchCase) {
        Pattern p = regexGetPattern(regex, matchCase);
        if (text != null) {
            Matcher m = p.matcher(text);
            if (m.find()) {
                return Optional.of(m.group(groupIndex));
            }
        }
        return Optional.empty();
    }

    public static boolean isMatch(String text, String pattern) {
        return isMatch(text, pattern, false);
    }

    public static boolean isMatch(String text, String regex, boolean matchCase) {
        if (text == null) {
            return false;
        }
        Pattern p = regexGetPattern(regex, matchCase);
        Matcher m = p.matcher(text);
        return m.find();
    }

    public static List<String> regexGetListOfMatches(String text, String regex) {
        return regexGetListOfMatches(text, regex, 1);
    }

    public static List<String> regexGetListOfMatches(String text, String regex, int group) {
        List<String> results = new ArrayList<>();
        Pattern p = regexGetPattern(regex, true);
        if (text != null) {
            Matcher m = p.matcher(text);
            while (m.find()) {
                results.add(m.group(group));
            }
        }
        return results;
    }

    public static String replaceNewLinesAndTrim(String text) {
        return text.replaceAll(LF, SPACE).replaceAll(SPACE + SPACE, SPACE).trim();
    }

    private static Pattern regexGetPattern(String regex, boolean matchCase) {
        int flags;
        if (matchCase) {
            flags = 0;
        } else {
            flags = Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE;
        }
        return Pattern.compile(regex, flags);
    }

    public static String removeAllDigits(String text) {
        return text.replaceAll("\\d+", "");
    }

    public static String substring(String text, String substringTo) {
        int index = text.indexOf(substringTo);
        if (index == -1)
            return text;
        else
            return text.substring(0, index);
    }

    public static String capitalizeFirst(String value) {
        return String.valueOf(value).substring(0, 1).toUpperCase() + String.valueOf(value).substring(1);
    }
}
