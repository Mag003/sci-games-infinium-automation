package utilities;

import org.apache.commons.text.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scigamesinfinium.DiProvider;

import java.util.List;
import java.util.Locale;

import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.SPACE;

public class NumberFunctions {
    private static final Logger LOGGER = LoggerFactory.getLogger(NumberFunctions.class);

    private NumberFunctions() {
    }

    public static Integer parseInt(String value) {
        return parseDouble(value).intValue();
    }

    public static Double parseDouble(String value) {
        Locale locale = DiProvider.configuration().localization().getLocale();
        return parseDouble(value, locale);
    }

    private static Double parseDouble(String value, Locale locale) {
        return parseDouble(value, 0, locale);
    }

    private static Double parseDouble(String value, int matchIndex, Locale locale) {
        String regexNumberPattern = "([\\d]+[\\d\\.\\,]*)";
        value = unescapeHtml(String.valueOf(value));
        value = String.valueOf(value).replace(SPACE, "").replace("'", EMPTY).replace(" ", EMPTY);
        List<String> matches = StringFunctions.regexGetListOfMatches(value, regexNumberPattern);
        if (matches.size() >= (matchIndex + 1)) {
            String numberAsString = matches.get(matchIndex);
            try {
                String separatorToRemove = ",";
                numberAsString = numberAsString
                        .replace(separatorToRemove, "");
                return Double.parseDouble(numberAsString);
            } catch (NumberFormatException e) {
                LOGGER.warn(String.format("Value '%1$s' can't be parsed to double. Exception: %2$s", numberAsString, e.getMessage()));
                return 0.;
            }
        }
        LOGGER.warn(String.format("Numbers are not found in the string '%1$s'", value));
        return 0.;
    }

    private static String unescapeHtml(String value) {
        try {
            value = StringEscapeUtils.unescapeHtml4(String.valueOf(value));
        } catch (RuntimeException e) {
            LOGGER.error("Can't unescape HTML encoding in the " + value + SPACE + e.getMessage());
        }
        return value;
    }
}
