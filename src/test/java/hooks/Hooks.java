package hooks;

import assertions.SoftAssert;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import scigamesinfinium.DiProvider;
import screenshots.ScreenshotHolder;
import utilities.ImageFunctions;
import utilities.StringFunctions;
import utilities.gmail.GmailBox;

import java.awt.image.BufferedImage;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

import static extensions.cucumber.ScenarioProcessor.getTestRailId;

public class Hooks {
    /**
     * We need to keep at least one before hook in the project
     * as PageTypeFactory will be automatically initialized before any steps
     */
    @Before
    public void registerConfiguration() {
        DiProvider.configuration().getPageType();
    }

    @After
    public void softAssert() {
        SoftAssert.getInstance().assertAll();
    }

    @After(value = "SCREENSHOT")
    public void sendScreenshots(Scenario scenario) {
        try {
            String name = getName(scenario);
            List<BufferedImage> images = ScreenshotHolder.getInstance().getImages();
            if (!images.isEmpty()) {
                Path path = Paths.get(name + ".png");
                ImageFunctions.concat(images, path);
                GmailBox gmailBox = new GmailBox(DiProvider.configuration().email());
                gmailBox.sendToDefault(name.replace("_", " "), "See scenario screenshots in the attachments", path.toFile());
                ScreenshotHolder.getInstance().clearImages();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getName(Scenario scenario) {
        String name = scenario.getName().replace(" ", "_");
        name = StringFunctions.getMatch(name.replaceAll("TR-[\\d]+", ""), "(\\w+)").orElse("scenario name undefined");

        Optional<String> optionalTrId = getTestRailId(scenario.getSourceTagNames());

        StringBuilder fullName = new StringBuilder();
        fullName.append(DiProvider.configuration().getPageType()).append("_");
        optionalTrId.ifPresent(id -> fullName.append(id).append("_"));
        fullName.append(name);

        return fullName.toString();
    }
}
