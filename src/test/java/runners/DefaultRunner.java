package runners;

import factory.PageTypeFactory;
import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        plugin = {"pretty", "testrail.TestRailCucumberListener"},
        features = "src/test/resources/features",
        tags = "@TR-2519387 and @MOBILE and @REGISTRATION and @ANDROID",
        glue = {"hooks", "scigames"},
        objectFactory = PageTypeFactory.class
        )
public class DefaultRunner {
}
