package assertions;

public interface IAssertionObject {
    String getPrintInfo();
}
