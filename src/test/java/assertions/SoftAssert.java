package assertions;

import net.serenitybdd.core.Serenity;
import net.thucydides.core.steps.StepEventBus;
import org.hamcrest.Matcher;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;

public class SoftAssert {
    private static final ThreadLocal<SoftAssert> instance = ThreadLocal.withInitial(SoftAssert::new);
    public final List<String> results = new ArrayList<>();
    public List<String> testrailResults = new ArrayList<>();
    protected static final Logger LOGGER = LoggerFactory.getLogger(SoftAssert.class);

    private SoftAssert() {
    }

    public static SoftAssert getInstance() {
        return instance.get();
    }

    public static <T> void assertField(String field, T actual, Matcher<? super T> matcher) {
        String descriptionTemplate = "Field '" + field + "' '%1$s' equal to API value";
        String descriptionShouldBe = String.format(descriptionTemplate, "SHOULD BE");
        String descriptionIs = String.format(descriptionTemplate, "IS");
        try {
            org.hamcrest.MatcherAssert.assertThat(descriptionShouldBe, actual, matcher);
            Serenity.recordReportData().withTitle(descriptionIs).andContents("ASSERTION SUCCESSFULLY PASSED");
        } catch (AssertionError error) {
            LOGGER.info(error.getMessage());
            getInstance().results.add(error.getMessage());
            Serenity.recordReportData().withTitle(descriptionShouldBe).andContents(error.getMessage());
        }
    }

    public static <T> void assertThat(String reason, T actual, Matcher<? super T> matcher) {
        try {
            org.hamcrest.MatcherAssert.assertThat(reason, actual, matcher);
        } catch (AssertionError error) {
            getInstance().results.add(error.getMessage());
        }
    }

    public static <T> void assertTrue(String reason, T actual) {
        try {
            Serenity.recordReportData().withTitle(reason).andContents("ASSERTION SUCCESSFULLY PASSED");
            org.hamcrest.MatcherAssert.assertThat(reason, actual, equalTo(true));
        } catch (AssertionError error) {
            getInstance().results.add(error.getMessage());
        }
    }

    public void assertAll() {
        try {
            testrailResults.addAll(results);
            Assert.assertTrue(
                    "Some of assertions were completed with errors \r\n"
                            .concat(String.join("\r\n", results))
                            .concat("\n"),
                    results.isEmpty()
            );
        } catch (AssertionError error) {
            StepEventBus.getEventBus().testFailed(new AssertionError(error.getMessage()));
            throw error;
        } finally {
            results.clear();
        }
    }
}
