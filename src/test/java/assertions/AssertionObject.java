package assertions;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;

import java.util.function.BooleanSupplier;
import java.util.function.Function;

import static org.hamcrest.CoreMatchers.equalTo;

public class AssertionObject<T extends IAssertionObject> {
    private final T first;
    private final T second;

    private AssertionObject(T first, T second) {
        this.first = first;
        this.second = second;
    }

    public static <T extends IAssertionObject> AssertionObject<T> compare(T first, T second) {
        return new AssertionObject<>(first, second);
    }

    public <R> AssertionObject<T> fieldIf(String fieldName, Function<T, R> fieldFunction, BooleanSupplier condition) {
        return fieldIf(fieldName, fieldFunction, Matchers::equalTo, condition);
    }

    public <R> AssertionObject<T> fieldIf(String fieldName, Function<T, R> fieldFunction, Function<R, Matcher<? super R>> matcher, BooleanSupplier condition) {
        if (condition.getAsBoolean()) {
            return field(fieldName, fieldFunction, matcher);
        }
        return this;
    }

    public <R> AssertionObject<T> field(String fieldName, Function<T, R> fieldFunction) {
        return field(fieldName, fieldFunction, Matchers::equalTo);
    }

    public <R> AssertionObject<T> field(String fieldName, Function<T, R> fieldFunction, Function<R, Matcher<? super R>> matcher) {
        String description = String.format("Comparison by field '%1$s' of objects '%2$s' and '%3$s' should be successfully passed",
                fieldName, first.getPrintInfo(), second.getPrintInfo());
        SoftAssert.assertThat(description, fieldFunction.apply(first), matcher.apply(fieldFunction.apply(second)));
        return this;
    }
}
