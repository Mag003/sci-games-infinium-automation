package screenshots;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class ScreenshotHolder {
    private static final ThreadLocal<ScreenshotHolder> instance = ThreadLocal.withInitial(ScreenshotHolder::new);
    private final ArrayList<BufferedImage> images = new ArrayList<>();

    private ScreenshotHolder() {
    }

    public static ScreenshotHolder getInstance() {
        return instance.get();
    }

    public void addScreenshot(BufferedImage image) {
        images.add(image);
    }

    public List<BufferedImage> getImages() {
        return images;
    }

    public void clearImages() {
        images.clear();
    }
}
