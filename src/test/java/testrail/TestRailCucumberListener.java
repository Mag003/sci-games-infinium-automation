package testrail;

import io.cucumber.plugin.ConcurrentEventListener;
import io.cucumber.plugin.event.*;

import java.util.List;
import java.util.Optional;

import static extensions.cucumber.ScenarioProcessor.*;

public class TestRailCucumberListener implements ConcurrentEventListener {
    private final TestRailLifeCycle lifecycle;

    public TestRailCucumberListener() {
        lifecycle = new TestRailLifeCycle();
    }

    @Override
    public void setEventPublisher(EventPublisher eventPublisher) {
        eventPublisher.registerHandlerFor(TestCaseStarted.class, this::onTestCaseStarted);
        if (lifecycle.isEnabled()) {
            eventPublisher.registerHandlerFor(TestRunStarted.class, this::onTestRunStarted);
            eventPublisher.registerHandlerFor(TestCaseFinished.class, this::onTestCaseFinished);
        }
    }

    public void onTestRunStarted(TestRunStarted testRunStarted) {
        lifecycle.addTestRun();
    }

    public void onTestCaseStarted(TestCaseStarted testCaseStarted) {
        Optional<String> optionalId = getTestRailId(testCaseStarted.getTestCase().getTags());
        String testCaseName = testCaseStarted.getTestCase().getName();
        String name = optionalId.map(id -> "TC [" + id + "] " + testCaseName).orElse(testCaseName);
        System.setProperty("scenario.name", name);
    }

    public void onTestCaseFinished(TestCaseFinished testCaseFinished) {
        try {
            TestCase testCase = testCaseFinished.getTestCase();
            List<String> trId = getTestRailIds(testCase.getTags());
            if (trId.size() > 0) {
                for (int i = 0; i < trId.size(); i++) {
                    lifecycle.addResultForCase(trId.get(i), testCaseFinished.getResult());
                }
            } else {
                throw new IllegalStateException(String.format(
                        "Scenario '%1$s' (%2$s) should have more than one tag @TR-{ID} to be able to run with TestRail integration",
                        testCase.getName(), testCase.getLocation()));
            }
        }
        catch (Exception exception){

        }
    }
}
