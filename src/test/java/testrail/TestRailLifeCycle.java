package testrail;

import assertions.SoftAssert;
import io.cucumber.plugin.event.Result;
import org.json.simple.JSONObject;
import utilities.ISettingsFile;
import utilities.JsonSettingsFile;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class TestRailLifeCycle {
    private final Configuration configuration;
    private final APIClient apiClient;

    private final ThreadLocal<Integer> currentRun = new ThreadLocal<>();

    public TestRailLifeCycle() {
        Path resourcePath = Paths.get("testrail.json");
        ISettingsFile settingsFile = new JsonSettingsFile(resourcePath.toString());
        this.configuration = new Configuration(settingsFile);
        apiClient = new APIClient(configuration.getUrl());
        apiClient.setUser(configuration.getUsername());
        apiClient.setPassword(configuration.getPassword());
    }

    public boolean isEnabled() {
        return configuration.isEnabled();
    }

    public void addTestRun() {
        Map<String, Object> data = new HashMap<>();
        data.put("name", configuration.getRunName() + "_" + LocalDateTime.now());
        data.put("suite_id", configuration.getSuiteId());
        JSONObject json = post("/add_run/" + configuration.getProjectId(), data);
        int id = Integer.parseInt(String.valueOf(json.get("id")));
        currentRun.set(id);
    }

    public void addResultForCase(String caseId, Result result) {
        Map<String, Object> data = new HashMap<>();
        data.put("status_id", configuration.getStatusId(result.getStatus().name()));
        if (result.getError() != null) {
            try {
                String message = Arrays.stream(result.getError().getStackTrace()).map(t -> t.toString()).collect(Collectors.joining("\r\n"));
                if (SoftAssert.getInstance().testrailResults.size() > 0) {
                    message += "\r\nSome of assertions were completed with errors \r\n"
                            .concat(String.join("\r\n", SoftAssert.getInstance().testrailResults))
                            .concat("\r\n");
                }
                data.put("comment", message);
            } catch (Exception e) {

            } finally {
                SoftAssert.getInstance().testrailResults.clear();
            }
        }
        post(String.format("/add_result_for_case/%1$s/%2$s", currentRun.get(), caseId), data);
    }

    public void stop() {
        currentRun.remove();
    }

    private JSONObject post(String path, Object data) {
        return request(APIClient.Method.POST, path, data);
    }

    private JSONObject request(APIClient.Method method, String path, Object data) {
        try {
            Object result = apiClient.sendRequest(method, path, data);
            return (JSONObject) result;
        } catch (IOException | APIException e) {
            throw new IllegalArgumentException("Cannot call TestRail API endpoint " + path + "%n" + e.getMessage());
        }
    }
}
