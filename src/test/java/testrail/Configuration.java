package testrail;

import utilities.ISettingsFile;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class Configuration {
    private final boolean enabled;
    private final String url;
    private final String username;
    private final String password;
    private final int projectId;
    private final int suiteId;
    private final String runName;
    private final Map<String, Integer> statuses;


    public Configuration(ISettingsFile configuration) {
        this(
                getValue(configuration, Fields.ENABLED, Boolean::parseBoolean),
                getValue(configuration, Fields.URL),
                getValue(configuration, Fields.USERNAME),
                getValue(configuration, Fields.PASSWORD),
                getValue(configuration, Fields.PROJECT_ID, Integer::parseInt),
                getValue(configuration, Fields.SUITE_ID, Integer::parseInt),
                getValue(configuration, Fields.RUN_NAME),
                getStatuses(configuration)
        );
    }

    private Configuration(boolean enabled, String url, String username, String password,
                          int projectId, int suiteId, String runName,
                          Map<String, Integer> statuses) {
        this.enabled = enabled;
        this.url = url;
        this.username = username;
        this.password = password;
        this.projectId = projectId;
        this.suiteId = suiteId;
        this.runName = runName;
        this.statuses = statuses;
    }

    private static String getValue(ISettingsFile configuration, Fields field) {
        return getValue(configuration, field, value -> value);
    }

    private static <T> T getValue(ISettingsFile configuration, Fields field, Function<String, T> conversion) {
        return conversion
                .apply(String.valueOf(configuration.getValue("/testrail/" + field.getPath())));
    }

    private static Map<String, Integer> getStatuses(ISettingsFile configuration) {
        Map<String, Integer> statuses = new HashMap<>();
        configuration.getMap("/testrail/statuses")
                .forEach((key, value) -> statuses.put(key.toLowerCase(), Integer.parseInt(String.valueOf(value))));
        return statuses;
    }

    public int getStatusId(String name){
        return statuses.get(name.toLowerCase());
    }

    public String getUrl() {
        return url;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public int getSuiteId() {
        return suiteId;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public int getProjectId() {
        return projectId;
    }

    public String getRunName() {
        return runName;
    }

    private enum Fields {
        ENABLED, URL, USERNAME, PASSWORD, SUITE_ID, PROJECT_ID,
        RUN_NAME("run/name");

        private final String path;

        Fields() {
            this.path = name().toLowerCase();
        }

        Fields(String path) {
            this.path = path;
        }

        public String getPath() {
            return path;
        }
    }
}
