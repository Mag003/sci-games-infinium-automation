package factory;

import io.appium.java_client.AppiumDriver;
import net.thucydides.core.ThucydidesSystemProperty;
import net.thucydides.core.guice.Injectors;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.webdriver.DriverSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import scigamesinfinium.DiProvider;

import java.net.MalformedURLException;
import java.net.URL;

public class BrowserStackFactory implements DriverSource {
    private final EnvironmentVariables environmentVariables = Injectors.getInjector().getProvider(EnvironmentVariables.class).get();

    @Override
    public WebDriver newDriver() {
        String bsUser = ThucydidesSystemProperty.BROWSERSTACK_USER.from(environmentVariables);
        String bsKey = ThucydidesSystemProperty.BROWSERSTACK_KEY.from(environmentVariables);

        String bsOs = ThucydidesSystemProperty.BROWSERSTACK_OS.from(environmentVariables);
        String bsOsVersion = ThucydidesSystemProperty.BROWSERSTACK_OS_VERSION.from(environmentVariables);
        String bsDevice = ThucydidesSystemProperty.BROWSERSTACK_DEVICE.from(environmentVariables);

        String bsBrowser = ThucydidesSystemProperty.BROWSERSTACK_BROWSER.from(environmentVariables);
        String bsBrowserVersion = ThucydidesSystemProperty.BROWSERSTACK_BROWSER_VERSION.from(environmentVariables);

        String app = getProperty("appium.app");
        String bsAppiumVersion = getProperty("browserstack.appium_version");
        String bsRealMobile = getProperty("browserstack.real_mobile");

        String bsTimeZone = DiProvider.configuration().getTimeZone().toString();
        String bsProject = ThucydidesSystemProperty.BROWSERSTACK_PROJECT.from(environmentVariables);
        String bsBuild = ThucydidesSystemProperty.BROWSERSTACK_BUILD.from(environmentVariables);

        String gpsLocation = getProperty("browserstack.gpsLocation");
        String geoLocation = getProperty("browserstack.geoLocation");
        String debug = getProperty("browserstack.debug");
        String networkLogs = getProperty("browserstack.networkLogs");

        try {
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability("os", bsOs);
            capabilities.setCapability("os_version", bsOsVersion);
            capabilities.setCapability("device", bsDevice);
            capabilities.setCapability("browser", bsBrowser);
            capabilities.setCapability("browser_version", bsBrowserVersion);
            capabilities.setCapability("app", app);
            capabilities.setCapability("browserstack.appium_version", bsAppiumVersion);
            capabilities.setCapability("real_mobile", bsRealMobile);
            capabilities.setCapability("name", getProperty("scenario.name"));
            capabilities.setCapability("browserstack.timezone", bsTimeZone);

            capabilities.setCapability("browserstack.gpsLocation", gpsLocation);
            capabilities.setCapability("browserstack.debug", debug);
            capabilities.setCapability("browserstack.networkLogs", networkLogs);

            capabilities.setCapability("project", bsProject);
            capabilities.setCapability("build", bsBuild);
            return new AppiumDriver<>(
                    new URL(String.format("https://%1$s:%2$s@hub-cloud.browserstack.com/wd/hub", bsUser, bsKey)),
                    capabilities);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    private String getProperty(String name) {
        String systemValue = System.getProperty(name);
        return systemValue != null ? systemValue : environmentVariables.getProperty(name);
    }

    @Override
    public boolean takesScreenshots() {
        return true;
    }
}
