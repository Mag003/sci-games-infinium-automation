package translations;

import utilities.sheets.AbstractSheet;

import java.util.Locale;

public class TranslationsSheet extends AbstractSheet {
    private final String name;

    public TranslationsSheet(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public enum Column {
        ENGLISH(0),
        SPANISH(1),
        ID(2);

        private final int index;

        Column(int index) {
            this.index = index;
        }

        public int getIndex() {
            return index;
        }

        public static Column getColumnByLocale(Locale locale) {
            switch (locale.getLanguage()) {
                case "es":
                    return Column.SPANISH;
                case "en":
                default:
                    return Column.ENGLISH;
            }
        }
    }
}
