package translations;

import scigamesinfinium.DiProvider;
import utilities.JsonSettingsFile;
import utilities.sheets.Form;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

public abstract class ATranslation {
    private final File file;
    private final Map<TranslationsForm.Sheets, List<List<String>>> sheets = new HashMap<>();

    protected ATranslation(String fileName) {
        JsonSettingsFile jsonSettingsFile = new JsonSettingsFile("config.json");
        String path = jsonSettingsFile.getValue("/translations") + fileName;
        this.file = new File(System.getProperty("user.dir") + path);
    }

    protected List<List<String>> get(TranslationsForm.Sheets sheet) {
        if (sheets.containsKey(sheet)) {
            return sheets.get(sheet);
        } else {
            Form translationsForm = new TranslationsForm(file);
            return translationsForm.getSheet(sheet.getName()).getStringList();
        }
    }

    protected List<String> getTranslations(TranslationsForm.Sheets sheet, TranslationId id) {
        List<List<String>> dictionary = get(sheet);
        TranslationsSheet.Column column = getLanguage();
        return dictionary.stream()
                .filter(line -> is(line, id))
                .map(line -> line.get(column.getIndex()))
                .collect(Collectors.toList());
    }

    protected String getTranslation(TranslationsForm.Sheets sheet, TranslationId id) {
        return getTranslations(sheet, id).stream().findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Translation by id '%1$s' was not found on sheet '%2$s' in the document '%3$s'",
                        id.getName(), sheet.getName(), file.getAbsoluteFile())));
    }

    private boolean is(List<String> line, TranslationId id) {
        return line.get(TranslationsSheet.Column.ID.getIndex()) != null &&
                line.get(TranslationsSheet.Column.ID.getIndex()).contains(id.getName());
    }

    private TranslationsSheet.Column getLanguage() {
        return TranslationsSheet.Column.getColumnByLocale(DiProvider.configuration().localization().getLocale());
    }
}
