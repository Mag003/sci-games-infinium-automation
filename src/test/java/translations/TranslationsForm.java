package translations;

import utilities.sheets.IWorkSheet;
import utilities.sheets.Sheet;
import utilities.sheets.WorkbookAbstractForm;

import java.io.File;

public class TranslationsForm extends WorkbookAbstractForm implements IWorkSheet {
    // Used as an example for working with translation file
    private final int TABLE_ROWS_OFFSET = 7;

    public TranslationsForm(File file) {
        super(file);
    }

    @Override
    public Sheet getSheet(String name) {
        TranslationsSheet translationsSheet = new TranslationsSheet(name);
        readSheet(form.getSheet(name), translationsSheet, TABLE_ROWS_OFFSET);
        return translationsSheet;
    }

    public enum Sheets {
        // Used as an example for working with translation file
        SCI_MOBILE("List name");

        private final String name;

        Sheets(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }
}
