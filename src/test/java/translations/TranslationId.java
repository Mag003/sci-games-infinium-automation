package translations;

public enum TranslationId {
    // Used as an example for working with translation file
    LOGIN("sci.games.home.login");

    private final String name;

    TranslationId(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
