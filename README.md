# Scientific Games Infinium 
## Overview
The template is build over [Serenity BDD framework](https://serenity-bdd.github.io/theserenitybook/latest/). The last was created to provide abilities to do automation testing with Selenium WebDriver and to provide HTML reports out of the box.

The basic concepts that the template cover are:
- [Page Object](#page-object)  
- [Cucumber](#cucumber)
- [Serenity Steps](#serenity-steps)
- [Typical project structure](#typical-project-structure)
- [BrowserStack](#browserstack)
- [Running the tests](#running-the-tests)
    - [Maven](#1-maven)
    - [IDEA Plugin](#2-idea-plugin)
    - [Useful commands](#3-useful-commands)
  
- [CI Integration](#ci-integration)

The template extends Serenity BDD by adding following functionaity:
- Screen Factory
- Pre-defined build configuration
- Multi-environment runs
- TestRail integration
- ConditionalWait utility

Also, the template provides examples of usage:
- API testing with Serenity RestAssured
- Mobile testing
  - Local
  - Service
- Web testing
- BrowserStack integration
- Parallel runs

### Page Object 
The most simple and clear principle of the quality programming is [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself) - do not repeat yourself. This rule can be seen across the all programming patterns and ideas.

In the automation, one of the main work is describing the interface of the application we test. If we automate GUI the interface is a list of elements/controls that users can interact with. If we automate an API - interface is a list of endpoints that user can call.

To follow the DRY principle in automation PageObjects approach/pattern was invented for. The idea is to avoid the same elements/controls duplicating across the project, we have to split the application into pages or forms (here and below under form the part of the full page will be mean) and keep each unique element/control only once. Then each such page/form will contain the list of elements that are presented on.
```
/src
	/test
		/java
  			/pages
  				LoginPage
  				ProfilePage
  				ShoppingCartPage
  				...
```

The login page will look like this

```
class LoginPage {
	WebElement txtUsername
	WebElement txbPassword
	WebElement btnLogin
}
```

But is not enough to list the names of the elements, we need to specify to the automation tool (WebDriver if we use Selenium) on how to locate this particular element in the browser. To do this Selenium provides different approaches, but generally all of them we call the element's **locator**. The most popular locator strategies are: by [XPath](https://en.wikipedia.org/wiki/XPath), by [CSS](https://en.wikipedia.org/wiki/CSS), by id. For each type Selenium WebDriver library provides appropriate class (By.Xpath, By.Css, and etc.). The details of how to build locators are out of scope this readme.

When we initialize our elements with locators the LoginPage can look like:
```
class LoginPage {
	WebElement txtUsername = driver.findElement(By.id("username"))
	WebElement txbPassword = driver.findElement(By.Xpath("//input[@type='password']"))
	WebElement btnLogin = driver.findElement(By.name("signin"))
}
```
As you can notice in the example above the **driver** was appeared. It is an instance of the WebDriver class of selenium. WebDriver works like a translator between your code and the web browser - you ask it to find element by id 'username' and to click on then, driver then translates your code into the command that the browser understands. Because there is some amount of browsers, we have a special translator (WebDriver) for each: ChromeDriver for Chrome, GeckoDriver for firefox, SafariDriver for Safari, and etc.

It is obvious we need to get WebDriver as soon as we need to do something in the browser or mobile application. Also, we need to keep the same driver within the steps of our test scenario. Let's see at how test scenario can look like:

```
testUserShouldBeAbleToLogin(){
	WebDriver driver = new ChromeDriver();				// 1. open the Chrome
	driver.manage().navigate("http://www.google.com"); 	// 2. go to url
	LoginPage loginPage = new LoginPage(driver); 		// 3. pass driver to page
	loginPage.login("username", "password"); 			// 4. perform login
	...
}
```
Now let's imagine we need to interact several pages during the test.
```
LoginPage loginPage = new LoginPage(driver);
loginPage.login("username", "password");

MainPage mainPage = new MainPage(driver);
mainPage.openProfile();

ProfilePage profilePage = new ProfilePage(driver);
profilePage.getAddress();
...
```
You may notice that **driver** has been passed to each page. And it is the case where Serenity can help us to hide this under the hood. Serenity uses Depency Injection library [Guice](https://github.com/google/guice/wiki/Motivation#dependency-injection-with-guice); This approach allows put the instance of some class (like WebDriver) into the Java context and then be able to fetch the same instance out. For us and the example with **driver** that means that we can create a driver before the test, put it into context and be able to get it on the **LoginPage** then.

```
class LoginPage extends net.serenitybdd.core.pages.PageObject{
	WebElement txtUsername = getDriver().findElement(By.id("username"))
	WebElement txbPassword = getDriver().findElement(By.Xpath("//input[@type='password']"))
	WebElement btnLogin = getDriver().findElement(By.name("signin"))
}
```
Probably you have noticed what has been changed:
- LoginPage currently **extends net.serenitybdd.core.pages.PageObject** - it is the Serenity provided class that is initializing with WebDriver before test started. By extending this class we allow LoginPage to access to the WebDriver trough the getDriver() method of **net.serenitybdd.core.pages.PageObject**. Inside the **net.serenitybdd.core.pages.PageObject** there Dependecy Injection approach that mentioned before.

- **driver** has been replaced with **getDriver()** method of the **net.serenitybdd.core.pages.PageObject** class.

Instead of passing **driver** to each page, we should simple extend **PageObject** class of Serenity.
```
// no drivers passed
LoginPage loginPage = new LoginPage();
MainPage mainPage = new MainPage();
ProfilePage profilePage = new ProfilePage();
...
```
**CONCLUSION:** Page Object approach allows to avoid duplicating of locators of elements and structure your code. By extending our classes of **net.serenitybdd.core.pages.PageObject**  we will get access to the WebDriver.

### Cucumber

In simple words, cucumber is a high level of abstraction written in a human-readable manner. So, instead of writing automated scripts with a programming language:
```
loginPage.login(username, password)
```
with cucumber the same can look:
```
When User login with username and password
```

You see that the cucumber provides abilities to involve in testing the people who do not know any programming languages. The most classical case is when cucumber scenarios are written by Business Analysts and then can be automated by an Automation Engineer.

But if we look deeper we will discover how the cucumber works. Under each step like "When User login with username and password" there is a "Step Definition" or "Step Implementation" that is programming code:
```
@When("User login with {string} and {string}")
public void login(Srting username, String password){
    loginPage.login(username, password)
}
```
This example shows how human-readable text is translated into the code that then will execute some actions related to the scenario.

More details about Cucumber is on [official web site](https://cucumber.io/docs/cucumber/)

**CONCLUSION:** Cucumber allows to involve non-programmers into the QA/Dev work.

### Serenity Steps

From the example below it is not obvious how we get **loginPage** instance:
```
@When("User login with {string} and {string}")
public void login(Srting username, String password){
    loginPage.login(username, password)
}
```
To have **loginPage** correctly initialized with Selenium WebDriver we have to use _@Steps_ annotation of the Serenity:

```
class Steps{

    @Steps
    LoginPage loginPage;

    @When("User login with {string} and {string}")
    public void login(Srting username, String password){
        loginPage.login(username, password)
    }
}

```
The code below will automatically create new instance of class LoginPage and assign this instance to variable **loginPage**

```
@Steps
LoginPage loginPage;

```
**CONCLUSION:** Serenity _@Steps_ annotation allows to instantiate new instances of our PageObject classes (or any  other classes).

### Typical project structure
The typical structure of the project with Cucumber and Serenity is following:
```
/project/
    /src/
        /test/
            /pages/
                class LoginPage {
                    WebElement txtUsername = driver.findElement(By.id("username"))
                    WebElement txbPassword = driver.findElement(By.Xpath("//input[@type='password']"))
                    WebElement btnLogin = driver.findElement(By.name("signin"))
                }
                class PageN
                class ...
            /steps/
                class Steps{
                    @Steps
                    LoginPage loginPage;

                    @When("User login with {string} and {string}")
                    public void login(Srting username, String password){
                        loginPage.login(username, password)
                    }
                }
                class StepN
                class ...
        /resources/
            /features/
                Login.feature
                    Scenario: User should be able to login
                        When User login with username and password
                FeatureN.feature
                ...
            serentity.conf
```
We have already passed though pages, steps and features(or scenarios), and now we want yo pay your attention to the serenity.conf file. It is the main configuration file for Serenity. Here we should specify necessary preferences for the Selenium WebDriver and other features that Serenity provides (screenshots, reports and etc.).

### BrowserStack
#### _Uploading APK on BrowserStack_
The API for uploading is [here](https://www.browserstack.com/docs/app-automate/api-reference/appium/apps#upload-an-app)
All you need is to use command like:

```
curl -u "YOUR_KEY:YOUR_PASS" -X POST "https://api-cloud.browserstack.com/app-live/upload" -F "file=@/path/to/app/file/Application-debug.apk"
```
In the response you have to get list of attributes related to uploaded atifact:

```
{"app_url":"bs://e35ab152994c99f0f40152721a65de8179ceeb90","custom_id":"sci_games_infinium","shareable_id":"a.demeshchenko/sci_games_infinium"}
```

### Running the tests
The framework supports running the tests using Maven command or using IDEA Plugin.
For integration tests into CI process, you should use Maven. For development and debugging it would be simpler to run tests with the IDEA plugin.
#### 1. Maven
To run tests with the Maven all you need is Maven installed on your machine. Then you can use following commands:
- _browserstack_:

```
clean verify -Dserenity.profile=browserstack.android -Denvironment.name=uat -Dparallel.tests=2 -Dcucumber.tags="--tags @TR-123456" -Dcucumber.glue=hooks,mobile,web -Dbrowserstack.user=${browserstack.user} -Dbrowserstack.key=${browserstack.key} -Dbrowserstack.os_version=11.0 -Dbrowserstack.device=Google Pixel 4 -Dbrowserstack.appium_version=1.17.0 -Dbrowserstack.real_mobile=true -Dbrowserstack.build=SciGames Infinium uat -Dappium.app=sci-games-app -Dtimezone=GMT+0
```

Where:

```
serenity.profile
```

file name from [./src/test/resources/serenity](./src/java/resources/serenity) folder that will be used as a Serenity configuration. During the executing of the Maven command selected profile will be copied to the root of the project. The profile should contain only [properties allowed by Serenity](https://serenity-bdd.github.io/theserenitybook/latest/serenity-system-properties.html).

```
environment.name
```

defines the directory with the configuration for a particular environment. The list of folders with environment configurations is located by path 'src/test/resources/environment'. During the run, environment-related properties (api url, front-end url and etc.) will be fetched from this file.

``` 
tags
```

defines the expression that will be used for filtering scenarios to execute. For example:

> Expression '--tags @INTEGRATION' will run only scenarios with tag @INTEGRATION.

> Expression '--tags @SMOKE --tags @P1' will run only scenarios with both tags: @SMOKE **AND** @P1.

> Expression '--tags @SMOKE,@P1' will run only scenarios with @SMOKE **OR** @P1 tags.

> Expression '--tags @SMOKE,@P1 --tags @ANDROID' will run only scenarios with one of the tags @SMOKE **OR** @P1 tags and tag @ANDROID.

```
cucumber.glue
```

defines comma-separated list of packages with step definitions

```
browserstack.user
browserstack.key
browserstack.os_version
browserstack.device
browserstack.appium_version
browserstack.real_mobile
browserstack.build
```

browserstack specific variables from https://www.browserstack.com/automate/capabilities. Also, pay attention that for BrowserStack we use custom driver factory *factory.BrowserStackFactory*:

```
webdriver.driver = provided
webdriver.provided.type = browserstack
webdriver.provided.browserstack = factory.BrowserStackFactory
```

```
parallel.tests
```

defines number of parallel threads during the run

```
appium.app
```

defines the path to the apk file or (for browserstack) app-id of the uploaded file. More details [here](https://www.browserstack.com/docs/app-automate/appium/getting-started/java#2-upload-your-app) and [here](http://appium.io/docs/en/writing-running-appium/caps/).

- _local_:

```
clean verify -Dparallel.tests=1 -Dserenity.profile=win.web.service  -Dcucumber.tags="--tags @TR-641473" -Dcucumber.glue=hooks,mobile,web
```

Here you can se the same parameters expect browserstack-related.

Pay attention that for local runs we are using different profile -> *appium.service.android*.

Also, pay attention that we do not pass *timezone*, *appium.app* parameters. That means the default parameters will be used. For *timezone* default parameter is defined in the file [.src/test/resources/config.json](.src/test/resources/config.json). For *appium.app* value will be fetched from the defined serenity profile.

#### 2. IDEA Plugin
To run tests with IDEA plugin simple configure Cucumber optins in the file [./src/test/java/runners/DefaultRunner.java](./src/test/java/runners/DefaultRunner.java). More details [here](https://cucumber.io/docs/cucumber/api/).
#### 3. Useful commands
- dry-run (to check whether all steps from the features have implementations):

```
clean verify -Dparallel.tests=2 -Drunner.template=cucumber-serenity-dry-runner 
-Dcucumber.glue=hooks,mobile,web
```

- clean and compile (to clean working directory and compile project):

```
clean test-compile -Dcucumber.glue=hooks,mobile,web
```

- serenity aggregate (to build serenity report):

```
serenity:aggregate
```

### Velocity for auto-geeneration of the scenarios

Sometimes you will need to have a list of scenarios that are different by only several parameters or steps. In many cases it is possible to use [Scenario Outline](https://cucumber.io/docs/gherkin/reference/). But Scenario Outline will need us to create complicated steps to process dynamic data (for example different amount of runners, multipliers and etc.)

Hopefully, there is other way to implement data driven scenarios without increasing of complexicity of our steps - using of [Apache Velocity Templates](https://velocity.apache.org/).

So, we created `Template` for each group. Templates are stored in [src/main/resources/templates](./src/main/resources/templates).

Also, we have added [Generator](src/main/java/scigamesinfinium/generators) class that process list of templates and generates list of feature files by parametrizing existing templates.

But you may notice that such files do not highlighted as Cucumber features. And it can be inconvinient as we do not see if the step definition is found. To correct this behaviour you can override FileType mapping in IDEA.

To do this go to File -> Preferences -> Editor -> File Types. There find 'Cucumber Scenario' and add new file wildcard to this: `*.vm`

![image](assets/add_file_type_vm.png)

## CI Integration
This chapter will cover steps that should be done to setup Jenkins job for running automated scenarios.

This guide includes only steps to setup Jenkins job. How to install Jenkins itself you can read here: https://www.jenkins.io/.

### 1. Plugins installation
Be sure that [Maven Integration Plugin](https://plugins.jenkins.io/maven-plugin/) has been installed. If not, install it as we need Maven to run tests in CI system.

After the installation you will have to configure Maven plugin to make it download automatically Maven itself. To do this in Jenkins go to: _Manage ->  Global Tool Configuration_. Then scroll to Maven section and add new Maven binary to be downloaded automatically.

![image](assets/configure_maven_binary.png)

The next plugin we are going to use is [HTML Publisher](https://plugins.jenkins.io/htmlpublisher/). This plugin will allow us to publish HTML reports with automation results.

As most modern browsers block executing javascript from ‘suspicious’ sources and because our Jenkins looks one of these. We need to correct CSP (content-security-policy) in the Jenkins to be able to see HTML reports and have all functions working.

Go to _Manage -> Script Console_. Then execute following code

```
System.setProperty("hudson.model.DirectoryBrowserSupport.CSP", "default-src * 'unsafe-inline' 'unsafe-eval'; script-src * 'unsafe-inline' 'unsafe-eval'; connect-src * 'unsafe-inline'; img-src * data: blob: 'unsafe-inline'; frame-src *; style-src * 'unsafe-inline';")
```

![image](assets/allow_to_execute_js.png)

### 2. Job Creation
The most simple way to configure job is using of Maven Type during creation.

![image](assets/create_maven_job.png)

### 3. Add VCS
Configure VCS to get project from the Git
![image](assets/add_vcs.png)

### 4. Add Maven Command
Add follwing Maven command:

```
clean verify -Dserenity.profile=${serenity.profile} -Denvironment.name=${environment.name} -Dparallel.tests=1 -Dcucumber.tags="${cucumber.tags}" -Dcucumber.glue=hooks,mobile,web -Dbrowserstack.user=${browserstack.user} -Dbrowserstack.key=${browserstack.key} -Dbrowserstack.os_version=${browserstack.os_version} -Dbrowserstack.device="${browserstack.device}" -Dbrowserstack.appium_version=${browserstack.appium_version} -Dbrowserstack.real_mobile=${browserstack.real_mobile} -Dappium.app=${appium.app} -Dbrowserstack.build="$JOB_NAME $BUILD_ID ${build_version} ${environment.name}" -Dtimezone="GMT+0"
```

Pay attention, ```${environment.name}``` means that this parameter will be replaced by value from the job's parameters.

Also, note that command above is for BrowserStack usage.

![image](assets/add_maven_command.png)

### 5. Serenity aggregate Maven command
To generate HTML report we have to run an additional command after tests have been completed.

To add additional command use POST Build step and select Maven option there.
As command use

```
serenity::aggregate
```

![image](assets/add_serenity_aggregate.png)


#### 6. Add Job Parameters
To make job more flexible, add parameters that can be changed during runs
![image](assets/parameters.png)

Congratulations! Now you can run your scenarios from Jenkins.
![image](assets/run_with_parameters.png).